package algorithms.sort;

import java.util.Arrays;
import java.util.Random;

public class ShellSort {

    public static void main(String[] args) {
        int[] array = new Random().ints(10, 1, 100).toArray();

        System.out.println(Arrays.toString(array));
        shellSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void shellSort(int[] array) {
        int inner, outer;
        int tmp;
        int h = 1;
        while (h <= array.length / 3) {
            h = h * 3 + 1;
        }
        while (h > 0) {
            for (outer = h; outer < array.length; outer++) {
                tmp = array[outer];
                inner = outer;
                while (inner > h - 1 && array[inner - h] >= tmp) {
                    array[inner] = array[inner - h];
                    inner -= h;
                }
                array[inner] = tmp;
            }
            h = (h - 1) / 3;
        }
    }
}
