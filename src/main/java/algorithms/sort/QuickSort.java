package algorithms.sort;

import java.util.Arrays;
import java.util.Random;

public class QuickSort {

    public static void main(String[] args) {
        int[] array = new Random().ints(5, 1, 100).toArray();

        System.out.println(Arrays.toString(array));
        quickSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void quickSort(int[] array) {
        quickSort(array, 0, array.length - 1);
    }

    private static void quickSort(int[] array, int left, int right) {
        if (left >= right) {
            return;
        }
        int pivot = array[right];
        int partition = partitionIt(array, left, right, pivot);
        quickSort(array, left, partition - 1);
        quickSort(array, partition + 1, right);
    }

    private static int partitionIt(int[] array, int left, int right, int pivot) {
        int leftPtr = left - 1;
        int rightPrt = right;
        while (true) {
            while (array[++leftPtr] < pivot);
            while (rightPrt > 0 && array[--rightPrt] > pivot);

            if (leftPtr >= rightPrt) {
                break;
            }
            swap(array, leftPtr, rightPrt);
        }
        swap(array, leftPtr, right);
        return leftPtr;
    }

    private static void swap(int[] array, int leftPtr, int rightPrt) {
        int tmp = array[leftPtr];
        array[leftPtr] = array[rightPrt];
        array[rightPrt] = tmp;
    }

}
