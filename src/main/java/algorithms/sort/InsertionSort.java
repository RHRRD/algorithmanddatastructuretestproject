package algorithms.sort;

import java.util.Arrays;
import java.util.Random;

public class InsertionSort {

    public static void main(String[] args) {
        int[] array = new Random().ints(10, 1, 100).toArray();

        System.out.println(Arrays.toString(array));
        insertionSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void insertionSort(int[] array) {
        int in;
        for (int out = 1; out < array.length; out++) {
            int tmp = array[out];
            in = out;
            while (in > 0 && array[in - 1] >= tmp) {
                array[in] = array[in - 1];
                --in;
            }
            array[in] = tmp;
        }
    }

}
