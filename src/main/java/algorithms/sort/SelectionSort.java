package algorithms.sort;

import java.util.Arrays;
import java.util.Random;

public class SelectionSort {

    public static void main(String[] args) {
        int[] array = new Random().ints(10, 1, 100).toArray();

        System.out.println(Arrays.toString(array));
        selectionSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void selectionSort(int[] array) {
        int min;
        for (int out = 0; out < array.length - 1; out++) {
            min = out;
            for (int in = out + 1; in < array.length; in++) {
                if (array[in] < array[min]) {
                    min = in;
                }
            }
            swap(array, out, min);
        }
    }

    private static void swap(int[] array, int in, int i) {
        int tmp = array[in];
        array[in] = array[i];
        array[i] = tmp;
    }

}
