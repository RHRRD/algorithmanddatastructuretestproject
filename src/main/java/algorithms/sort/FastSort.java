package algorithms.sort;

import java.util.Arrays;
import java.util.Random;

public class FastSort {

    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(90) + 10;
        }
        System.out.println(Arrays.toString(array));

        fastSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void fastSort(int[] array) {
        fastSort(array, 0, array.length - 1);
    }

    private static void fastSort(int[] array, int left, int right) {
        if (left >= right) {
            return;
        }
        int pivot = array[right];
        int partition = partition(array, left, right, pivot);
        fastSort(array, left, partition - 1);
        fastSort(array, partition + 1, right);
    }

    private static int partition(int[] array, int left, int right, int pivot) {
        int leftPtr = left - 1;
        int rightPtr = right;
        while (true) {
            while (array[++leftPtr] < pivot);
            while (rightPtr > 0 && array[--rightPtr] > pivot);

            if (leftPtr >= rightPtr) {
                break;
            }
            swap(array, leftPtr, rightPtr);
        }
        swap(array, leftPtr, right);
        return leftPtr;
    }

    private static void swap(int[] array, int leftPtr, int rightPtr) {
        int tmp = array[leftPtr];
        array[leftPtr] = array[rightPtr];
        array[rightPtr] = tmp;
    }


}
