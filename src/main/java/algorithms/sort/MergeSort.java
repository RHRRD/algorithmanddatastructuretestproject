package algorithms.sort;

import java.util.Arrays;
import java.util.Random;

public class MergeSort {

    public static void main(String[] args) {
        int[] array = new Random().ints(10, 1, 100).toArray();

        System.out.println(Arrays.toString(array));
        mergeSort(array);
        System.out.println(Arrays.toString(array));
    }

    private static void mergeSort(int[] array) {
        int[] tmpArray = new int[array.length];
        mergeSort(array, 0, array.length - 1, tmpArray);
    }

    private static void mergeSort(int[] array, int left, int right, int[] tmpArray) {
        if (left == right) {
            return;
        }
        int mid = (left + right) / 2;
        mergeSort(array, left, mid, tmpArray);
        mergeSort(array, mid + 1, right, tmpArray);
        merge(array, left, mid + 1, right, tmpArray);
    }

    private static void merge(int[] array, int left, int mid, int right, int[] tmpArray) {
        int i1 = left, i2 = mid, iTmp = 0;
        while (i1 < mid && i2 <= right) {
            if (array[i1] < array[i2]) {
                tmpArray[iTmp++] = array[i1++];
            } else {
                tmpArray[iTmp++] = array[i2++];
            }
        }
        while (i1 < mid) {
            tmpArray[iTmp++] = array[i1++];
        }
        while (i2 <= right) {
            tmpArray[iTmp++] = array[i2++];
        }
        for (int i = 0; i < right - left + 1; i++) {
            array[left + i] = tmpArray[i];
        }
    }

}
