--https://leetcode.com/problems/exchange-seats/
--
--Create table If Not Exists Seat (id int, student varchar(255))
--Truncate table Seat
--insert into Seat (id, student) values ('1', 'Abbot')
--insert into Seat (id, student) values ('2', 'Doris')
--insert into Seat (id, student) values ('3', 'Emerson')
--insert into Seat (id, student) values ('4', 'Green')
--insert into Seat (id, student) values ('5', 'Jeames')
--
--Table: Seat
--
--+-------------+---------+
--| Column Name | Type    |
--+-------------+---------+
--| id          | int     |
--| student     | varchar |
--+-------------+---------+
--id is the primary key column for this table.
--Each row of this table indicates the name and the ID of a student.
--id is a continuous increment.
--
--
--Write an SQL query to swap the seat id of every two consecutive students. If the number of students is odd, the id of the last student is not swapped.
--
--Return the result table ordered by id in ascending order.
--
--The query result format is in the following example.
--
--
--
--Example 1:
--
--Input:
--Seat table:
--+----+---------+
--| id | student |
--+----+---------+
--| 1  | Abbot   |
--| 2  | Doris   |
--| 3  | Emerson |
--| 4  | Green   |
--| 5  | Jeames  |
--+----+---------+
--Output:
--+----+---------+
--| id | student |
--+----+---------+
--| 1  | Doris   |
--| 2  | Abbot   |
--| 3  | Green   |
--| 4  | Emerson |
--| 5  | Jeames  |
--+----+---------+
--Explanation:
--Note that if the number of students is odd, there is no need to change the last one's seat.
with tmp as (
  select
    s.id as id_1, s.student as student_1,
    sub.id as id_2, sub.student as student_2
  from Seat s left join Seat sub on s.id + 1 = sub.id
),
tmp_2 as (
  select * from tmp
  where id_1 % 2 = (select min(id) from Seat) % 2
)
select id_1 as id, student_2 as student from tmp_2
where student_2 is not null
union
select id_2 as id, student_1 as student from tmp_2
where id_2 is not null
union
select id_1 as id, student_1 as student from tmp_2
where id_2 is null and student_2 is null
order by id asc