WITH my_table (id, email) AS (
     VALUES (1, 'a.1')
     ,(2, 'b.2')
     ,(3, 'a.1')
     )
SELECT
email
FROM my_table group by email having count(*) > 1;