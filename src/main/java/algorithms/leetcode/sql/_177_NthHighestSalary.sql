with Employee (Id, Salary) as (
VALUES
(1, 70000),
(2, 80000),
(3, 60000),
(4, 90000)
) select * from Employee order by Salary desc limit 1 offset 0;


CREATE FUNCTION getNthHighestSalary(N INT) RETURNS INT
BEGIN
DECLARE M INT;
SET M=N-1;
  RETURN (
      # Write your MySQL query statement below.
      select distinct Salary from Employee order by Salary desc limit 1 offset M
  );
END