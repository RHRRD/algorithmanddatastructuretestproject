WITH my_table (student, class) as (
VALUES
('A', 'Math'),
('B', 'English'),
('C', 'Math'),
('D', 'Biology'),
('E', 'Math'),
('F', 'Computer'),
('G', 'Math'),
('A', 'Math'),
('A', 'Math')
) select t.class from (select distinct student, class from my_table) as t group by t.class having count(*) >= 5;