--https://leetcode.com/problems/consecutive-numbers/
--
--Create table If Not Exists Logs (id int, num int)
--Truncate table Logs
--insert into Logs (id, num) values ('1', '1')
--insert into Logs (id, num) values ('2', '1')
--insert into Logs (id, num) values ('3', '1')
--insert into Logs (id, num) values ('4', '2')
--insert into Logs (id, num) values ('5', '1')
--insert into Logs (id, num) values ('6', '2')
--insert into Logs (id, num) values ('7', '2')
--
--Table: Logs
--
--+-------------+---------+
--| Column Name | Type    |
--+-------------+---------+
--| id          | int     |
--| num         | varchar |
--+-------------+---------+
--id is the primary key for this table.
--id is an autoincrement column.
--
--
--Write an SQL query to find all numbers that appear at least three times consecutively.
--
--Return the result table in any order.
--
--The query result format is in the following example.
--
--
--
--Example 1:
--
--Input:
--Logs table:
--+----+-----+
--| id | num |
--+----+-----+
--| 1  | 1   |
--| 2  | 1   |
--| 3  | 1   |
--| 4  | 2   |
--| 5  | 1   |
--| 6  | 2   |
--| 7  | 2   |
--+----+-----+
--Output:
--+-----------------+
--| ConsecutiveNums |
--+-----------------+
--| 1               |
--+-----------------+
--Explanation: 1 is the only number that appears consecutively for at least three times.
select
    distinct l.num as ConsecutiveNums
from Logs l
    left join Logs l_2 on l.id = l_2.id + 1
    left join Logs l_3 on l_2.id = l_3.id + 1
where l.num = l_2.num and l_2.num = l_3.num