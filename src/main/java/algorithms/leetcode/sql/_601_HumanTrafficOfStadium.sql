--https://leetcode.com/problems/human-traffic-of-stadium/
--
--Create table If Not Exists Stadium (id int, visit_date DATE NULL, people int)
--Truncate table Stadium
--insert into Stadium (id, visit_date, people) values ('1', '2017-01-01', '10')
--insert into Stadium (id, visit_date, people) values ('2', '2017-01-02', '109')
--insert into Stadium (id, visit_date, people) values ('3', '2017-01-03', '150')
--insert into Stadium (id, visit_date, people) values ('4', '2017-01-04', '99')
--insert into Stadium (id, visit_date, people) values ('5', '2017-01-05', '145')
--insert into Stadium (id, visit_date, people) values ('6', '2017-01-06', '1455')
--insert into Stadium (id, visit_date, people) values ('7', '2017-01-07', '199')
--insert into Stadium (id, visit_date, people) values ('8', '2017-01-09', '188')
--
--Table: Stadium
--
--+---------------+---------+
--| Column Name   | Type    |
--+---------------+---------+
--| id            | int     |
--| visit_date    | date    |
--| people        | int     |
--+---------------+---------+
--visit_date is the primary key for this table.
--Each row of this table contains the visit date and visit id to the stadium with the number of people during the visit.
--No two rows will have the same visit_date, and as the id increases, the dates increase as well.
--
--
--Write an SQL query to display the records with three or more rows with consecutive id's, and the number of people is greater than or equal to 100 for each.
--
--Return the result table ordered by visit_date in ascending order.
--
--The query result format is in the following example.
--
--
--
--Example 1:
--
--Input:
--Stadium table:
--+------+------------+-----------+
--| id   | visit_date | people    |
--+------+------------+-----------+
--| 1    | 2017-01-01 | 10        |
--| 2    | 2017-01-02 | 109       |
--| 3    | 2017-01-03 | 150       |
--| 4    | 2017-01-04 | 99        |
--| 5    | 2017-01-05 | 145       |
--| 6    | 2017-01-06 | 1455      |
--| 7    | 2017-01-07 | 199       |
--| 8    | 2017-01-09 | 188       |
--+------+------------+-----------+
--Output:
--+------+------------+-----------+
--| id   | visit_date | people    |
--+------+------------+-----------+
--| 5    | 2017-01-05 | 145       |
--| 6    | 2017-01-06 | 1455      |
--| 7    | 2017-01-07 | 199       |
--| 8    | 2017-01-09 | 188       |
--+------+------------+-----------+
--Explanation:
--The four rows with ids 5, 6, 7, and 8 have consecutive ids and each of them has >= 100 people attended. Note that row 8 was included even though the visit_date was not the next day after row 7.
--The rows with ids 2 and 3 are not included because we need at least three consecutive ids.
with tmp as (
  select
    s.id as s_1_id, s.visit_date as s_1_v_d, s.people as s_1_p,
    sub_1.id as s_2_id, sub_1.visit_date as s_2_v_d, sub_1.people as s_2_p,
    sub_2.id as s_3_id, sub_2.visit_date as s_3_v_d, sub_2.people as s_3_p
  from Stadium s
    join Stadium sub_1 on s.id + 1 = sub_1.id and sub_1.people >= 100
    join Stadium sub_2 on sub_1.id + 1 = sub_2.id and sub_2.people >= 100
  where s.people > 100
), res as (
  select s_1_id as id, s_1_v_d as visit_date, s_1_p as people from tmp
  union
  select s_2_id as id, s_2_v_d as visit_date, s_2_p as people from tmp
  union
  select s_3_id as id, s_3_v_d as visit_date, s_3_p as people from tmp
) select distinct id, visit_date, people from res order by id asc