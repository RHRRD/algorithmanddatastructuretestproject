WITH Customers (id, name) as (
VALUES
(1, 'Math'),
(2, 'English'),
(3, 'Math'),
(4, 'Biology')
), Orders (id, customerId) as (values
(1, 2),
(3, 4)
) select cust.name from Customers cust left join Orders ord on cust.id = ord.customerId where ord.customerId is null;