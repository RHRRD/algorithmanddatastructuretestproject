with Employee (Id, Name, Salary, DepartmentId) as (
VALUES
(1, 'Joe', 70000, 1),
(2, 'Jim', 90000, 1),
(3, 'Henry', 80000, 2),
(4, 'Sam', 60000, 2),
(5, 'Max', 90000, 1)
), Department (Id, Name) as (
VALUES
(1, 'IT'),
(2, 'Sales')
) select d.NAme as Department, em.Name as Employee, em.Salary as Salary
from Employee em join Department d on em.departmentId = d.id
where (departmentId, salary) in (select DepartmentId, max(salary) from Employee group by departmentId)
order by em.salary desc;
