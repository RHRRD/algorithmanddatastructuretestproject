package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

/**
 * https://leetcode.com/problems/convert-sorted-array-to-binary-search-tree/description/
 * <p>
 * Given an integer array nums where the elements are sorted in ascending order, convert it to a
 * height-balanced
 * binary search tree.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: nums = [-10,-3,0,5,9]
 * Output: [0,-3,9,-10,null,5]
 * Explanation: [0,-10,5,null,-3,null,9] is also accepted:
 * <p>
 * Example 2:
 * <p>
 * <p>
 * Input: nums = [1,3]
 * Output: [3,1]
 * Explanation: [1,null,3] and [3,1] are both height-balanced BSTs.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 10^4
 * -10^4 <= nums[i] <= 10^4
 * nums is sorted in a strictly increasing order.
 */
public class _108_ConvertSortedArrayToBinarySearchTree {

    public static TreeNode sortedArrayToBST(int[] nums) {
        return sortedArrayToBST(nums, 0, nums.length - 1);
    }

    private static TreeNode sortedArrayToBST(int[] nums, int left, int right) {
        if (right - left == 2) {
            return new TreeNode(nums[left + 1], new TreeNode(nums[left]), new TreeNode(nums[right]));
        } else if (right - left == 1) {
            return new TreeNode(nums[right], new TreeNode(nums[left]), null);
        } else if (right - left == 0) {
            return new TreeNode(nums[left]);
        } else {
            int mid = left + (right - left) / 2;
            return new TreeNode(nums[mid], sortedArrayToBST(nums, left, mid - 1), sortedArrayToBST(nums, mid + 1, right));
        }
    }

    public static void main(String[] args) {
        System.out.println(sortedArrayToBST(new int[]{-10, -3, 0, 5, 9}));

        System.out.println(sortedArrayToBST(new int[]{1, 3}));

        System.out.println(sortedArrayToBST(new int[]{1}));

        System.out.println(sortedArrayToBST(new int[]{1, 2, 3}));

        System.out.println(sortedArrayToBST(new int[]{0, 1, 2, 3, 4, 5, 6, 7}));
    }
}