package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/number-of-strings-that-appear-as-substrings-in-word/
 * <p>
 * Given an array of strings patterns and a string word, return the number of strings in patterns that exist as a substring in word.
 * <p>
 * A substring is a contiguous sequence of characters within a string.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: patterns = ["a","abc","bc","d"], word = "abc"
 * Output: 3
 * Explanation:
 * - "a" appears as a substring in "abc".
 * - "abc" appears as a substring in "abc".
 * - "bc" appears as a substring in "abc".
 * - "d" does not appear as a substring in "abc".
 * 3 of the strings in patterns appear as a substring in word.
 * Example 2:
 * <p>
 * Input: patterns = ["a","b","c"], word = "aaaaabbbbb"
 * Output: 2
 * Explanation:
 * - "a" appears as a substring in "aaaaabbbbb".
 * - "b" appears as a substring in "aaaaabbbbb".
 * - "c" does not appear as a substring in "aaaaabbbbb".
 * 2 of the strings in patterns appear as a substring in word.
 * Example 3:
 * <p>
 * Input: patterns = ["a","a","a"], word = "ab"
 * Output: 3
 * Explanation: Each of the patterns appears as a substring in word "ab".
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= patterns.length <= 100
 * 1 <= patterns[i].length <= 100
 * 1 <= word.length <= 100
 * patterns[i] and word consist of lowercase English letters.
 */
public class _1967_NumberOfStringsThatAppearAsSubstringsInWord {

    public static int numOfStrings(String[] patterns, String word) {
        int res = 0;
        for (String tmp : patterns) {
            if (word.contains(tmp)) {
                res++;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(3, numOfStrings(new String[]{"a", "abc", "bc", "d"}, "abc"));
        CheckAnswerUtil.check(2, numOfStrings(new String[]{"a", "b", "c"}, "aaaaabbbbb"));
        CheckAnswerUtil.check(3, numOfStrings(new String[]{"a", "a", "a"}, "ab"));
        CheckAnswerUtil.check(0, numOfStrings(new String[]{"a", "a", "a"}, "bcd"));
    }
}
