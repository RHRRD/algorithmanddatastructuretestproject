package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.ArrayList;

/**
 * https://leetcode.com/problems/check-completeness-of-a-binary-tree/
 * <p>
 * Given the root of a binary tree, determine if it is a complete binary tree.
 * <p>
 * In a complete binary tree, every level, except possibly the last, is completely filled, and all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [1,2,3,4,5,6]
 * Output: true
 * Explanation: Every level before the last is full (ie. levels with node-values {1} and {2, 3}), and all nodes in the last level ({4, 5, 6}) are as far left as possible.
 * Example 2:
 * <p>
 * <p>
 * Input: root = [1,2,3,4,5,null,7]
 * Output: false
 * Explanation: The node with value 7 isn't as far left as possible.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 100].
 * 1 <= Node.val <= 1000
 */
public class _958_CheckCompletenessOfABinaryTree {

    public static boolean isCompleteTree(TreeNode root) {
        if (root.left == null && root.right == null) {
            return true;
        }
        int depth = getDepth(root, 1);
        boolean fullBeforeDepth = isFullBeforeDepth(root, depth, 1);
        if (!fullBeforeDepth) {
            return false;
        }
        ArrayList<Integer> list = new ArrayList<>();
        isCompleteTree(root, depth, 1, list);
        boolean isNull = false;
        for (Integer num : list) {
            if (num == null) {
                isNull = true;
            } else {
                if (isNull) {
                    return false;
                }
            }
        }
        return true;
    }

    private static void isCompleteTree(TreeNode node, int maxDepth, int curDepth, ArrayList<Integer> list) {
        if (curDepth == maxDepth) {
            if (node == null) {
                list.add(null);
            } else {
                list.add(node.val);
            }
            return;
        }
        isCompleteTree(node.left, maxDepth, curDepth + 1, list);
        isCompleteTree(node.right, maxDepth, curDepth + 1, list);
    }

    private static boolean isFullBeforeDepth(TreeNode node, int maxDepth, int curDepth) {
        if (curDepth < maxDepth && node == null) {
            return false;
        }
        if (curDepth == maxDepth - 1 && node.left == null && node.right != null) {
            return false;
        }
        if (curDepth == maxDepth - 1) {
            return true;
        }
        return isFullBeforeDepth(node.left, maxDepth, curDepth + 1) && isFullBeforeDepth(node.right, maxDepth, curDepth + 1);
    }

    private static int getDepth(TreeNode node, int h) {
        if (node == null) {
            return h - 1;
        }
        if (node.left == null && node.right == null) {
            return h;
        }
        return Math.max(getDepth(node.left, h + 1), getDepth(node.right, h + 1));
    }

    public static void main(String[] args) {
//        CheckAnswerUtil.check(true, isCompleteTree(new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, new TreeNode(6), null))));

        CheckAnswerUtil.check(true, isCompleteTree(new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, null, null))));

        CheckAnswerUtil.check(false, isCompleteTree(new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, null, new TreeNode(7)))));

        CheckAnswerUtil.check(false, isCompleteTree(new TreeNode(1, new TreeNode(2, new TreeNode(4), null), new TreeNode(3, null, new TreeNode(7)))));

        CheckAnswerUtil.check(false, isCompleteTree(new TreeNode(1, new TreeNode(2, new TreeNode(4), null), new TreeNode(3, new TreeNode(6), new TreeNode(7)))));

        CheckAnswerUtil.check(false, isCompleteTree(new TreeNode(1, new TreeNode(2, new TreeNode(4), null), new TreeNode(3, new TreeNode(6), null))));

        CheckAnswerUtil.check(true, isCompleteTree(new TreeNode(1)));
    }
}
