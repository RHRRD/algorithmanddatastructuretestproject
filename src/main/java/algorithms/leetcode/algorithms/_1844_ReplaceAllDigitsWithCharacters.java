package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/replace-all-digits-with-characters/
 * <p>
 * You are given a 0-indexed string s that has lowercase English letters in its even indices and digits in its odd indices.
 * <p>
 * There is a function shift(c, x), where c is a character and x is a digit, that returns the xth character after c.
 * <p>
 * For example, shift('a', 5) = 'f' and shift('x', 0) = 'x'.
 * For every odd index i, you want to replace the digit s[i] with shift(s[i-1], s[i]).
 * <p>
 * Return s after replacing all digits. It is guaranteed that shift(s[i-1], s[i]) will never exceed 'z'.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "a1c1e1"
 * Output: "abcdef"
 * Explanation: The digits are replaced as follows:
 * - s[1] -> shift('a',1) = 'b'
 * - s[3] -> shift('c',1) = 'd'
 * - s[5] -> shift('e',1) = 'f'
 * Example 2:
 * <p>
 * Input: s = "a1b2c3d4e"
 * Output: "abbdcfdhe"
 * Explanation: The digits are replaced as follows:
 * - s[1] -> shift('a',1) = 'b'
 * - s[3] -> shift('b',2) = 'd'
 * - s[5] -> shift('c',3) = 'f'
 * - s[7] -> shift('d',4) = 'h'
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 100
 * s consists only of lowercase English letters and digits.
 * shift(s[i-1], s[i]) <= 'z' for all odd indices i.
 */
public class _1844_ReplaceAllDigitsWithCharacters {

    public static String replaceDigits(String s) {
        char[] array = s.toCharArray();
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (i % 2 == 0) {
                res.append(array[i]);
            } else {
                res.append((char) (array[i - 1] + Integer.parseInt(String.valueOf(array[i]))));
            }
        }
        return res.toString();
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check("abcdef", replaceDigits("a1c1e1"), "a1c1e1");
        CheckAnswerUtil.check("abbdcfdhe", replaceDigits("a1b2c3d4e"), "a1b2c3d4e");
    }
}
