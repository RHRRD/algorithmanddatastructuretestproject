package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * https://leetcode.com/problems/binary-tree-level-order-traversal-ii/
 * <p>
 * Given the root of a binary tree, return the bottom-up level order traversal of its nodes' values. (i.e., from left to right, level by level from leaf to root).
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [3,9,20,null,null,15,7]
 * Output: [[15,7],[9,20],[3]]
 * Example 2:
 * <p>
 * Input: root = [1]
 * Output: [[1]]
 * Example 3:
 * <p>
 * Input: root = []
 * Output: []
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [0, 2000].
 * -1000 <= Node.val <= 1000
 */
public class _107_BinaryTreeLevelOrderTraversalII {

    public static List<List<Integer>> levelOrderBottom(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrderBottom(root, result, 1);
        Collections.reverse(result);
        return result;
    }

    private static void levelOrderBottom(TreeNode root, List<List<Integer>> result, int level) {
        if (root == null) {
            return;
        }
        if (result.size() < level) {
            ArrayList<Integer> tmp = new ArrayList<>();
            tmp.add(root.val);
            result.add(tmp);
        } else {
            List<Integer> list = result.get(level - 1);
            list.add(root.val);
        }
        levelOrderBottom(root.left, result, level + 1);
        levelOrderBottom(root.right, result, level + 1);
    }

    public static void main(String[] args) {
        System.out.println(levelOrderBottom(new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)))));

        System.out.println(levelOrderBottom(new TreeNode(1)));

        System.out.println(levelOrderBottom(null));

        System.out.println(levelOrderBottom(new TreeNode(1, null, new TreeNode(3))));

        System.out.println(levelOrderBottom(new TreeNode(1, new TreeNode(2), null)));

        System.out.println(levelOrderBottom(new TreeNode(1, new TreeNode(2, new TreeNode(4), null), new TreeNode(3, new TreeNode(5, new TreeNode(6), null), null))));

        System.out.println(levelOrderBottom(new TreeNode(1, new TreeNode(2, new TreeNode(4, new TreeNode(8), new TreeNode(9)), new TreeNode(5, new TreeNode(10), new TreeNode(11))), new TreeNode(3, new TreeNode(6, new TreeNode(12), new TreeNode(13)), new TreeNode(7, new TreeNode(14), new TreeNode(15))))));
    }
}
