package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/permutation-in-string/
 * <p>
 * Given two strings s1 and s2, return true if s2 contains a permutation of s1, or false otherwise.
 * <p>
 * In other words, return true if one of s1's permutations is the substring of s2.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s1 = "ab", s2 = "eidbaooo"
 * Output: true
 * Explanation: s2 contains one permutation of s1 ("ba").
 * Example 2:
 * <p>
 * Input: s1 = "ab", s2 = "eidboaoo"
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s1.length, s2.length <= 10^4
 * s1 and s2 consist of lowercase English letters.
 */
public class _567_PermutationInString {

    public static boolean checkInclusion(String s1, String s2) {
        if (s1.length() > s2.length()) {
            return false;
        }
        char[] s1Array = new char[26];
        char[] s2Array = new char[26];
        for (int i = 0; i < s1.length(); i++) {
            s1Array[s1.charAt(i) - 'a']++;
        }
        for (int i = 0; i < s1.length(); i++) {
            s2Array[s2.charAt(i) - 'a']++;
        }
        if (eq(s1Array, s2Array)) {
            return true;
        }
        for (int i = 1; i < s2.length() - s1.length() + 1; i++) {
            s2Array[s2.charAt(i - 1) - 'a']--;
            s2Array[s2.charAt(i + s1.length() - 1) - 'a']++;
            if (eq(s1Array, s2Array)) {
                return true;
            }
        }
        return false;
    }

    private static boolean eq(char[] s1, char[] s2) {
        for (int i = 0; i < s1.length; i++) {
            if (s1[i] != s2[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, checkInclusion("ab", "eidbaooo"));
        CheckAnswerUtil.check(true, checkInclusion("ab", "eidoooba"));
        CheckAnswerUtil.check(false, checkInclusion("ab", "eidboaoo"));
    }
}
