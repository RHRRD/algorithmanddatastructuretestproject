package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/add-digits/
 * <p>
 * Given an integer num, repeatedly add all its digits until the result has only one digit, and return it.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: num = 38
 * Output: 2
 * Explanation: The process is
 * 38 --> 3 + 8 --> 11
 * 11 --> 1 + 1 --> 2
 * Since 2 has only one digit, return it.
 * Example 2:
 * <p>
 * Input: num = 0
 * Output: 0
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 0 <= num <= 2^31 - 1
 */
public class _258_AddDigits {

    public static int addDigits(int num) {
        if (num / 10 == 0) {
            return num;
        }
        int res = 0;
        do {
            res += num % 10;
            num /= 10;
        } while (num != 0);

        return addDigits(res);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, addDigits(38), 38);
        CheckAnswerUtil.check(0, addDigits(0), 0);
        CheckAnswerUtil.check(1, addDigits(1), 1);
        CheckAnswerUtil.check(9, addDigits(9), 9);
        CheckAnswerUtil.check(1, addDigits(10), 10);
        CheckAnswerUtil.check(6, addDigits(123), 123);
        CheckAnswerUtil.check(1, addDigits(1234), 1234);
    }
}
