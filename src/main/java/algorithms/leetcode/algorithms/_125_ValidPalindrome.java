package algorithms.leetcode.algorithms;

public class _125_ValidPalindrome {

    public static void main(String[] args) {
        assert isPalindrome("A man, a plan, a canal: Panama");
        assert isPalindrome("");
        assert !isPalindrome("race a car");
        assert isPalindrome("ab@a");
    }

    private static boolean isPalindrome(String s) {
        s = s.toLowerCase().replaceAll("[^A-Za-z0-9]","").trim();
        return new StringBuilder(s).reverse().toString().equals(s);
    }


}
