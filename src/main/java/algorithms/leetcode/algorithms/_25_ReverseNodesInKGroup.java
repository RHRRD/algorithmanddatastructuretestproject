package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

/**
 * https://leetcode.com/problems/reverse-nodes-in-k-group/
 * <p>
 * Given the head of a linked list, reverse the nodes of the list k at a time, and return the modified list.
 * <p>
 * k is a positive integer and is less than or equal to the length of the linked list. If the number of nodes is not a multiple of k then left-out nodes, in the end, should remain as it is.
 * <p>
 * You may not alter the values in the list's nodes, only nodes themselves may be changed.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [2,1,4,3,5]
 * Example 2:
 * <p>
 * <p>
 * Input: head = [1,2,3,4,5], k = 3
 * Output: [3,2,1,4,5]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is n.
 * 1 <= k <= n <= 5000
 * 0 <= Node.val <= 1000
 * <p>
 * <p>
 * Follow-up: Can you solve the problem in O(1) extra memory space?
 */
public class _25_ReverseNodesInKGroup {

    public static ListNode reverseKGroup(ListNode head, int k) {
        if (head == null || head.next == null || k == 1) {
            return head;
        }
        ListNode current = head;
        ListNode newHead = null;
        while (current != null) {
            ListNode startOfSublist = current;
            ListNode currentSublist = current;
            int i = 1;
            while (i < k && currentSublist != null) {
                currentSublist = currentSublist.next;
                i++;
            }
            if (currentSublist == null) {
                addToEnd(current, newHead);
                break;
            }
            ListNode tt = currentSublist.next;
            currentSublist.next = null;
            if (newHead == null) {
                newHead = reverseList(startOfSublist);
            } else {
                addToEnd(reverseList(startOfSublist), newHead);
            }
            current = tt;
        }
        return newHead;
    }

    private static void addToEnd(ListNode nextNode, ListNode headNode) {
        ListNode node = headNode;
        while (node.next != null) {
            node = node.next;
        }
        node.next = nextNode;
    }

    private static ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;
        }
        return newHead;
    }

    public static void main(String[] args) {
        System.out.println("2->1->4->3->5 : " + reverseKGroup(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 2));
        System.out.println("3->2->1->4->5 : " + reverseKGroup(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 3));
        System.out.println("1->2->3->4->5 : " + reverseKGroup(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 1));
        System.out.println("4->3->2->1->5 : " + reverseKGroup(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 4));
        System.out.println("5->4->3->2->1 : " + reverseKGroup(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 5));
    }
}
