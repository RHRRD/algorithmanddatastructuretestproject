package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

/**
 * https://leetcode.com/problems/root-equals-sum-of-children/
 * <p>
 * You are given the root of a binary tree that consists of exactly 3 nodes: the root, its left child, and its right child.
 * <p>
 * Return true if the value of the root is equal to the sum of the values of its two children, or false otherwise.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [10,4,6]
 * Output: true
 * Explanation: The values of the root, its left child, and its right child are 10, 4, and 6, respectively.
 * 10 is equal to 4 + 6, so we return true.
 * Example 2:
 * <p>
 * <p>
 * Input: root = [5,3,1]
 * Output: false
 * Explanation: The values of the root, its left child, and its right child are 5, 3, and 1, respectively.
 * 5 is not equal to 3 + 1, so we return false.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The tree consists only of the root, its left child, and its right child.
 * -100 <= Node.val <= 100
 */
public class _2236_RootEqualsSumOfChildren {

    public static boolean checkTree(TreeNode root) {
        return root.val == root.left.val + root.right.val;
    }

    public static void main(String[] args) {
        System.out.println(checkTree(new TreeNode(10, new TreeNode(4), new TreeNode(6))));
        System.out.println(true);
        System.out.println();

        System.out.println(checkTree(new TreeNode(5, new TreeNode(3), new TreeNode(1))));
        System.out.println(false);
    }
}
