package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class _1078_OccurrencesAfterBigram {

    public static void main(String[] args) {
        assert Arrays.equals(new String[]{"we", "rock"}, findOcurrences("we will we will rock you", "we", "will"));
    }

    private static String[] findOcurrences(String text, String first, String second) {
        if (text.isEmpty()) return new String[]{};
        String[] split = text.split(" ");
        if (split.length < 2) return new String[]{};
        List<String> list = new ArrayList<>();
        for (int i = 2; i < split.length; i++) {
            if (split[i - 2].equals(first) && split[i - 1].equals(second)) {
                list.add(split[i]);
            }
        }

        return list.stream().toArray(String[]::new);
    }

}
