package algorithms.leetcode.algorithms;

public class _1309_DecryptStringFromAlphabetToIntegerMapping {

    public String freqAlphabets(String s) {
        StringBuilder dec = new StringBuilder();
        StringBuilder buffer = new StringBuilder();
        int count = 0;
        boolean flag = false;
        char[] array = s.toCharArray();
        for (int i = array.length - 1; i >= 0; i--) {
            if (array[i] == '#') {
                flag = true;
            } else if (flag && count < 2) {
                count++;
                buffer.append(array[i]);
            } else {
                flag = false;
                int parseInt = Integer.parseInt(String.valueOf(array[i]));
                dec.append((char) (96 + parseInt));
            }
            if (count == 2) {
                int parseInt = Integer.parseInt(buffer.reverse().toString());
                dec.append((char) (96 + parseInt));
                count = 0;
                flag = false;
                buffer = new StringBuilder();
            }
        }

        return dec.reverse().toString();
    }
}
