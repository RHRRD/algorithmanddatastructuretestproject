package algorithms.leetcode.algorithms;

import java.util.Arrays;

public class _922_SortArrayByParityII {

    public static void main(String[] args) {
        int[] array = sortArrayByParityII(new int[]{4, 2, 5, 7});
        System.out.println(Arrays.toString(array));
    }

    private static int[] sortArrayByParityII(int[] A) {
        int odd = 1, even = 0;
        int[] res = new int[A.length];
        for (int i = 0; i < A.length; i++) {
            if (A[i] % 2 == 0) {
                res[even] = A[i];
                even += 2;
            } else {
                res[odd] = A[i];
                odd += 2;
            }
        }
        return res;
    }
}
