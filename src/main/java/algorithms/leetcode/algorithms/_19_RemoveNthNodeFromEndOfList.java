package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

/**
 * https://leetcode.com/problems/remove-nth-node-from-end-of-list/
 * <p>
 * Given the head of a linked list, remove the nth node from the end of the list and return its head.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,2,3,4,5], n = 2
 * Output: [1,2,3,5]
 * Example 2:
 * <p>
 * Input: head = [1], n = 1
 * Output: []
 * Example 3:
 * <p>
 * Input: head = [1,2], n = 1
 * Output: [1]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is sz.
 * 1 <= sz <= 30
 * 0 <= Node.val <= 100
 * 1 <= n <= sz
 * <p>
 * <p>
 * Follow up: Could you do this in one pass?
 */
public class _19_RemoveNthNodeFromEndOfList {

    public static ListNode removeNthFromEnd(ListNode head, int n) {
        if (head.next == null) {
            return null;
        }
        ListNode prev = null;
        ListNode current = head;
        ListNode node = head;
        while (n != 0 && node != null) {
            node = node.next;
            n--;
        }
        while (node != null) {
            node = node.next;
            prev = current;
            current = current.next;
        }
        if (prev == null) {
            head = head.next;
        } else {
            prev.next = current.next;
        }
        return head;
    }

    public static void main(String[] args) {
        System.out.println("1->2->3->5 : " + removeNthFromEnd(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 2));
        System.out.println("null : " + removeNthFromEnd(new ListNode(1), 1));
        System.out.println("1 : " + removeNthFromEnd(new ListNode(1, new ListNode(2)), 1));
        System.out.println("1->2->3->4 : " + removeNthFromEnd(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 1));
        System.out.println("1->2->4->5 : " + removeNthFromEnd(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 3));
        System.out.println("1->3->4->5 : " + removeNthFromEnd(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 4));
        System.out.println("2->3->4->5 : " + removeNthFromEnd(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))), 5));
    }
}
