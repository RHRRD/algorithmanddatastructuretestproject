package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * https://leetcode.com/problems/check-if-all-characters-have-equal-number-of-occurrences/
 * <p>
 * Given a string s, return true if s is a good string, or false otherwise.
 * <p>
 * A string s is good if all the characters that appear in s have the same number of occurrences (i.e., the same frequency).
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "abacbc"
 * Output: true
 * Explanation: The characters that appear in s are 'a', 'b', and 'c'. All characters occur 2 times in s.
 * Example 2:
 * <p>
 * Input: s = "aaabb"
 * Output: false
 * Explanation: The characters that appear in s are 'a' and 'b'.
 * 'a' occurs 3 times while 'b' occurs 2 times, which is not the same number of times.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 1000
 * s consists of lowercase English letters.
 */
public class _1941_CheckIfAllCharactersHaveEqualNumberOfOccurrences {

    public static boolean areOccurrencesEqual(String s) {
        Map<Character, Integer> map = new HashMap<>();
        for (Character ch : s.toCharArray()) {
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }
        return new HashSet<>(map.values()).size() == 1;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, areOccurrencesEqual("abacbc"), "abacbc");
        CheckAnswerUtil.check(false, areOccurrencesEqual("aaabb"), "aaabb");
        CheckAnswerUtil.check(true, areOccurrencesEqual("a"), "a");
    }
}
