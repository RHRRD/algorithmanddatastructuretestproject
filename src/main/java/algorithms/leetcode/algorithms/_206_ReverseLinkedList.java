package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

import javax.print.attribute.standard.PresentationDirection;

/**
 * https://leetcode.com/problems/reverse-linked-list/
 * <p>
 * Given the head of a singly linked list, reverse the list, and return the reversed list.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,2,3,4,5]
 * Output: [5,4,3,2,1]
 * Example 2:
 * <p>
 * <p>
 * Input: head = [1,2]
 * Output: [2,1]
 * Example 3:
 * <p>
 * Input: head = []
 * Output: []
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is the range [0, 5000].
 * -5000 <= Node.val <= 5000
 */
public class _206_ReverseLinkedList {

    public static ListNode reverseList2(ListNode head) {
        if (head == null) {
            return null;
        }
        ListNode newHead = new ListNode(head.val);
        ListNode currentNode = head.next;
        while (currentNode != null) {
            ListNode newNode = new ListNode(currentNode.val);
            newNode.next = newHead;
            newHead = newNode;
            currentNode = currentNode.next;
        }
        return newHead;
    }

    public static ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = null;
        while (head != null) {
            ListNode next = head.next;
            head.next = newHead;
            newHead = head;
            head = next;
        }
        return newHead;
    }

    public static void main(String[] args) {
        print(reverseList(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))))));
        print(new ListNode(5, new ListNode(4, new ListNode(3, new ListNode(2, new ListNode(1))))));
        System.out.println();

        print(reverseList(new ListNode(1, new ListNode(2))));
        print(new ListNode(2, new ListNode(1)));
        System.out.println();

        print(reverseList(new ListNode()));
        print(new ListNode());
    }

    private static void print(ListNode head) {
        if (head == null) {
            System.out.println("null");
            return;
        }
        ListNode current = head;
        do {
            System.out.print(current.val + " ");
            current = current.next;
        } while (current != null);
        System.out.println();
    }

}
