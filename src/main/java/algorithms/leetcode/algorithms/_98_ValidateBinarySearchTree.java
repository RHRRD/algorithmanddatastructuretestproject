package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/validate-binary-search-tree/description/
 * <p>
 * Given the root of a binary tree, determine if it is a valid binary search tree (BST).
 * <p>
 * A valid BST is defined as follows:
 * <p>
 * The left
 * subtree
 * of a node contains only nodes with keys less than the node's key.
 * The right subtree of a node contains only nodes with keys greater than the node's key.
 * Both the left and right subtrees must also be binary search trees.
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [2,1,3]
 * Output: true
 * Example 2:
 * <p>
 * <p>
 * Input: root = [5,1,4,null,null,3,6]
 * Output: false
 * Explanation: The root node's value is 5 but its right child's value is 4.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 104].
 * -2^31 <= Node.val <= 2^31 - 1
 */
public class _98_ValidateBinarySearchTree {

    public static boolean isValidBST(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) {
            return true;
        }
        if (root.left != null) {
            boolean res = isValidBSTLeft(root.left, root.val);
            if (!res) {
                return false;
            }
        }
        if (root.right != null) {
            boolean res = isValidBSTRight(root.right, root.val);
            if (!res) {
                return false;
            }
        }
        return isValidBST(root.left) && isValidBST(root.right);
    }

    private static boolean isValidBSTRight(TreeNode right, int val) {
        if (right == null) {
            return true;
        }
        return right.val > val && isValidBSTRight(right.left, val) && isValidBSTRight(right.right, val);
    }

    private static boolean isValidBSTLeft(TreeNode left, int val) {
        if (left == null) {
            return true;
        }
        return left.val < val && isValidBSTLeft(left.left, val) && isValidBSTLeft(left.right, val);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, isValidBST(new TreeNode(2, new TreeNode(1), new TreeNode(3))));

        CheckAnswerUtil.check(true, isValidBST(null));

        CheckAnswerUtil.check(true, isValidBST(new TreeNode(1)));

        CheckAnswerUtil.check(false, isValidBST(new TreeNode(1, new TreeNode(0), new TreeNode(4, new TreeNode(-1), new TreeNode(5)))));

        CheckAnswerUtil.check(false, isValidBST(new TreeNode(5, new TreeNode(1), new TreeNode(4, new TreeNode(3), new TreeNode(6)))));

        CheckAnswerUtil.check(true, isValidBST(new TreeNode(3, new TreeNode(1, new TreeNode(0), new TreeNode(2)), new TreeNode(5, new TreeNode(4), new TreeNode(6)))));
    }
}
