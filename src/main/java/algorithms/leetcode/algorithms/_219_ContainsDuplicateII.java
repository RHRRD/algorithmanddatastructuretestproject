package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/contains-duplicate-ii/
 * <p>
 * Given an integer array nums and an integer k, return true if there are two distinct indices i and j in the array such that nums[i] == nums[j] and abs(i - j) <= k.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [1,2,3,1], k = 3
 * Output: true
 * Example 2:
 * <p>
 * Input: nums = [1,0,1,1], k = 1
 * Output: true
 * Example 3:
 * <p>
 * Input: nums = [1,2,3,1,2,3], k = 2
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 10^5
 * -10^9 <= nums[i] <= 10^9
 * 0 <= k <= 10^5
 */
public class _219_ContainsDuplicateII {

    public static boolean containsNearbyDuplicate(int[] nums, int k) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j <= i + k && j < nums.length; j++) {
                if (nums[i] == nums[j]) {
                    return true;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, containsNearbyDuplicate(new int[]{99, 99}, 2));
        CheckAnswerUtil.check(false, containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 0));
        CheckAnswerUtil.check(true, containsNearbyDuplicate(new int[]{1, 2, 3, 1}, 3));
        CheckAnswerUtil.check(true, containsNearbyDuplicate(new int[]{1, 0, 1, 1}, 1));
        CheckAnswerUtil.check(false, containsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3,}, 2));
        CheckAnswerUtil.check(true, containsNearbyDuplicate(new int[]{1, 2, 3, 1, 2, 3,}, 6));
    }
}
