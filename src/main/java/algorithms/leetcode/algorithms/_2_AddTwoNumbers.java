package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

/**
 * https://leetcode.com/problems/add-two-numbers/
 * <p>
 * You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
 * <p>
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: l1 = [2,4,3], l2 = [5,6,4]
 * Output: [7,0,8]
 * Explanation: 342 + 465 = 807.
 * Example 2:
 * <p>
 * Input: l1 = [0], l2 = [0]
 * Output: [0]
 * Example 3:
 * <p>
 * Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * Output: [8,9,9,9,0,0,0,1]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in each linked list is in the range [1, 100].
 * 0 <= Node.val <= 9
 * It is guaranteed that the list represents a number that does not have leading zeros.
 */
public class _2_AddTwoNumbers {

    public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode head, current = new ListNode();
        head = current;
        int currentVal = 0;
        ListNode list1 = l1, list2 = l2;
        while (true) {
            if (list1 == null && list2 == null) {
                if (currentVal != 0) {
                    current.next = new ListNode(currentVal);
                }
                return head.next;
            }
            if (list1 == null) {
                currentVal += list2.val;
                list2 = list2.next;
            } else if (list2 == null) {
                currentVal += list1.val;
                list1 = list1.next;
            } else {
                currentVal += list1.val + list2.val;
                list1 = list1.next;
                list2 = list2.next;
            }
            current.next = new ListNode(currentVal % 10);
            current = current.next;
            currentVal = currentVal / 10;
        }
    }

    public static void main(String[] args) {
        System.out.println(addTwoNumbers(
                new ListNode(2, new ListNode(4, new ListNode(3))),
                new ListNode(5, new ListNode(6, new ListNode(4)))
        ));
        System.out.println(new ListNode(7, new ListNode(0, new ListNode(8))));
        System.out.println();

        System.out.println(addTwoNumbers(
                new ListNode(2, new ListNode(4)),
                new ListNode(5, new ListNode(6, new ListNode(4)))
        ));
        System.out.println(new ListNode(7, new ListNode(0, new ListNode(5))));
        System.out.println();

        System.out.println(addTwoNumbers(
                new ListNode(2, new ListNode(4, new ListNode(3))),
                new ListNode(5, new ListNode(6))
        ));
        System.out.println(new ListNode(7, new ListNode(0, new ListNode(4))));
        System.out.println();

        System.out.println(addTwoNumbers(new ListNode(0), new ListNode(0)));
        System.out.println(new ListNode(0));
    }
}
