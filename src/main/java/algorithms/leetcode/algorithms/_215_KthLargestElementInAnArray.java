package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.Arrays;
import java.util.Random;

/**
 * https://leetcode.com/problems/kth-largest-element-in-an-array/
 * <p>
 * Given an integer array nums and an integer k, return the kth largest element in the array.
 * <p>
 * Note that it is the kth largest element in the sorted order, not the kth distinct element.
 * <p>
 * You must solve it in O(n) time complexity.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [3,2,1,5,6,4], k = 2
 * Output: 5
 * Example 2:
 * <p>
 * Input: nums = [3,2,3,1,2,4,5,5,6], k = 4
 * Output: 4
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= k <= nums.length <= 10^5
 * -10^4 <= nums[i] <= 10^4
 */
public class _215_KthLargestElementInAnArray {

    public static int findKthLargest(int[] nums, int k) {
        if (nums.length == 1 && k == 1) {
            return nums[0];
        }
        return quickSort(nums, nums.length - k);
    }

    private static int quickSort(int[] array, int k) {
        return quickSort(array, 0, array.length - 1, k);
    }

    private static int quickSort(int[] array, int left, int right, int k) {
        if (left >= right) {
            return array[k];
        }
        int pivot = array[right];
        int partition = partitionIt(array, left, right, pivot);
        if (partition == k) {
            return array[partition];
        }
        if (partition > k) {
            return quickSort(array, left, partition - 1, k);
        } else {
            return quickSort(array, partition + 1, right, k);
        }
    }

    private static int partitionIt(int[] array, int left, int right, int pivot) {
        int leftPtr = left - 1;
        int rightPrt = right;
        while (true) {
            while (array[++leftPtr] < pivot) ;
            while (rightPrt > 0 && array[--rightPrt] > pivot) ;

            if (leftPtr >= rightPrt) {
                break;
            }
            swap(array, leftPtr, rightPrt);
        }
        swap(array, leftPtr, right);
        return leftPtr;
    }

    private static void swap(int[] array, int leftPtr, int rightPrt) {
        int tmp = array[leftPtr];
        array[leftPtr] = array[rightPrt];
        array[rightPrt] = tmp;
    }

    private static int findKthLargestTest(int[] nums, int k) {
        Arrays.sort(nums);
        return nums[nums.length - k];
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(5, findKthLargest(new int[]{3, 2, 1, 5, 6, 4}, 2));

        CheckAnswerUtil.check(4, findKthLargest(new int[]{3, 2, 3, 1, 2, 4, 5, 5, 6}, 4));

        CheckAnswerUtil.check(10, findKthLargest(new int[]{10}, 1));

        CheckAnswerUtil.check(10, findKthLargest(new int[]{10, 10, 10, 10}, 1));
        CheckAnswerUtil.check(10, findKthLargest(new int[]{10, 10, 10, 10}, 2));
        CheckAnswerUtil.check(10, findKthLargest(new int[]{10, 10, 10, 10}, 3));
        CheckAnswerUtil.check(10, findKthLargest(new int[]{10, 10, 10, 10}, 4));

        Random rand = new Random();
        for (int i = 0; i < 10000; i++) {
            int[] arr = new int[rand.nextInt(100000) + 1];
            for (int j = 0; j < arr.length; j++) {
                arr[j] = rand.nextInt(10000) * (rand.nextBoolean() ? 1 : -1);
            }
            int k = rand.nextInt(arr.length);
            if (k == 0) {
                k++;
            }
            if (findKthLargestTest(arr, k) != findKthLargestTest(arr, k)) {
                System.out.println(Arrays.toString(arr) + " " + k);
                break;
            }
            if (i % 10 == 0) {
                System.out.println(i);
            }
        }
    }
}
