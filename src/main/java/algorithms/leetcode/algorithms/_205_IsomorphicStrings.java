package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/isomorphic-strings/
 * <p>
 * Given two strings s and t, determine if they are isomorphic.
 * <p>
 * Two strings s and t are isomorphic if the characters in s can be replaced to get t.
 * <p>
 * All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character, but a character may map to itself.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "egg", t = "add"
 * Output: true
 * Example 2:
 * <p>
 * Input: s = "foo", t = "bar"
 * Output: false
 * Example 3:
 * <p>
 * Input: s = "paper", t = "title"
 * Output: true
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 5 * 10^4
 * t.length == s.length
 * s and t consist of any valid ascii character.
 */
public class _205_IsomorphicStrings {

    public static boolean isIsomorphic(String s, String t) {
        if (s.equals(t)) {
            return true;
        }
        Map<Character, Character> mapS = new HashMap<>();
        Map<Character, Character> mapT = new HashMap<>();
        char[] arrayS = s.toCharArray();
        char[] arrayT = t.toCharArray();
        StringBuilder sbS = new StringBuilder();
        StringBuilder sbT = new StringBuilder();
        for (int i = 0; i < arrayS.length; i++) {
            mapS.put(arrayS[i], arrayT[i]);
            mapT.put(arrayT[i], arrayS[i]);
        }
        for (int i = 0; i < arrayS.length; i++) {
            sbS.append(mapS.get(arrayS[i]));
            sbT.append(mapT.get(arrayT[i]));
        }
        return sbS.toString().equals(t) && sbT.toString().equals(s);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, isIsomorphic("egg", "add"));
        CheckAnswerUtil.check(false, isIsomorphic("foo", "bar"));
        CheckAnswerUtil.check(true, isIsomorphic("paper", "title"));
        CheckAnswerUtil.check(false, isIsomorphic("badc", "baba"));
    }
}
