package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * https://leetcode.com/problems/letter-combinations-of-a-phone-number/
 * <p>
 * Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent. Return the answer in any order.
 * <p>
 * A mapping of digits to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.
 * <p>
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: digits = "23"
 * Output: ["ad","ae","af","bd","be","bf","cd","ce","cf"]
 * Example 2:
 * <p>
 * Input: digits = ""
 * Output: []
 * Example 3:
 * <p>
 * Input: digits = "2"
 * Output: ["a","b","c"]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 0 <= digits.length <= 4
 * digits[i] is a digit in the range ['2', '9'].
 */
public class _17_LetterCombinationsOfAPhoneNumber {

    private static Map<Character, List<String>> map = Map.of(
            '2', List.of("a", "b", "c"),
            '3', List.of("d", "e", "f"),
            '4', List.of("g", "h", "i"),
            '5', List.of("j", "k", "l"),
            '6', List.of("m", "n", "o"),
            '7', List.of("p", "q", "r", "s"),
            '8', List.of("t", "u", "v"),
            '9', List.of("w", "x", "y", "z")
    );

    public static List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<>();
        char[] array = digits.toCharArray();
        if (array.length > 0) {
            result.addAll(map.get(array[0]));
        }
        for (int i = 1; i < array.length; i++) {
            List<String> tmp = new ArrayList<>();
            for (int j = 0; j < result.size(); j++) {
                for (int k = 0; k < map.get(array[i]).size(); k++) {
                    tmp.add(result.get(j) + map.get(array[i]).get(k));
                }
            }
            result = tmp;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(letterCombinations("23"));
        System.out.println(List.of("ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"));
        System.out.println();

        System.out.println(letterCombinations(""));
        System.out.println(List.of());
        System.out.println();

        System.out.println(letterCombinations("2"));
        System.out.println(List.of("a", "b", "c"));
        System.out.println();

        System.out.println(letterCombinations("22"));
        System.out.println(List.of("aa", "ab", "ac", "ba", "bb", "bc", "ca", "cb", "cc"));
        System.out.println();

        System.out.println(letterCombinations("2345"));
        System.out.println();
    }
}
