package algorithms.leetcode.algorithms;

public class _1252_CellsWithOddValuesInAMatrix {

    public static void main(String[] args) {
        System.out.println(oddCells(2, 3, new int[][]{{0, 1}, {1, 1}}));
        System.out.println(oddCells(2, 2, new int[][]{{1, 1}, {0, 0}}));
    }

    private static int oddCells(int n, int m, int[][] indices) {
        int[][] tmp = new int[n][m];
        int count = 0;
        for (int[] point : indices) {
            for (int i = 0; i < m; i++) {
                tmp[point[0]][i]++;
            }
            for (int i = 0; i < n; i++) {
                tmp[i][point[1]]++;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (tmp[i][j] % 2 == 1) {
                    count++;
                }
            }
        }
        return count;
    }

}
