package algorithms.leetcode.algorithms;

public class _434_NumberOfSegmentsInAString {

    public static void main(String[] args) {
        assert 5 == countSegments("Hello, my name is John.");
        assert 0 == countSegments("");
        assert 0 == countSegments(null);
        assert 1 == countSegments("null");
        assert 6 == countSegments(", , , ,        a, eaefa");
    }

    private static int countSegments(String s) {
        if (s == null || s.isEmpty()) return 0;
        int count = 0;
        for (String str : s.split(" ")) {
            if (str.length() != 0) count++;
        }
        return count;
    }

}
