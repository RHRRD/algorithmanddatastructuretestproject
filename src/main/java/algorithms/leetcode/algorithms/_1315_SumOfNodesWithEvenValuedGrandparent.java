package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/sum-of-nodes-with-even-valued-grandparent/
 * <p>
 * Given the root of a binary tree, return the sum of values of nodes with an even-valued grandparent. If there are no nodes with an even-valued grandparent, return 0.
 * <p>
 * A grandparent of a node is the parent of its parent if it exists.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
 * Output: 18
 * Explanation: The red nodes are the nodes with even-value grandparent while the blue nodes are the even-value grandparents.
 * Example 2:
 * <p>
 * <p>
 * Input: root = [1]
 * Output: 0
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 104].
 * 1 <= Node.val <= 100
 */
public class _1315_SumOfNodesWithEvenValuedGrandparent {

    public static int sumEvenGrandparent(TreeNode root) {
        return sumEvenGrandparent(root, false, false);
    }

    public static int sumEvenGrandparent(TreeNode node, boolean evenParent, boolean evenGrandparent) {
        if (node == null) {
            return 0;
        }
        if (node.left == null && node.right == null) {
            return evenGrandparent ? node.val : 0;
        }
        if (evenGrandparent) {
            return node.val + sumEvenGrandparent(node.left, node.val % 2 == 0, evenParent) + sumEvenGrandparent(node.right, node.val % 2 == 0, evenParent);
        } else {
            return sumEvenGrandparent(node.left, node.val % 2 == 0, evenParent) + sumEvenGrandparent(node.right, node.val % 2 == 0, evenParent);
        }
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(18, sumEvenGrandparent(new TreeNode(6, new TreeNode(7, new TreeNode(2, new TreeNode(9), null), new TreeNode(7, new TreeNode(1), new TreeNode(4))), new TreeNode(8, new TreeNode(1), new TreeNode(3, null, new TreeNode(5))))));

        CheckAnswerUtil.check(0, sumEvenGrandparent(new TreeNode(1)));

        CheckAnswerUtil.check(0, sumEvenGrandparent(new TreeNode(1, new TreeNode(2), null)));
        CheckAnswerUtil.check(0, sumEvenGrandparent(new TreeNode(1, null, new TreeNode(2))));
        CheckAnswerUtil.check(0, sumEvenGrandparent(new TreeNode(1, new TreeNode(2), new TreeNode(2))));
        CheckAnswerUtil.check(0, sumEvenGrandparent(new TreeNode(1, new TreeNode(2, new TreeNode(1), null), new TreeNode(2))));
        CheckAnswerUtil.check(1, sumEvenGrandparent(new TreeNode(2, new TreeNode(2, new TreeNode(1), null), new TreeNode(2))));
        CheckAnswerUtil.check(2, sumEvenGrandparent(new TreeNode(2, new TreeNode(2, new TreeNode(1), null), new TreeNode(2, null, new TreeNode(1)))));
    }
}
