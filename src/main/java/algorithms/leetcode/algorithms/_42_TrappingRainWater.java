package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/trapping-rain-water/
 * <p>
 * Given n non-negative integers representing an elevation map where the width of each bar is 1, compute how much water it can trap after raining.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: height = [0,1,0,2,1,0,1,3,2,1,2,1]
 * Output: 6
 * Explanation: The above elevation map (black section) is represented by array [0,1,0,2,1,0,1,3,2,1,2,1]. In this case, 6 units of rain water (blue section) are being trapped.
 * Example 2:
 * <p>
 * Input: height = [4,2,0,3,2,5]
 * Output: 9
 * <p>
 * <p>
 * Constraints:
 * <p>
 * n == height.length
 * 1 <= n <= 2 * 10^4
 * 0 <= height[i] <= 10^5
 */
public class _42_TrappingRainWater {

    public static int trap(int[] height) {
        if (height.length <= 2) {
            return 0;
        }
        int n = height.length;
        int[] left = new int[n];
        int[] right = new int[n];
        left[0] = height[0];
        for (int i = 1; i < n; i++) {
            left[i] = Math.max(left[i - 1], height[i]);
        }
        right[right.length - 1] = height[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            right[i] = Math.max(right[i + 1], height[i]);
        }
        int result = 0;
        for (int i = 0; i < n; i++) {
            result += Math.min(left[i], right[i]) - height[i];
        }
        return result;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(0, trap(new int[]{1, 10}));
        CheckAnswerUtil.check(1, trap(new int[]{1, 0, 1}));
        CheckAnswerUtil.check(0, trap(new int[]{0, 1, 0}));
        CheckAnswerUtil.check(6, trap(new int[]{0, 1, 0, 2, 1, 0, 1, 3, 2, 1, 2, 1}));
        CheckAnswerUtil.check(9, trap(new int[]{4, 2, 0, 3, 2, 5}));
    }
}
