package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/string-compression/
 * <p>
 * Given an array of characters chars, compress it using the following algorithm:
 * <p>
 * Begin with an empty string s. For each group of consecutive repeating characters in chars:
 * <p>
 * If the group's length is 1, append the character to s.
 * Otherwise, append the character followed by the group's length.
 * The compressed string s should not be returned separately, but instead, be stored in the input character array chars. Note that group lengths that are 10 or longer will be split into multiple characters in chars.
 * <p>
 * After you are done modifying the input array, return the new length of the array.
 * <p>
 * You must write an algorithm that uses only constant extra space.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: chars = ["a","a","b","b","c","c","c"]
 * Output: Return 6, and the first 6 characters of the input array should be: ["a","2","b","2","c","3"]
 * Explanation: The groups are "aa", "bb", and "ccc". This compresses to "a2b2c3".
 * Example 2:
 * <p>
 * Input: chars = ["a"]
 * Output: Return 1, and the first character of the input array should be: ["a"]
 * Explanation: The only group is "a", which remains uncompressed since it's a single character.
 * Example 3:
 * <p>
 * Input: chars = ["a","b","b","b","b","b","b","b","b","b","b","b","b"]
 * Output: Return 4, and the first 4 characters of the input array should be: ["a","b","1","2"].
 * Explanation: The groups are "a" and "bbbbbbbbbbbb". This compresses to "ab12".
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= chars.length <= 2000
 * chars[i] is a lowercase English letter, uppercase English letter, digit, or symbol.
 */
public class _443_StringCompression {

    public static int compress(char[] chars) {
        int ans = 1;
        int index = 0;
        int curLength = 1;
        char prev = chars[index++];
        for (int i = 1; i < chars.length; i++) {
            while (i < chars.length && chars[i] == prev) {
                curLength++;
                i++;
            }
            if (curLength > 1) {
                char[] array = String.valueOf(curLength).toCharArray();
                for (char c : array) {
                    chars[index++] = c;
                    ans++;
                }
            }
            curLength = 1;
            if (i < chars.length) {
                prev = chars[i];
                ans++;
                chars[index++] = prev;
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        char[] chars = {'a', 'a', 'b', 'b', 'c', 'c', 'c'};
        CheckAnswerUtil.check(6, compress(chars));
        System.out.println(Arrays.toString(chars));
        System.out.println();

        chars = new char[]{'a'};
        CheckAnswerUtil.check(1, compress(chars));
        System.out.println(Arrays.toString(chars));
        System.out.println();

        chars = new char[]{'a', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'};
        CheckAnswerUtil.check(4, compress(chars));
        System.out.println(Arrays.toString(chars));
        System.out.println();

        chars = new char[]{'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b', 'b'};
        CheckAnswerUtil.check(3, compress(chars));
        System.out.println(Arrays.toString(chars));
        System.out.println();

        chars = new char[]{'a', 'a', 'a', 'b', 'b', 'a', 'a'};
        CheckAnswerUtil.check(6, compress(chars));
        System.out.println(Arrays.toString(chars));
        System.out.println();

        chars = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g'};
        CheckAnswerUtil.check(7, compress(chars));
        System.out.println(Arrays.toString(chars));
        System.out.println();

        chars = new char[]{'a', 'a', 'b', 'b', 'c', 'c', 'd', 'd', 'e', 'e', 'f', 'f', 'g', 'g'};
        CheckAnswerUtil.check(14, compress(chars));
        System.out.println(Arrays.toString(chars));
        System.out.println();
    }
}
