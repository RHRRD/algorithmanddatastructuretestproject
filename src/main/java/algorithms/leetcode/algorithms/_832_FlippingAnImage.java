package algorithms.leetcode.algorithms;

public class _832_FlippingAnImage {

    public static void main(String[] args) {
        int[][] input = new int[][]{{1, 1, 0}, {1, 0, 1}, {0, 0, 0}};
        int[][] output = new int[][]{{1, 0, 0}, {0, 1, 0}, {1, 1, 1}};

        int[][] ints = flipAndInvertImage(input);
        System.out.println(input);
    }

    private static int[][] flipAndInvertImage(int[][] A) {
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[i].length / 2; j++) {
                int tmp = A[i][j];
                A[i][j] = A[i][A.length - 1 - j];
                A[i][A.length - 1 - j] = tmp;
            }
            for (int j = 0; j < A[i].length; j++) {
                if (A[i][j] == 0) {
                    A[i][j] = 1;
                } else {
                    A[i][j] = 0;
                }
            }
        }
        return A;
    }

}
