package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/move-zeroes/
 * <p>
 * Given an integer array nums, move all 0's to the end of it while maintaining the relative order of the non-zero elements.
 * <p>
 * Note that you must do this in-place without making a copy of the array.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [0,1,0,3,12]
 * Output: [1,3,12,0,0]
 * Example 2:
 * <p>
 * Input: nums = [0]
 * Output: [0]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 104
 * -231 <= nums[i] <= 231 - 1
 * <p>
 * <p>
 * Follow up: Could you minimize the total number of operations done?
 */
public class _283_MoveZeroes {

    public static void moveZeroes(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                int j = i + 1;
                while (j < nums.length && nums[j] == 0) {
                    j++;
                }
                if (j < nums.length) {
                    nums[i] = nums[j];
                    nums[j] = 0;
                }
            }
        }
    }

    public static void moveZeroes2(int[] nums) {
        int i = 0, j = nums.length - 1;
        while (i < j) {
            while (nums[i] != 0 && i < nums.length && i < j) {
                i++;
            }
            while (nums[j] == 0 && j >= 0 && j > i) {
                j--;
            }
            nums[i++] = nums[j];
            nums[j--] = 0;
        }
    }

    public static void main(String[] args) {
        int[] nums = {0, 1, 0, 3, 12};
        System.out.println(Arrays.toString(nums));
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
        System.out.println(Arrays.toString(new int[]{1, 3, 12, 0, 0}));
        System.out.println();

        nums = new int[]{2, 0, 1, 0, 3, 0, 12, 15};
        System.out.println(Arrays.toString(nums));
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
        System.out.println(Arrays.toString(new int[]{2, 1, 3, 12, 15, 0, 0, 0}));
        System.out.println();

        nums = new int[]{2, 0, 1, 0, 3, 0, 12, 15, 0, 0, 0};
        System.out.println(Arrays.toString(nums));
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
        System.out.println(Arrays.toString(new int[]{2, 1, 3, 12, 15, 0, 0, 0, 0, 0, 0}));
        System.out.println();

        nums = new int[]{0};
        moveZeroes(nums);
        System.out.println(Arrays.toString(nums));
    }

}
