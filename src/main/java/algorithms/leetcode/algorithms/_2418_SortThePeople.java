package algorithms.leetcode.algorithms;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/sort-the-people/
 * <p>
 * You are given an array of strings names, and an array heights that consists of distinct positive integers. Both arrays are of length n.
 * <p>
 * For each index i, names[i] and heights[i] denote the name and height of the ith person.
 * <p>
 * Return names sorted in descending order by the people's heights.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: names = ["Mary","John","Emma"], heights = [180,165,170]
 * Output: ["Mary","Emma","John"]
 * Explanation: Mary is the tallest, followed by Emma and John.
 * Example 2:
 * <p>
 * Input: names = ["Alice","Bob","Bob"], heights = [155,185,150]
 * Output: ["Bob","Alice","Bob"]
 * Explanation: The first Bob is the tallest, followed by Alice and the second Bob.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * n == names.length == heights.length
 * 1 <= n <= 10^3
 * 1 <= names[i].length <= 20
 * 1 <= heights[i] <= 10^5
 * names[i] consists of lower and upper case English letters.
 * All the values of heights are distinct.
 */
public class _2418_SortThePeople {

    public static String[] sortPeople(String[] names, int[] heights) {
        String[] res = new String[names.length];
        Map<Integer, Integer> initMap = new HashMap<>();
        Integer[] newHeights = new Integer[heights.length];
        for (int i = 0; i < heights.length; i++) {
            initMap.put(heights[i], i);
            newHeights[i] = heights[i];
        }
        Arrays.sort(newHeights, Collections.reverseOrder());
        Map<Integer, Integer> sortMap = new HashMap<>();
        for (int i = 0; i < newHeights.length; i++) {
            sortMap.put(initMap.get(newHeights[i]), i);
        }
        for (int i = 0; i < res.length; i++) {
            res[sortMap.get(i)] = names[i];
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(sortPeople(new String[]{"Mary", "John", "Emma"}, new int[]{180, 165, 170})));
        System.out.println(Arrays.toString(new String[]{"Mary", "Emma", "John"}));
        System.out.println();

        System.out.println(Arrays.toString(sortPeople(new String[]{"Alice", "Bob", "Bob"}, new int[]{155, 185, 150})));
        System.out.println(Arrays.toString(new String[]{"Bob", "Alice", "Bob"}));
        System.out.println();

        System.out.println(Arrays.toString(sortPeople(new String[]{"GXLVEHVABFOGSFXUYYR", "TUHxnsxmu", "X", "OOYBLVKmzlaeaxbprc", "ARNLAPtfvmutkfsa", "XPMKPDUWOQEEILtbdjip", "QICEutjbr", "R"}, new int[]{11578, 89340, 73785, 12096, 55734, 89484, 59775, 72652})));
        System.out.println(Arrays.toString(new String[]{"XPMKPDUWOQEEILtbdjip", "TUHxnsxmu", "X", "R", "QICEutjbr", "ARNLAPtfvmutkfsa", "OOYBLVKmzlaeaxbprc", "GXLVEHVABFOGSFXUYYR"}))
        ;
    }
}
