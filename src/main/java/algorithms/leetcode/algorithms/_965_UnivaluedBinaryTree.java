package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

public class _965_UnivaluedBinaryTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(1);
        TreeNode treeNode = new TreeNode(1);
        root.right = treeNode;
        assert isUnivalTree(root);
        treeNode.left = new TreeNode(2);
        assert !isUnivalTree(root);
    }

    private static boolean isUnivalTree(TreeNode root) {
        return isUnivalTree(root, root.val);
    }

    private static boolean isUnivalTree(TreeNode root, int val) {
        if (root == null) {
            return true;
        }
        return root.val == val && isUnivalTree(root.left, val) && isUnivalTree(root.right, val);
    }

}
