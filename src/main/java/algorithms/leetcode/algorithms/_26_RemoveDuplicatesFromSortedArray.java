package algorithms.leetcode.algorithms;

public class _26_RemoveDuplicatesFromSortedArray {

    public static void main(String[] args) {
        assert 0 == removeDuplicates(null);
        assert 0 == removeDuplicates(new int[]{});
        assert 1 == removeDuplicates(new int[]{1, 1, 1});
        assert 1 == removeDuplicates(new int[]{1});
        assert 2 == removeDuplicates(new int[]{1, 2});
        assert 2 == removeDuplicates(new int[]{1, 1, 2});
        assert 3 == removeDuplicates(new int[]{1, 2, 3});
        assert 5 == removeDuplicates(new int[]{0, 0, 1, 1, 1, 2, 2, 3, 3, 4});
    }

    private static int removeDuplicates(int[] nums) {
        if (nums == null || nums.length == 0) return 0;
        int res = 0, step = 1;
        for (int i = 0; i < nums.length - step; i++) {
            if (nums[i] != nums[i + 1]) {
                res++;
            } else {
                for (int j = i + 1; j < nums.length - 1; j++) {
                    int tmp = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = tmp;
                }
                i--;
                step++;
            }
        }
        return res + 1;
    }
}
