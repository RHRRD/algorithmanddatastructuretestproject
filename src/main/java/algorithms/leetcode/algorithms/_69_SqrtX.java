package algorithms.leetcode.algorithms;

public class _69_SqrtX {

    public static void main(String[] args) {
        assert 0 == mySqrt(0);
        assert 2 == mySqrt(4);
        assert 2 == mySqrt(8);
        assert 3 == mySqrt(9);
        assert 5 == mySqrt(25);
        assert 5 == mySqrt(30);
        assert 46340 == mySqrt(2147483647);
    }

    private static int mySqrt(int x) {
        String s = String.valueOf(Math.sqrt(x));
        return Integer.parseInt(s.substring(0, s.indexOf('.')));
//        if (x == 0) return 0;
//        int last = 1;
//        for (int i = 2; i <= x / 2; i++) {
//            int i1 = i * i;
//            if (i1 == x) return i;
//            if (last < x && x < i1) return last;
//            last = i;
//        }
//        return last;
    }

}
