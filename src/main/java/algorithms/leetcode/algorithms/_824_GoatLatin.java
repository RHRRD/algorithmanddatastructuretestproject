package algorithms.leetcode.algorithms;

public class _824_GoatLatin {

    public static void main(String[] args) {
        assert "Imaa peaksmaaa oatGmaaaa atinLmaaaaa".equals(toGoatLatin("I speak Goat Latin"));
        assert "heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa".equals(toGoatLatin("The quick brown fox jumped over the lazy dog"));
        assert "Eachmaa ordwmaaa onsistscmaaaa ofmaaaaa owercaselmaaaaaa andmaaaaaaa uppercasemaaaaaaaa etterslmaaaaaaaaa onlymaaaaaaaaaa".equals(toGoatLatin("Each word consists of lowercase and uppercase letters only"));
    }

    private static String toGoatLatin(String S) {
        String vowel = "aeiouAEIOU";
        StringBuilder sb = new StringBuilder();
        int cnt = 1;
        for (String str : S.split(" ")) {
            String first = str.substring(0, 1);
            if (vowel.contains(first)) {
                sb.append(str);
            } else if (str.length() != 1) {
                sb.append(str.substring(1));
                sb.append(first);
            } else {
                sb.append(first);
            }
            sb.append("ma");
            for (int i = 0; i < cnt; i++) {
                sb.append("a");
            }
            sb.append(" ");
            cnt++;
        }

        return sb.toString().trim();
    }
}
