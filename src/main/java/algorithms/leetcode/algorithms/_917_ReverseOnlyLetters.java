package algorithms.leetcode.algorithms;

public class _917_ReverseOnlyLetters {

    public static void main(String[] args) {
        assert "dc-ba".equals(reverseOnlyLetters("ab-cd"));
        assert "j-Ih-gfE-dCba".equals(reverseOnlyLetters("a-bC-dEf-ghIj"));
        assert "Qedo1ct-eeLg=ntse-T!".equals(reverseOnlyLetters("Test1ng-Leet=code-Q!"));
        assert "ryzC^".equals(reverseOnlyLetters("Czyr^"));
        assert "7_28]".equals(reverseOnlyLetters("7_28]"));
    }

    private static String reverseOnlyLetters(String S) {
        char[] array = S.toCharArray();
        int i = 0, j = array.length - 1;
        while (i < j) {
            while (i < array.length && !Character.isLetter(array[i])) {
                i++;
            }
            while (j >= 0 && !Character.isLetter(array[j])) {
                j--;
            }
            if (i < j) {
                char tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
                i++;
                j--;
            }
        }
        return String.valueOf(array);
    }
}
