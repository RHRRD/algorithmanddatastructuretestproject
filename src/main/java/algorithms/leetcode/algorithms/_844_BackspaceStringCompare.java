package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/backspace-string-compare/
 * <p>
 * Given two strings s and t, return true if they are equal when both are typed into empty text editors. '#' means a backspace character.
 * <p>
 * Note that after backspacing an empty text, the text will continue empty.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "ab#c", t = "ad#c"
 * Output: true
 * Explanation: Both s and t become "ac".
 * Example 2:
 * <p>
 * Input: s = "ab##", t = "c#d#"
 * Output: true
 * Explanation: Both s and t become "".
 * Example 3:
 * <p>
 * Input: s = "a#c", t = "b"
 * Output: false
 * Explanation: s becomes "c" while t becomes "b".
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length, t.length <= 200
 * s and t only contain lowercase letters and '#' characters.
 * <p>
 * <p>
 * Follow up: Can you solve it in O(n) time and O(1) space?
 */
public class _844_BackspaceStringCompare {

    public static boolean backspaceCompare(String s, String t) {
        if (s.equals(t)) {
            return true;
        }
        StringBuilder sbs = new StringBuilder();
        StringBuilder sbt = new StringBuilder();
        for (char tmp : s.toCharArray()) {
            int size = sbs.length();
            if (size > 0 && tmp == '#') {
                sbs.deleteCharAt(size - 1);
            } else if (tmp != '#') {
                sbs.append(tmp);
            }
        }
        for (char tmp : t.toCharArray()) {
            int size = sbt.length();
            if (size > 0 && tmp == '#') {
                sbt.deleteCharAt(size - 1);
            } else if (tmp != '#') {
                sbt.append(tmp);
            }
        }
        return sbs.toString().equals(sbt.toString());
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, backspaceCompare("ab#c", "ad#c"));
        CheckAnswerUtil.check(true, backspaceCompare("ab##", "c#d#"));
        CheckAnswerUtil.check(false, backspaceCompare("a#c", "b"));
        CheckAnswerUtil.check(true, backspaceCompare("a#", "b#"));
        CheckAnswerUtil.check(false, backspaceCompare("#a", "#a#"));
        CheckAnswerUtil.check(true, backspaceCompare("#a", "#a#a"));
    }
}
