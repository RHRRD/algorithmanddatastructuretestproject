package algorithms.leetcode.algorithms

fun uniqueOccurrences(arr: IntArray): Boolean {
    val hashMap: MutableMap<Int, Int> = HashMap()
    val hashSet: MutableSet<Int> = HashSet()
    arr.forEach { hashMap[it] = hashMap.getOrDefault(it, 0) + 1 }
    hashMap.values.forEach { hashSet.add(it) }
    return hashMap.values.size == hashSet.size
}
