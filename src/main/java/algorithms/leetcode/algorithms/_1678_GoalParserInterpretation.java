package algorithms.leetcode.algorithms;

/**
 * https://leetcode.com/problems/goal-parser-interpretation/
 * <p>
 * You own a Goal Parser that can interpret a string command. The command consists of an alphabet of "G", "()" and/or "(al)" in some order. The Goal Parser will interpret "G" as the string "G", "()" as the string "o", and "(al)" as the string "al". The interpreted strings are then concatenated in the original order.
 * <p>
 * Given the string command, return the Goal Parser's interpretation of command.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: command = "G()(al)"
 * Output: "Goal"
 * Explanation: The Goal Parser interprets the command as follows:
 * G -> G
 * () -> o
 * (al) -> al
 * The final concatenated result is "Goal".
 * Example 2:
 * <p>
 * Input: command = "G()()()()(al)"
 * Output: "Gooooal"
 * Example 3:
 * <p>
 * Input: command = "(al)G(al)()()G"
 * Output: "alGalooG"
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= command.length <= 100
 * command consists of "G", "()", and/or "(al)" in some order.
 */
public class _1678_GoalParserInterpretation {

    public static String interpret(String command) {
        StringBuilder res = new StringBuilder();
        char prevChar = ' ';
        for (char currentChar : command.toCharArray()) {
            if (currentChar == 'G') {
                res.append("G");
            }
            if (currentChar == ')') {
                if (prevChar == '(') {
                    res.append("o");
                }
                if (prevChar == 'l') {
                    res.append("al");
                }
            }
            prevChar = currentChar;
        }
        return res.toString();
    }

    public static void main(String[] args) {
        System.out.println(interpret("G()(al)"));
        System.out.println("Goal");
        System.out.println();

        System.out.println(interpret("G()()()()(al)"));
        System.out.println("Gooooal");
        System.out.println();

        System.out.println(interpret("(al)G(al)()()G"));
        System.out.println("alGalooG");
        System.out.println();

    }
}
