package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/make-the-string-great/
 * <p>
 * Given a string s of lower and upper case English letters.
 * <p>
 * A good string is a string which doesn't have two adjacent characters s[i] and s[i + 1] where:
 * <p>
 * 0 <= i <= s.length - 2
 * s[i] is a lower-case letter and s[i + 1] is the same letter but in upper-case or vice-versa.
 * To make the string good, you can choose two adjacent characters that make the string bad and remove them. You can keep doing this until the string becomes good.
 * <p>
 * Return the string after making it good. The answer is guaranteed to be unique under the given constraints.
 * <p>
 * Notice that an empty string is also good.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "leEeetcode"
 * Output: "leetcode"
 * Explanation: In the first step, either you choose i = 1 or i = 2, both will result "leEeetcode" to be reduced to "leetcode".
 * Example 2:
 * <p>
 * Input: s = "abBAcC"
 * Output: ""
 * Explanation: We have many possible scenarios, and all lead to the same answer. For example:
 * "abBAcC" --> "aAcC" --> "cC" --> ""
 * "abBAcC" --> "abBA" --> "aA" --> ""
 * Example 3:
 * <p>
 * Input: s = "s"
 * Output: "s"
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 100
 * s contains only lower and upper case English letters.
 */
public class _1544_MakeTheStringGreat {

    public static String makeGood(String s) {
        if (s.length() == 1) {
            return s;
        }
        int step;
        StringBuilder tmp = new StringBuilder(s);
        char[] array;
        do {
            step = 0;
            array = tmp.toString().toCharArray();
            tmp = new StringBuilder();
            for (int i = 0; i < array.length; i++) {
                if (i == array.length - 1) {
                    tmp.append(array[i]);
                    continue;
                }
                if (Math.abs(array[i] - array[i + 1]) != 32) {
                    tmp.append(array[i]);
                } else {
                    step++;
                    i++;
                }
            }
        } while (step != 0);
        return tmp.toString();
    }

    public static String makeGood1(String s) {
        if (s.length() == 1) {
            return s;
        }
        char[] array = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        int current = 0, next = 1;
        boolean flag = false;
        while (current < array.length && next < array.length) {
            if (Math.abs(array[current] - array[next]) == 32) {
//                if (sb.length() > 0) {
//                    prev--;
//                } else {
//                    prev = current + 1;
//                    current++;
//                }
//                if (prev < 0) {
//                    prev = current + 1;
//                    current++;
//                    sb.deleteCharAt(sb.length() - 1);
//                }
                current--;
                if (current < 0) {
                    current = next;
                }
                if (flag) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                flag = false;
            } else {
                sb.append(array[current]);
                current = next;
                flag = true;
            }
            next++;
        }
//        if (prev < array.length) {
//            sb.append(array[prev]);
//        }
        return sb.toString();
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check("leetcode", makeGood("leEeetcode"), "leEeetcode");
        CheckAnswerUtil.check("", makeGood("abBAcC"), "abBAcC");
        CheckAnswerUtil.check("", makeGood("DabBAcCd"), "DabBAcCd");
        CheckAnswerUtil.check("", makeGood("abBAcCdD"), "abBAcCdD");
        CheckAnswerUtil.check("s", makeGood("s"), "s");
        CheckAnswerUtil.check("ab", makeGood("ab"), "ab");
        CheckAnswerUtil.check("", makeGood(""), "");
    }
}
