package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

public class _21_MergeTwoSortedLists {

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode listNode1 = new ListNode(2);
        l1.next = listNode1;
        listNode1.next = new ListNode(3);
        ListNode l2 = new ListNode(3);
        l2.next = new ListNode(4);
        ListNode listNode = mergeTwoLists(l1, l2);
        System.out.println(listNode.toString());
    }

    private static ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if (l1 == null && l2 == null) {
            return null;
        }
        if (l1 == null) {
            return l2;
        }
        if (l2 == null) {
            return l1;
        }
        if (l1.val < l2.val) {
            ListNode node = new ListNode(l1.val);
            node.next = mergeTwoLists(l1.next, l2);
            return node;
        } else {
            ListNode node = new ListNode(l2.val);
            node.next = mergeTwoLists(l1, l2.next);
            return node;
        }
    }

}
