package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/count-items-matching-a-rule/
 * <p>
 * You are given an array items, where each items[i] = [typei, colori, namei] describes the type, color, and name of the ith item. You are also given a rule represented by two strings, ruleKey and ruleValue.
 * <p>
 * The ith item is said to match the rule if one of the following is true:
 * <p>
 * ruleKey == "type" and ruleValue == typei.
 * ruleKey == "color" and ruleValue == colori.
 * ruleKey == "name" and ruleValue == namei.
 * Return the number of items that match the given rule.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: items = [["phone","blue","pixel"],["computer","silver","lenovo"],["phone","gold","iphone"]], ruleKey = "color", ruleValue = "silver"
 * Output: 1
 * Explanation: There is only one item matching the given rule, which is ["computer","silver","lenovo"].
 * Example 2:
 * <p>
 * Input: items = [["phone","blue","pixel"],["computer","silver","phone"],["phone","gold","iphone"]], ruleKey = "type", ruleValue = "phone"
 * Output: 2
 * Explanation: There are only two items matching the given rule, which are ["phone","blue","pixel"] and ["phone","gold","iphone"]. Note that the item ["computer","silver","phone"] does not match.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= items.length <= 10^4
 * 1 <= typei.length, colori.length, namei.length, ruleValue.length <= 10
 * ruleKey is equal to either "type", "color", or "name".
 * All strings consist only of lowercase letters.
 */
public class _1773_CountItemsMatchingARule {

    public static int countMatches(List<List<String>> items, String ruleKey, String ruleValue) {
        int res = 0;
        for (List<String> lst : items) {
            String type = lst.get(0);
            String color = lst.get(1);
            String name = lst.get(2);
            switch (ruleKey) {
                case "type":
                    if (ruleValue.equals(type)) {
                        res++;
                    }
                    break;
                case "color":
                    if (ruleValue.equals(color)) {
                        res++;
                    }
                    break;
                case "name":
                    if (ruleValue.equals(name)) {
                        res++;
                    }
                    break;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        ArrayList<List<String>> lists = new ArrayList<>();
        ArrayList<String> tmpList = new ArrayList<>();
        tmpList.add("phone");
        tmpList.add("blue");
        tmpList.add("pixel");
        lists.add(tmpList);
        tmpList = new ArrayList<>();
        tmpList.add("computer");
        tmpList.add("silver");
        tmpList.add("lenovo");
        lists.add(tmpList);
        tmpList = new ArrayList<>();
        tmpList.add("phone");
        tmpList.add("gold");
        tmpList.add("iphone");
        lists.add(tmpList);
        System.out.println(countMatches(lists, "color", "silver"));
        System.out.println(1);
        System.out.println();

        System.out.println(countMatches(lists, "type", "phone"));
        System.out.println(2);
    }

}
