package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/reverse-words-in-a-string/
 * <p>
 * Given an input string s, reverse the order of the words.
 * <p>
 * A word is defined as a sequence of non-space characters. The words in s will be separated by at least one space.
 * <p>
 * Return a string of the words in reverse order concatenated by a single space.
 * <p>
 * Note that s may contain leading or trailing spaces or multiple spaces between two words. The returned string should only have a single space separating the words. Do not include any extra spaces.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "the sky is blue"
 * Output: "blue is sky the"
 * Example 2:
 * <p>
 * Input: s = "  hello world  "
 * Output: "world hello"
 * Explanation: Your reversed string should not contain leading or trailing spaces.
 * Example 3:
 * <p>
 * Input: s = "a good   example"
 * Output: "example good a"
 * Explanation: You need to reduce multiple spaces between two words to a single space in the reversed string.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 10^4
 * s contains English letters (upper-case and lower-case), digits, and spaces ' '.
 * There is at least one word in s.
 * <p>
 * <p>
 * Follow-up: If the string data type is mutable in your language, can you solve it in-place with O(1) extra space?
 */
public class _151_ReverseWordsInAString {

    public static String reverseWords(String s) {
        StringBuilder sb = new StringBuilder();
        sb.append(s);
        String newS = sb.reverse().toString();
        sb = new StringBuilder();
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < newS.length(); i++) {
            if (newS.charAt(i) != ' ') {
                sb.append(newS.charAt(i));
            } else {
                if (sb.length() > 0 && res.length() > 0) {
                    res.append(" ");
                }
                res.append(sb.reverse());
                sb = new StringBuilder();
            }
        }
        if (sb.length() > 0) {
            if (res.length() > 0) {
                res.append(" ");
            }
            res.append(sb.reverse());
        }
        return res.toString();
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check("blue is sky the", reverseWords("the sky is blue"));
        CheckAnswerUtil.check("world hello", reverseWords("  hello world "));
        CheckAnswerUtil.check("example good a", reverseWords("a good   example"));
    }
}
