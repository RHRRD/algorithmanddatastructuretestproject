package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

public class _700_SearchInABinarySearchTree {

    public static void main(String[] args) {
        TreeNode root = new TreeNode(4);
        TreeNode left = new TreeNode(2);
        left.left = new TreeNode(1);
        left.right = new TreeNode(3);
        root.left = left;
        root.right = new TreeNode(7);
        assert left == searchBST(root, 2);
        assert root == searchBST(root, 4);
        assert null == searchBST(null, 4);
    }

    private static TreeNode searchBST(TreeNode root, int val) {
        if (root == null) {
            return null;
        }
        if (root.val == val) {
            return root;
        }
        TreeNode left = searchBST(root.left, val);
        if (left != null) return left;
        return searchBST(root.right, val);
    }

}
