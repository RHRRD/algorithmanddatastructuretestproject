package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/shifting-letters/
 * <p>
 * You are given a string s of lowercase English letters and an integer array shifts of the same length.
 * <p>
 * Call the shift() of a letter, the next letter in the alphabet, (wrapping around so that 'z' becomes 'a').
 * <p>
 * For example, shift('a') = 'b', shift('t') = 'u', and shift('z') = 'a'.
 * Now for each shifts[i] = x, we want to shift the first i + 1 letters of s, x times.
 * <p>
 * Return the final string after all such shifts to s are applied.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "abc", shifts = [3,5,9]
 * Output: "rpl"
 * Explanation: We start with "abc".
 * After shifting the first 1 letters of s by 3, we have "dbc".
 * After shifting the first 2 letters of s by 5, we have "igc".
 * After shifting the first 3 letters of s by 9, we have "rpl", the answer.
 * Example 2:
 * <p>
 * Input: s = "aaa", shifts = [1,2,3]
 * Output: "gfd"
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 10^5
 * s consists of lowercase English letters.
 * shifts.length == s.length
 * 0 <= shifts[i] <= 10^9
 */
public class _848_ShiftingLetters {

    public static String shiftingLetters(String s, int[] shifts) {
        if (shifts.length > 0) {
            shifts[shifts.length - 1] %= 26;
        }
        for (int i = shifts.length - 2; i >= 0; i--) {
            shifts[i] = (shifts[i] + shifts[i + 1]) % 26;
        }
        StringBuilder res = new StringBuilder();
        char[] array = s.toCharArray();
        for (int i = 0; i < array.length; i++) {
            int shift = array[i] + shifts[i] % 26;
            char c = (char) (shift > 122 ? shift % 122 + 96 : shift);
            res.append(c);
        }
        return res.toString();
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check("rpl", shiftingLetters("abc", new int[]{3, 5, 9}));
        CheckAnswerUtil.check("gfd", shiftingLetters("aaa", new int[]{1, 2, 3}));
        CheckAnswerUtil.check("a", shiftingLetters("z", new int[]{1}));
        CheckAnswerUtil.check("z", shiftingLetters("z", new int[]{26}));
        CheckAnswerUtil.check("z", shiftingLetters("z", new int[]{260}));
        CheckAnswerUtil.check("wqqwlcjnkphhsyvrkdod", shiftingLetters("mkgfzkkuxownxvfvxasy", new int[]{505870226, 437526072, 266740649, 224336793, 532917782, 311122363, 567754492, 595798950, 81520022, 684110326, 137742843, 275267355, 856903962, 148291585, 919054234, 467541837, 622939912, 116899933, 983296461, 536563513}));
    }
}
