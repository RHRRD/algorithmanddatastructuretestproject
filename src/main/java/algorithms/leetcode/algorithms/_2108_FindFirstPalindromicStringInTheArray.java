package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/find-first-palindromic-string-in-the-array/
 * <p>
 * Given an array of strings words, return the first palindromic string in the array. If there is no such string, return an empty string "".
 * <p>
 * A string is palindromic if it reads the same forward and backward.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: words = ["abc","car","ada","racecar","cool"]
 * Output: "ada"
 * Explanation: The first string that is palindromic is "ada".
 * Note that "racecar" is also palindromic, but it is not the first.
 * Example 2:
 * <p>
 * Input: words = ["notapalindrome","racecar"]
 * Output: "racecar"
 * Explanation: The first and only string that is palindromic is "racecar".
 * Example 3:
 * <p>
 * Input: words = ["def","ghi"]
 * Output: ""
 * Explanation: There are no palindromic strings, so the empty string is returned.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= words.length <= 100
 * 1 <= words[i].length <= 100
 * words[i] consists only of lowercase English letters.
 */
public class _2108_FindFirstPalindromicStringInTheArray {

    public static String firstPalindrome(String[] words) {
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            if (sb.append(word).reverse().toString().equals(word)) {
                return word;
            }
            sb = new StringBuilder();
        }
        return "";
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check("ada", firstPalindrome(new String[]{"abc", "car", "ada", "racecar", "cool"}));
        CheckAnswerUtil.check("racecar", firstPalindrome(new String[]{"notapalindrome", "racecar"}));
        CheckAnswerUtil.check("", firstPalindrome(new String[]{"def", "ghi"}));
    }
}
