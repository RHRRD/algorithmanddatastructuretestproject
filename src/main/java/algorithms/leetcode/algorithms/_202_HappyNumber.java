package algorithms.leetcode.algorithms;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode.com/problems/happy-number/
 * <p>
 * Write an algorithm to determine if a number n is happy.
 * <p>
 * A happy number is a number defined by the following process:
 * <p>
 * Starting with any positive integer, replace the number by the sum of the squares of its digits.
 * Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.
 * Those numbers for which this process ends in 1 are happy.
 * Return true if n is a happy number, and false if not.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: n = 19
 * Output: true
 * Explanation:
 * 12 + 92 = 82
 * 82 + 22 = 68
 * 62 + 82 = 100
 * 12 + 02 + 02 = 1
 * Example 2:
 * <p>
 * Input: n = 2
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= n <= 2^31 - 1
 */
public class _202_HappyNumber {

    public static boolean isHappy(int n) {
        Set<Integer> set = new HashSet<>();
        int sum = n;
        while (set.add(sum)) {
            String[] split = String.valueOf(sum).split("");
            sum = 0;
            for (String s : split) {
                sum += Math.pow(Integer.parseInt(s), 2);
            }
            if (sum == 1) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(isHappy(19));
        System.out.println(true);
        System.out.println();

        System.out.println(isHappy(2));
        System.out.println(false);
        System.out.println();

        System.out.println(isHappy(7));
        System.out.println(true);
        System.out.println();

        System.out.println(isHappy(1111111));
        System.out.println(true);
        System.out.println();
    }
}
