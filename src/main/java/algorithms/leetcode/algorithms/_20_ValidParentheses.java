package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.Stack;

/**
 * https://leetcode.com/problems/valid-parentheses/description/
 * <p>
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 * <p>
 * An input string is valid if:
 * <p>
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 * Every close bracket has a corresponding open bracket of the same type.
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "()"
 * Output: true
 * Example 2:
 * <p>
 * Input: s = "()[]{}"
 * Output: true
 * Example 3:
 * <p>
 * Input: s = "(]"
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 104
 * s consists of parentheses only '()[]{}'.
 */
public class _20_ValidParentheses {

    public static boolean isValid2(String s) {
        if (s == null || s.length() == 0) return true;
        char[] array = s.toCharArray();
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == ')') {
                if (str.length() == 0 || str.charAt(str.length() - 1) != '(') return false;
                else {
                    str.deleteCharAt(str.length() - 1);
                    continue;
                }
            } else if (array[i] == ']') {
                if (str.length() == 0 || str.charAt(str.length() - 1) != '[') return false;
                else {
                    str.deleteCharAt(str.length() - 1);
                    continue;
                }
            } else if (array[i] == '}') {
                if (str.length() == 0 || str.charAt(str.length() - 1) != '{') return false;
                else {
                    str.deleteCharAt(str.length() - 1);
                    continue;
                }
            }
            str.append(array[i]);
        }
        return str.length() == 0;
    }

    public static boolean isValid(String s) {
        if (s == null || s.isBlank() || s.isEmpty()) {
            return true;
        }
        Stack<Character> stack = new Stack<>();
        for (Character ch : s.toCharArray()) {
            if (ch == '(') {
                stack.push(')');
            } else if (ch == '{') {
                stack.push('}');
            } else if (ch == '[') {
                stack.push(']');
            } else if (stack.isEmpty() || stack.pop() != ch) {
                return false;
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, isValid(null));
        CheckAnswerUtil.check(true, isValid(""));
        CheckAnswerUtil.check(true, isValid("()"));
        CheckAnswerUtil.check(true, isValid("()[]{}"));
        CheckAnswerUtil.check(false, isValid("(]"));
        CheckAnswerUtil.check(false, isValid("([)]"));
        CheckAnswerUtil.check(true, isValid("({})"));
        CheckAnswerUtil.check(false, isValid("([)]"));
        CheckAnswerUtil.check(true, isValid("(())"));
        CheckAnswerUtil.check(false, isValid("(()[)]"));
        CheckAnswerUtil.check(true, isValid("(()[])"));
        CheckAnswerUtil.check(true, isValid("(()[[]])"));
        CheckAnswerUtil.check(false, isValid("[([]])"));
        CheckAnswerUtil.check(false, isValid("]"));
        CheckAnswerUtil.check(false, isValid("["));
        CheckAnswerUtil.check(false, isValid("["));
    }

}
