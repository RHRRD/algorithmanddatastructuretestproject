package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/minimum-depth-of-binary-tree/description/
 * <p>
 * Given a binary tree, find its minimum depth.
 * <p>
 * The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.
 * <p>
 * Note: A leaf is a node with no children.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 2
 * Example 2:
 * <p>
 * Input: root = [2,null,3,null,4,null,5,null,6]
 * Output: 5
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [0, 10^5].
 * -1000 <= Node.val <= 1000
 */
public class _111_MinimumDepthOfBinaryTree {

    public static int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        return minDepth(root, 0);
    }

    private static int minDepth(TreeNode node, int depth) {
        if (node.left == null && node.right == null) {
            return depth + 1;
        }
        if (node.left != null && node.right != null) {
            return Math.min(minDepth(node.left, depth + 1), minDepth(node.right, depth + 1));
        } else if (node.left == null) {
            return minDepth(node.right, depth + 1);
        } else {
            return minDepth(node.left, depth + 1);
        }
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, minDepth(new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)))));
        CheckAnswerUtil.check(5, minDepth(new TreeNode(2, null, new TreeNode(3, null, new TreeNode(4, null, new TreeNode(5, null, new TreeNode(6)))))));
        CheckAnswerUtil.check(0, minDepth(null));
        CheckAnswerUtil.check(1, minDepth(new TreeNode(1)));
    }

}
