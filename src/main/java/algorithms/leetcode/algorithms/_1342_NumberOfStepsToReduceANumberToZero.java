package algorithms.leetcode.algorithms;

public class _1342_NumberOfStepsToReduceANumberToZero {

    public static void main(String[] args) {
        assert 0 == numberOfSteps(0);
        assert 1 == numberOfSteps(1);
        assert 2 == numberOfSteps(2);
        assert 6 == numberOfSteps(14);
        assert 4 == numberOfSteps(8);
        assert 12 == numberOfSteps(123);
    }

    private static int numberOfSteps(int num) {
        int steps = 0;
        while (num != 0) {
            if (num % 2 == 0) {
                num /= 2;
            } else {
                num--;
            }
            steps++;
        }
        return steps;
    }

}
