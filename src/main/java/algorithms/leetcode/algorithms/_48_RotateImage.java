package algorithms.leetcode.algorithms;

/**
 * https://leetcode.com/problems/rotate-image/
 * <p>
 * You are given an n x n 2D matrix representing an image, rotate the image by 90 degrees (clockwise).
 * <p>
 * You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
 * Output: [[7,4,1],[8,5,2],[9,6,3]]
 * Example 2:
 * <p>
 * <p>
 * Input: matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
 * Output: [[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * n == matrix.length == matrix[i].length
 * 1 <= n <= 20
 * -1000 <= matrix[i][j] <= 1000
 */
public class _48_RotateImage {

    public static void rotate(int[][] matrix) {
        int n = matrix.length;
        int tmp_1, tmp_2, tmp_3, tmp_4;
        n--;
        for (int i = 0; i < matrix.length / 2; i++) {
            for (int j = i; j < n - i; j++) {
                tmp_1 = matrix[i][j];
                tmp_2 = matrix[j][n - i];
                tmp_3 = matrix[n - i][n - j];
                tmp_4 = matrix[n - j][i];
                matrix[j][n - i] = tmp_1;
                matrix[n - i][n - j] = tmp_2;
                matrix[n - j][i] = tmp_3;
                matrix[i][j] = tmp_4;
            }
        }
    }

    public static void main(String[] args) {
        int[][] matrix_0 = {{1}};
        printMatrix(matrix_0);
        System.out.println();
        rotate(matrix_0);
        printMatrix(matrix_0);
        System.out.println();

        int[][] matrix_1 = {{1, 2}, {3, 4}};
        printMatrix(matrix_1);
        System.out.println();
        rotate(matrix_1);
        printMatrix(matrix_1);
        System.out.println();

        int[][] matrix_2 = new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        printMatrix(matrix_2);
        System.out.println();
        rotate(matrix_2);
        printMatrix(matrix_2);
        System.out.println();

        int[][] matrix_3 = new int[][]{{5, 1, 9, 11}, {2, 4, 8, 10}, {13, 3, 6, 7}, {15, 14, 12, 16}};
        printMatrix(matrix_3);
        System.out.println();
        rotate(matrix_3);
        printMatrix(matrix_3);
        System.out.println();
    }

    private static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
