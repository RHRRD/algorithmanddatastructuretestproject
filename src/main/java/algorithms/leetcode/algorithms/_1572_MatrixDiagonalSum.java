package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/matrix-diagonal-sum/
 * <p>
 * Given a square matrix mat, return the sum of the matrix diagonals.
 * <p>
 * Only include the sum of all the elements on the primary diagonal and all the elements on the secondary diagonal that are not part of the primary diagonal.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: mat = [[1,2,3],
 * [4,5,6],
 * [7,8,9]]
 * Output: 25
 * Explanation: Diagonals sum: 1 + 5 + 9 + 3 + 7 = 25
 * Notice that element mat[1][1] = 5 is counted only once.
 * Example 2:
 * <p>
 * Input: mat = [[1,1,1,1],
 * [1,1,1,1],
 * [1,1,1,1],
 * [1,1,1,1]]
 * Output: 8
 * Example 3:
 * <p>
 * Input: mat = [[5]]
 * Output: 5
 * <p>
 * <p>
 * Constraints:
 * <p>
 * n == mat.length == mat[i].length
 * 1 <= n <= 100
 * 1 <= mat[i][j] <= 100
 */
public class _1572_MatrixDiagonalSum {

    public static int diagonalSum(int[][] mat) {
        int res = 0;
        for (int i = 0; i < mat.length; i++) {
            res += mat[i][i];
            res += mat[mat.length - 1 - i][i];
        }
        if (mat.length % 2 != 0) {
            res -= mat[(mat.length - 1) / 2][(mat.length - 1) / 2];
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(25, diagonalSum(new int[][]{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}));
        CheckAnswerUtil.check(8, diagonalSum(new int[][]{{1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}, {1, 1, 1, 1}}));
        CheckAnswerUtil.check(5, diagonalSum(new int[][]{{5}}));
        CheckAnswerUtil.check(4, diagonalSum(new int[][]{{1, 1}, {1, 1}}));
    }
}
