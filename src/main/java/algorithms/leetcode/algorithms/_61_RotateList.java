package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

/**
 * https://leetcode.com/problems/rotate-list/
 * <p>
 * Given the head of a linked list, rotate the list to the right by k places.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,2,3,4,5], k = 2
 * Output: [4,5,1,2,3]
 * Example 2:
 * <p>
 * <p>
 * Input: head = [0,1,2], k = 4
 * Output: [2,0,1]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is in the range [0, 500].
 * -100 <= Node.val <= 100
 * 0 <= k <= 2 * 10^9
 */
public class _61_RotateList {

    public static ListNode rotateRight(ListNode head, int k) {
        if (k == 0 || head == null || head.next == null) {
            return head;
        }
        int length = 0;
        ListNode tmp = head;
        while (tmp != null) {
            length++;
            tmp = tmp.next;
        }
        k %= length;
        if (k == 0) {
            return head;
        }
        tmp = head;
        int i = 0;
        ListNode prev = null;
        while (i != length - k) {
            i++;
            prev = tmp;
            tmp = tmp.next;
        }
        prev.next = null;
        ListNode result = tmp;
        while (tmp.next != null) {
            tmp = tmp.next;
        }
        tmp.next = head;
        return result;
    }

    public static void main(String[] args) {
        ListNode listNode_1 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(listNode_1);
        System.out.println(rotateRight(listNode_1, 2));
        System.out.println(new ListNode(4, new ListNode(5, new ListNode(1, new ListNode(2, new ListNode(3))))));
        System.out.println();

        ListNode listNode_11 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(listNode_11);
        System.out.println(rotateRight(listNode_11, 1));
        System.out.println(new ListNode(5, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4))))));
        System.out.println();

        ListNode listNode_13 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(listNode_13);
        System.out.println(rotateRight(listNode_13, 3));
        System.out.println(new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(1, new ListNode(2))))));
        System.out.println();

        ListNode listNode_14 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(listNode_14);
        System.out.println(rotateRight(listNode_14, 4));
        System.out.println(new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(1))))));
        System.out.println();

        ListNode listNode_2 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(listNode_2);
        System.out.println(rotateRight(listNode_2, 5));
        System.out.println(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5))))));
        System.out.println();

        ListNode listNode_3 = new ListNode(0, new ListNode(1, new ListNode(2)));
        System.out.println(listNode_3);
        System.out.println(rotateRight(listNode_3, 4));
        System.out.println(new ListNode(2, new ListNode(0, new ListNode(1))));
        System.out.println();

        ListNode listNode_4 = new ListNode(1, new ListNode(2, new ListNode(3)));
        System.out.println(listNode_4);
        System.out.println(rotateRight(listNode_4, 0));
        System.out.println(new ListNode(1, new ListNode(2, new ListNode(3))));
        System.out.println();
    }
}
