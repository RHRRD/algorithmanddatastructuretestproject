package algorithms.leetcode.algorithms;

import kotlin.Pair;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * https://leetcode.com/problems/merge-intervals/
 * <p>
 * Given an array of intervals where intervals[i] = [starti, endi], merge all overlapping intervals, and return an array of the non-overlapping intervals that cover all the intervals in the input.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
 * Output: [[1,6],[8,10],[15,18]]
 * Explanation: Since intervals [1,3] and [2,6] overlap, merge them into [1,6].
 * Example 2:
 * <p>
 * Input: intervals = [[1,4],[4,5]]
 * Output: [[1,5]]
 * Explanation: Intervals [1,4] and [4,5] are considered overlapping.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= intervals.length <= 10^4
 * intervals[i].length == 2
 * 0 <= starti <= endi <= 10^4
 */
public class _56_MergeIntervals {

    public static int[][] merge2(int[][] intervals) {
        int[] res = new int[10000];
        for (int i = 0; i < intervals.length; i++) {
            for (int j = intervals[i][0]; j < intervals[i][1]; j++) {
                res[j]++;
            }
        }
        List<Pair<Integer, Integer>> list = new ArrayList<>();
        for (int i = 0; i < res.length; i++) {
            if (res[i] > 0) {
                int j = i;
                while (i < res.length && res[i] > 0) {
                    i++;
                }
                list.add(new Pair<>(j, i));
            }
        }
        int[][] result = new int[list.size()][2];
        for (int i = 0; i < list.size(); i++) {
            result[i] = new int[]{list.get(i).component1(), list.get(i).component2()};
        }
        return result;
    }

    public static int[][] merge(int[][] intervals) {
        if (intervals.length == 1) {
            return intervals;
        }
        Arrays.sort(intervals, Comparator.comparingInt(it -> it[0]));
        List<int[]> result = new ArrayList<>();
        int[] newInterval = intervals[0];
        result.add(newInterval);
        for (int i = 0; i < intervals.length; i++) {
            if (intervals[i][0] <= newInterval[1]) {
                newInterval[1] = Math.max(newInterval[1], intervals[i][1]);
            } else {
                newInterval = intervals[i];
                result.add(newInterval);
            }
        }
        return result.toArray(new int[result.size()][]);
    }

    public static void main(String[] args) {
        System.out.println("[[1,6],[8,10],[15,18]] : " + Arrays.deepToString(merge(new int[][]{
                {1, 3},
                {2, 6},
                {8, 10},
                {15, 18}
        })));
        System.out.println("[[1,5]] : " + Arrays.deepToString(merge(new int[][]{
                {1, 4},
                {4, 5}
        })));
        System.out.println("[[0,10000]] : " + Arrays.deepToString(merge(new int[][]{
                {1, 10000},
                {0, 5}
        })));
    }
}
