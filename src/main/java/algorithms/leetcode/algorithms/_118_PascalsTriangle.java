package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/pascals-triangle/
 * <p>
 * Given an integer numRows, return the first numRows of Pascal's triangle.
 * <p>
 * In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:
 * <p>
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: numRows = 5
 * Output: [[1],[1,1],[1,2,1],[1,3,3,1],[1,4,6,4,1]]
 * Example 2:
 * <p>
 * Input: numRows = 1
 * Output: [[1]]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= numRows <= 30
 */
public class _118_PascalsTriangle {

    public static List<List<Integer>> generate(int numRows) {
        List<List<Integer>> result = new ArrayList<>();
        result.add(List.of(1));
        if (numRows == 1) {
            return result;
        }
        result.add(List.of(1, 1));
        int i = 1;
        while (numRows - i - 1 > 0) {
            List<Integer> list = new ArrayList<>();
            list.add(1);
            for (int j = 0; j < result.get(i).size() - 1; j++) {
                list.add(result.get(i).get(j) + result.get(i).get(j + 1));
            }
            list.add(1);
            result.add(list);
            i++;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("[[1]] : "  + generate(1));
        System.out.println("[[1], [1, 1]] : "  + generate(2));
        System.out.println("[[1], [1, 1], [1, 2, 1], [1, 3, 3, 1], [1, 4, 6, 4, 1]] : "  + generate(5));
        System.out.println(generate(30));
    }
}
