package algorithms.leetcode.algorithms;

public class _35_SearchInsertPosition {

    public static void main(String[] args) {
        assert 2 == searchInsert(new int[]{1, 3, 5, 6}, 5);
        assert 1 == searchInsert(new int[]{1, 3, 5, 6}, 2);
        assert 4 == searchInsert(new int[]{1, 3, 5, 6}, 7);
        assert 0 == searchInsert(new int[]{1, 3, 5, 6}, 0);
        assert 0 == searchInsert(new int[]{}, 1);
    }

    private static int searchInsert(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == target || target < nums[i]) {
                return i;
            }
        }
        return nums.length;
    }

}
