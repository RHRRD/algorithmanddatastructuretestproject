package algorithms.leetcode.algorithms;

import java.util.Arrays;

public class _821_ShortestDistanceToACharacter {

    public static void main(String[] args) {
        assert Arrays.equals(new int[]{3, 2, 1, 0, 1, 0, 0, 1, 2, 2, 1, 0}, shortestToChar("loveleetcode", 'e'));
        assert Arrays.equals(new int[]{0}, shortestToChar("l", 'l'));
        assert Arrays.equals(new int[]{2, 1, 0, 1}, shortestToChar("aaba", 'b'));
    }

    private static int[] shortestToChar(String S, char C) {
        int cnt = 1;
        char[] array = S.toCharArray();
        int i = 0, j = array.length - 1;
        int[] res = new int[array.length];
        while (array[i] != C) i++;
        while (array[j] != C) j--;

        for (int k = 0; k < i; k++) {
            res[k] = i - k;
        }
        for (int k = array.length - 1; k > j; k--) {
            res[k] = k - j;
        }
        for (int k = i + 1; k < j; k++) {
            if (array[k] == C) cnt = 0;
            res[k] = cnt++;
        }
        for (int k = j; k > i; k--) {
            if (array[k] == C) cnt = 0;
            if (res[k] != 0) res[k] = Math.min(res[k], cnt);
            cnt++;
        }

        return res;
    }

}
