package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/maximum-69-number/
 * <p>
 * You are given a positive integer num consisting only of digits 6 and 9.
 * <p>
 * Return the maximum number you can get by changing at most one digit (6 becomes 9, and 9 becomes 6).
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: num = 9669
 * Output: 9969
 * Explanation:
 * Changing the first digit results in 6669.
 * Changing the second digit results in 9969.
 * Changing the third digit results in 9699.
 * Changing the fourth digit results in 9666.
 * The maximum number is 9969.
 * Example 2:
 * <p>
 * Input: num = 9996
 * Output: 9999
 * Explanation: Changing the last digit 6 to 9 results in the maximum number.
 * Example 3:
 * <p>
 * Input: num = 9999
 * Output: 9999
 * Explanation: It is better not to apply any change.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= num <= 10^4
 * num consists of only 6 and 9 digits.
 */
public class _1323_Maximum69Number {

    public static int maximum69Number(int num) {
        char[] array = String.valueOf(num).toCharArray();
        StringBuilder result = new StringBuilder();
        boolean flag = true;
        for (char ch : array) {
            if (ch == '6' && flag) {
                result.append(9);
                flag = false;
            } else {
                result.append(ch);
            }
        }
        return Integer.parseInt(result.toString());
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(9999, maximum69Number(9996), 9996);
        CheckAnswerUtil.check(9969, maximum69Number(9669), 9669);
        CheckAnswerUtil.check(9999, maximum69Number(9999), 9999);
        CheckAnswerUtil.check(9, maximum69Number(6), 6);
        CheckAnswerUtil.check(9, maximum69Number(9), 9);
    }
}
