package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

public class _938_RangeSumOfBST {

    public static void main(String[] args) {
        TreeNode tt = new TreeNode(10);
        TreeNode t1 = new TreeNode(5);
        TreeNode t2 = new TreeNode(15);
        TreeNode t3 = new TreeNode(3);
        TreeNode t4 = new TreeNode(7);
        TreeNode t5 = new TreeNode(0);
        TreeNode t6 = new TreeNode(18);
        tt.left = t1;
        t1.left = t3;
        t1.right = t4;
        tt.right = t2;
        t2.left = t5;
        t2.right = t6;

        System.out.println(rangeSumBST(tt, 7, 15));
    }

    private static int rangeSumBST(TreeNode root, int L, int R) {
        if (root == null) {
            return 0;
        }
        int val = L <= root.val && root.val <= R ? root.val : 0;
        return val + rangeSumBST(root.left, L, R) + rangeSumBST(root.right, L, R);
    }
}
