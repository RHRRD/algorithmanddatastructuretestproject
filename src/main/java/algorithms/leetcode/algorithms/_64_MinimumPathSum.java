package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/minimum-path-sum/
 * <p>
 * Given a m x n grid filled with non-negative numbers, find a path from top left to bottom right, which minimizes the sum of all numbers along its path.
 * <p>
 * Note: You can only move either down or right at any point in time.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: grid = [[1,3,1],[1,5,1],[4,2,1]]
 * Output: 7
 * Explanation: Because the path 1 → 3 → 1 → 1 → 1 minimizes the sum.
 * Example 2:
 * <p>
 * Input: grid = [[1,2,3],[4,5,6]]
 * Output: 12
 * <p>
 * <p>
 * Constraints:
 * <p>
 * m == grid.length
 * n == grid[i].length
 * 1 <= m, n <= 200
 * 0 <= grid[i][j] <= 100
 */
public class _64_MinimumPathSum {

    public static int minPathSum(int[][] grid) {
        for (int i = 1; i < grid[0].length; i++) {
            grid[0][i] += grid[0][i - 1];
        }
        for (int i = 1; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                grid[i][j] += Math.min(grid[i - 1][j], j == 0 ? grid[i - 1][j] : grid[i][j - 1]);
            }
        }
        return grid[grid.length - 1][grid[grid.length - 1].length - 1];
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(7, minPathSum(new int[][]{{1, 3, 1}, {1, 5, 1}, {4, 2, 1}}));

        CheckAnswerUtil.check(12, minPathSum(new int[][]{{1, 2, 3}, {4, 5, 6}}));

        CheckAnswerUtil.check(3, minPathSum(new int[][]{{1, 1}, {1, 1}}));

        CheckAnswerUtil.check(7, minPathSum(new int[][]{{1, 2}, {3, 4}}));

        CheckAnswerUtil.check(10, minPathSum(new int[][]{{10}}));

        CheckAnswerUtil.check(10, minPathSum(new int[][]{{5}, {5}}));
    }
}
