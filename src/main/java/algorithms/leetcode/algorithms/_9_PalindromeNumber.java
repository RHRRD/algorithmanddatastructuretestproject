package algorithms.leetcode.algorithms;

public class _9_PalindromeNumber {

    public static void main(String[] args) {
        assert isPalindrome(121);
        assert !isPalindrome(-121);
        assert !isPalindrome(10);
    }

    private static boolean isPalindrome(int x) {
        if (x < 0) return false;
        int y = x;
        int res = 0;
        while (y > 0) {
            int tmp = y % 10;
            y /= 10;
            res = res * 10 + tmp;
        }
        return x == res;
    }

}
