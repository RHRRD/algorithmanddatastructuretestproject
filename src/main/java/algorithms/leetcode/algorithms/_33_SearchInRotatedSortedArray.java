package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/search-in-rotated-sorted-array/
 * <p>
 * There is an integer array nums sorted in ascending order (with distinct values).
 * <p>
 * Prior to being passed to your function, nums is possibly rotated at an unknown pivot index k (1 <= k < nums.length) such that the resulting array is [nums[k], nums[k+1], ..., nums[n-1], nums[0], nums[1], ..., nums[k-1]] (0-indexed). For example, [0,1,2,4,5,6,7] might be rotated at pivot index 3 and become [4,5,6,7,0,1,2].
 * <p>
 * Given the array nums after the possible rotation and an integer target, return the index of target if it is in nums, or -1 if it is not in nums.
 * <p>
 * You must write an algorithm with O(log n) runtime complexity.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [4,5,6,7,0,1,2], target = 0
 * Output: 4
 * Example 2:
 * <p>
 * Input: nums = [4,5,6,7,0,1,2], target = 3
 * Output: -1
 * Example 3:
 * <p>
 * Input: nums = [1], target = 0
 * Output: -1
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 5000
 * -10^4 <= nums[i] <= 10^4
 * All values of nums are unique.
 * nums is an ascending array that is possibly rotated.
 * -10^4 <= target <= 10^4
 */
public class _33_SearchInRotatedSortedArray {

    public static int search(int[] nums, int target) {
        int rot = 0;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] > nums[i]) {
                rot = i;
                break;
            }
        }
        int search1 = leftSearch(nums, 0, rot - 1, target);
        int search2 = leftSearch(nums, rot, nums.length - 1, target);
        return nums[search1] == target ? search1 : nums[search2] == target ? search2 : -1;
    }

    public static int leftSearch(int[] nums, int left, int right, int target) {
        while (left < right) {
            int mid = (left + right) / 2;
            if (nums[mid] >= target) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(4, search(new int[]{4, 5, 6, 7, 0, 1, 2}, 0));
        CheckAnswerUtil.check(-1, search(new int[]{4, 5, 6, 7, 0, 1, 2}, 3));
        CheckAnswerUtil.check(-1, search(new int[]{1}, 0));
        CheckAnswerUtil.check(1, search(new int[]{0, 1, 2, 4, 5, 6, 7}, 1));
        CheckAnswerUtil.check(5, search(new int[]{0, 1, 2, 4, 5, 6, 7}, 6));
        CheckAnswerUtil.check(6, search(new int[]{0, 1, 2, 4, 5, 6, 7}, 7));
        CheckAnswerUtil.check(6, search(new int[]{5, 6, 7, 0, 1, 2, 4}, 4));
        CheckAnswerUtil.check(0, search(new int[]{5, 6, 7, 0, 1, 2, 4}, 5));
        CheckAnswerUtil.check(6, search(new int[]{1, 2, 4, 5, 6, 7, 0}, 0));
        CheckAnswerUtil.check(0, search(new int[]{1, 2, 4, 5, 6, 7, 0}, 1));
        CheckAnswerUtil.check(-1, search(new int[]{1, 2, 4, 5, 6, 7, 0}, 3));
        CheckAnswerUtil.check(-1, search(new int[]{0, 1, 2, 4, 5, 6, 7}, 3));
        CheckAnswerUtil.check(-1, search(new int[]{5, 6, 7, 0, 1, 2, 4}, 3));
        CheckAnswerUtil.check(-1, search(new int[]{4, 5, 6, 7, 0, 1, 2}, 3));
        CheckAnswerUtil.check(-1, search(new int[]{1, 3, 5}, 6));
        CheckAnswerUtil.check(1, search(new int[]{5, 1, 2, 3, 4}, 1));
    }
}
