package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/number-of-common-factors/
 * <p>
 * Given two positive integers a and b, return the number of common factors of a and b.
 * <p>
 * An integer x is a common factor of a and b if x divides both a and b.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: a = 12, b = 6
 * Output: 4
 * Explanation: The common factors of 12 and 6 are 1, 2, 3, 6.
 * Example 2:
 * <p>
 * Input: a = 25, b = 30
 * Output: 2
 * Explanation: The common factors of 25 and 30 are 1, 5.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= a, b <= 1000
 */
public class _2427_NumberOfCommonFactors {

    public static int commonFactors(int a, int b) {
        int res = 0;
        for (int i = 1; i <= Math.min(a, b); i++) {
            if (a % i == 0 && b % i == 0) {
                res++;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(4, commonFactors(12, 6));
        CheckAnswerUtil.check(2, commonFactors(25, 30));
    }
}
