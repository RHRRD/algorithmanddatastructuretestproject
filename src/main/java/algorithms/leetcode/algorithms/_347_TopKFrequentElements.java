package algorithms.leetcode.algorithms;

import java.util.*;
import java.util.stream.Collectors;

/**
 * https://leetcode.com/problems/top-k-frequent-elements/description/
 * Given an integer array nums and an integer k, return the k most frequent elements. You may return the answer in any order.
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [1,1,1,2,2,3], k = 2
 * Output: [1,2]
 * Example 2:
 * <p>
 * Input: nums = [1], k = 1
 * Output: [1]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 10^5
 * -10^4 <= nums[i] <= 10^4
 * k is in the range [1, the number of unique elements in the array].
 * It is guaranteed that the answer is unique.
 * <p>
 * <p>
 * Follow up: Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
 */
public class _347_TopKFrequentElements {

    public static int[] topKFrequent(int[] nums, int k) {
        Map<Integer, Integer> map = new HashMap<>();
        Map<Integer, List<Integer>> mapReverse = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], map.getOrDefault(nums[i], 0) + 1);
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            List<Integer> list = mapReverse.getOrDefault(entry.getValue(), new ArrayList<>());
            list.add(entry.getKey());
            mapReverse.put(entry.getValue(), list);
        }
        List<Integer> list = map.values().stream()
                .distinct()
                .sorted(Comparator.reverseOrder())
                .flatMap(it -> mapReverse.get(it).stream())
                .limit(k)
                .collect(Collectors.toList());
        int[] result = new int[k];
        for (int i = 0; i < list.size(); i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(topKFrequent(new int[]{1, 1, 1, 2, 2, 3}, 2)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{2, 3, 1, 1, 2, 1}, 2)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{1}, 1)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{1, 2}, 2)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{1, 2}, 1)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{3, 4, 1, 2}, 4)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{3, 3, 1, 2, 3}, 1)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{2, 3, 3, 1, 2, 2, 2, 3}, 1)));
        System.out.println(Arrays.toString(topKFrequent(new int[]{3, 2, 3, 1, 2, 4, 5, 5, 6, 7, 7, 8, 2, 3, 1, 1, 1, 10, 11, 5, 6, 2, 4, 7, 8, 5, 6}, 10)));
    }
}
