package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/binary-tree-paths/
 * <p>
 * Given the root of a binary tree, return all root-to-leaf paths in any order.
 * <p>
 * A leaf is a node with no children.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [1,2,3,null,5]
 * Output: ["1->2->5","1->3"]
 * Example 2:
 * <p>
 * Input: root = [1]
 * Output: ["1"]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 100].
 * -100 <= Node.val <= 100
 */
public class _257_BinaryTreePaths {

    public static List<String> binaryTreePaths(TreeNode root) {
        if (root == null) {
            return List.of();
        }
        List<String> resultList = new ArrayList<>();
        String path = String.valueOf(root.val);
        if (root.right == null && root.left == null) {
            resultList.add(path);
            return resultList;
        }
        for (String str : binaryTreePaths(root.left)) {
            resultList.add(path + "->" + str);
        }
        for (String str : binaryTreePaths(root.right)) {
            resultList.add(path + "->" + str);
        }
        return resultList;
    }

    public static void main(String[] args) {
        System.out.println(binaryTreePaths(new TreeNode(1)));
        System.out.println(binaryTreePaths(new TreeNode(1, new TreeNode(2, null, new TreeNode(5)), new TreeNode(3))));
    }
}
