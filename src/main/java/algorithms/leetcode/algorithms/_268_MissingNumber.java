package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/missing-number/
 * <p>
 * Given an array nums containing n distinct numbers in the range [0, n], return the only number in the range that is missing from the array.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [3,0,1]
 * Output: 2
 * Explanation: n = 3 since there are 3 numbers, so all numbers are in the range [0,3]. 2 is the missing number in the range since it does not appear in nums.
 * Example 2:
 * <p>
 * Input: nums = [0,1]
 * Output: 2
 * Explanation: n = 2 since there are 2 numbers, so all numbers are in the range [0,2]. 2 is the missing number in the range since it does not appear in nums.
 * Example 3:
 * <p>
 * Input: nums = [9,6,4,2,3,5,7,0,1]
 * Output: 8
 * Explanation: n = 9 since there are 9 numbers, so all numbers are in the range [0,9]. 8 is the missing number in the range since it does not appear in nums.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * n == nums.length
 * 1 <= n <= 10^4
 * 0 <= nums[i] <= n
 * All the numbers of nums are unique.
 */
public class _268_MissingNumber {

    public static int missingNumber(int[] nums) {
        int odd = 0, even = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 == 0) {
                even += nums[i];
            } else {
                odd += nums[i];
            }
        }
        int n = nums.length;
        if (n % 2 == 0) {
            return Math.abs(even - (odd + n / 2));
        } else {
            return Math.abs(odd - (even + n / 2 + 1));
        }
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(1, missingNumber(new int[]{0}));
        CheckAnswerUtil.check(0, missingNumber(new int[]{1}));
        CheckAnswerUtil.check(2, missingNumber(new int[]{3, 0, 1}));
        CheckAnswerUtil.check(2, missingNumber(new int[]{0, 1}));
        CheckAnswerUtil.check(8, missingNumber(new int[]{9, 6, 4, 2, 3, 5, 7, 0, 1}));
        CheckAnswerUtil.check(7, missingNumber(new int[]{0, 1, 2, 3, 4, 5, 6, 8, 9}));
        CheckAnswerUtil.check(6, missingNumber(new int[]{0, 1, 2, 3, 4, 5, 7, 8, 9}));
    }
}
