package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/generate-a-string-with-characters-that-have-odd-counts/
 * <p>
 * Given an integer n, return a string with n characters such that each character in such string occurs an odd number of times.
 * <p>
 * The returned string must contain only lowercase English letters. If there are multiples valid strings, return any of them.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: n = 4
 * Output: "pppz"
 * Explanation: "pppz" is a valid string since the character 'p' occurs three times and the character 'z' occurs once. Note that there are many other valid strings such as "ohhh" and "love".
 * Example 2:
 * <p>
 * Input: n = 2
 * Output: "xy"
 * Explanation: "xy" is a valid string since the characters 'x' and 'y' occur once. Note that there are many other valid strings such as "ag" and "ur".
 * Example 3:
 * <p>
 * Input: n = 7
 * Output: "holasss"
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= n <= 500
 */
public class _1374_GenerateAStringWithCharactersThatHaveOddCounts {

    public static String generateTheString(int n) {
        StringBuilder res = new StringBuilder();
        if (n % 2 == 0) {
            res.append("a".repeat(n - 1));
            res.append("b");
        } else {
            res.append("a".repeat(n));
        }
        return res.toString();
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check("aaab", generateTheString(4), 4);
        CheckAnswerUtil.check("ab", generateTheString(2), 2);
        CheckAnswerUtil.check("a", generateTheString(1), 1);
    }
}
