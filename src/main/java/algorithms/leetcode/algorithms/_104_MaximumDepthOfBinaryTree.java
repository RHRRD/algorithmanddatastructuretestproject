package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

/**
 * https://leetcode.com/problems/maximum-depth-of-binary-tree/
 * <p>
 * Given the root of a binary tree, return its maximum depth.
 * <p>
 * A binary tree's maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 3
 * Example 2:
 * <p>
 * Input: root = [1,null,2]
 * Output: 2
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [0, 104].
 * -100 <= Node.val <= 100
 */

public class _104_MaximumDepthOfBinaryTree {

    public static int maxDepth(TreeNode root) {
        return maxDepth(root, 0);
    }

    public static int maxDepth(TreeNode node, int currentHigh) {
        if (node == null) {
            return currentHigh;
        }
        return Math.max(
                maxDepth(node.left, currentHigh + 1),
                maxDepth(node.right, currentHigh + 1)
        );
    }

    public static void main(String[] args) {
        TreeNode root1 = null;
        System.out.println(maxDepth(root1));

        TreeNode root2 = new TreeNode(10);
        System.out.println(maxDepth(root2));

        TreeNode root3 = new TreeNode(1, null, new TreeNode(2));
        System.out.println(maxDepth(root3));

        TreeNode root4 = new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)));
        System.out.println(maxDepth(root4));
    }

}
