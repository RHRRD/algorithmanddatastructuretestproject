package algorithms.leetcode.algorithms;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/successful-pairs-of-spells-and-potions/
 * <p>
 * You are given two positive integer arrays spells and potions, of length n and m respectively, where spells[i] represents the strength of the ith spell and potions[j] represents the strength of the jth potion.
 * <p>
 * You are also given an integer success. A spell and potion pair is considered successful if the product of their strengths is at least success.
 * <p>
 * Return an integer array pairs of length n where pairs[i] is the number of potions that will form a successful pair with the ith spell.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: spells = [5,1,3], potions = [1,2,3,4,5], success = 7
 * Output: [4,0,3]
 * Explanation:
 * - 0th spell: 5 * [1,2,3,4,5] = [5,10,15,20,25]. 4 pairs are successful.
 * - 1st spell: 1 * [1,2,3,4,5] = [1,2,3,4,5]. 0 pairs are successful.
 * - 2nd spell: 3 * [1,2,3,4,5] = [3,6,9,12,15]. 3 pairs are successful.
 * Thus, [4,0,3] is returned.
 * Example 2:
 * <p>
 * Input: spells = [3,1,2], potions = [8,5,8], success = 16
 * Output: [2,0,2]
 * Explanation:
 * - 0th spell: 3 * [8,5,8] = [24,15,24]. 2 pairs are successful.
 * - 1st spell: 1 * [8,5,8] = [8,5,8]. 0 pairs are successful.
 * - 2nd spell: 2 * [8,5,8] = [16,10,16]. 2 pairs are successful.
 * Thus, [2,0,2] is returned.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * n == spells.length
 * m == potions.length
 * 1 <= n, m <= 10^5
 * 1 <= spells[i], potions[i] <= 10^5
 * 1 <= success <= 10^10
 */
public class _2300_SuccessfulPairsOfSpellsAndPotions {

    public static int[] successfulPairs(int[] spells, int[] potions, long success) {
        long[] array = new long[potions.length];
        for (int i = 0; i < potions.length; i++) {
            boolean flag = success % potions[i] == 0;
            array[i] = success / potions[i];
            if (!flag) {
                array[i]++;
            }
        }
        Arrays.sort(array);
        int[] res = new int[spells.length];
        for (int i = 0; i < spells.length; i++) {
            res[i] = leftSearch(array, 0, array.length, spells[i]);
        }
        return res;
    }

    private static int leftSearch(long[] array, int left, int right, long target) {
        while (left < right) {
            int mid = (left + right) / 2;
            if (array[mid] > target) {
                right = mid;
            } else {
                left = mid + 1;
            }
        }
        return left;
    }

    public static void main(String[] args) {
        System.out.println("[4, 0, 3] : " + Arrays.toString(successfulPairs(new int[]{5, 1, 3}, new int[]{1, 2, 3, 4, 5}, 7)));
        System.out.println("[2, 0, 2] : " + Arrays.toString(successfulPairs(new int[]{3, 1, 2}, new int[]{8, 5, 8}, 16)));
        System.out.println("[1] : " + Arrays.toString(successfulPairs(new int[]{100000}, new int[]{100000}, 1000000000)));
    }
}
