package algorithms.leetcode.algorithms;

import java.util.*;

/**
 * https://leetcode.com/problems/sort-the-matrix-diagonally/
 * <p>
 * A matrix diagonal is a diagonal line of cells starting from some cell in either the topmost row or leftmost column and going in the bottom-right direction until reaching the matrix's end. For example, the matrix diagonal starting from mat[2][0], where mat is a 6 x 3 matrix, includes cells mat[2][0], mat[3][1], and mat[4][2].
 * <p>
 * Given an m x n matrix mat of integers, sort each matrix diagonal in ascending order and return the resulting matrix.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: mat = [[3,3,1,1],[2,2,1,2],[1,1,1,2]]
 * Output: [[1,1,1,1],[1,2,2,2],[1,2,3,3]]
 * Example 2:
 * <p>
 * Input: mat = [[11,25,66,1,69,7],[23,55,17,45,15,52],[75,31,36,44,58,8],[22,27,33,25,68,4],[84,28,14,11,5,50]]
 * Output: [[5,17,4,1,52,7],[11,11,25,45,8,69],[14,23,25,44,58,15],[22,27,31,36,50,66],[84,28,75,33,55,68]]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * m == mat.length
 * n == mat[i].length
 * 1 <= m, n <= 100
 * 1 <= mat[i][j] <= 100
 */
public class _1329_SortTheMatrixDiagonally {

    public static int[][] diagonalSort(int[][] mat) {
        int[][] result = new int[mat.length][mat[0].length];
        for (int k = mat.length - 1; k >= 0; k--) {
            List<Integer> list = new ArrayList<>();
            for (int i = k, j = 0; i < mat.length && j < mat[i].length; i++, j++) {
                list.add(mat[i][j]);
            }
            Collections.sort(list);
            for (int i = k, j = 0; i < mat.length && j < mat[i].length; i++, j++) {
                result[i][j] = list.get(j);
            }
        }
        for (int k = 1; k < mat[0].length; k++) {
            List<Integer> list = new ArrayList<>();
            for (int i = 0, j = k; i < mat.length && j < mat[i].length; i++, j++) {
                list.add(mat[i][j]);
            }
            Collections.sort(list);
            for (int i = 0, j = k; i < mat.length && j < mat[i].length; i++, j++) {
                result[i][j] = list.get(i);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[][] mat = {{3, 3, 1, 1}, {2, 2, 1, 2}, {1, 1, 1, 2}};
        printMatrix(mat);
        printMatrix(diagonalSort(mat));
        int[][] mat2 = {{11, 25, 66, 1, 69, 7}, {23, 55, 17, 45, 15, 52}, {75, 31, 36, 44, 58, 8}, {22, 27, 33, 25, 68, 4}, {84, 28, 14, 11, 5, 50}};
        printMatrix(mat2);
        printMatrix(diagonalSort(mat2));
    }

    private static void printMatrix(int[][] matrix) {
        System.out.println("--------------------------");
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("--------------------------");
    }

}
