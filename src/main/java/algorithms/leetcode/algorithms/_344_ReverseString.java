package algorithms.leetcode.algorithms;

public class _344_ReverseString {

    public static void main(String[] args) {
        char[] test = {'h', 'e', 'l', 'l', 'o'};
        reverseString(test);
        System.out.println(new String(test));
    }

    public static void reverseString(char[] s) {
        for (int i = 0; i < s.length / 2; i++) {
            char tmp = s[i];
            s[i] = s[s.length - 1 - i];
            s[s.length - 1 - i] = tmp;
        }
    }
}
