package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/sum-of-unique-elements/
 * <p>
 * You are given an integer array nums. The unique elements of an array are the elements that appear exactly once in the array.
 * <p>
 * Return the sum of all the unique elements of nums.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [1,2,3,2]
 * Output: 4
 * Explanation: The unique elements are [1,3], and the sum is 4.
 * Example 2:
 * <p>
 * Input: nums = [1,1,1,1,1]
 * Output: 0
 * Explanation: There are no unique elements, and the sum is 0.
 * Example 3:
 * <p>
 * Input: nums = [1,2,3,4,5]
 * Output: 15
 * Explanation: The unique elements are [1,2,3,4,5], and the sum is 15.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 100
 * 1 <= nums[i] <= 100
 */
public class _1748_SumOfUniqueElements {

    public static int sumOfUnique(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], map.getOrDefault(nums[i], 0) + 1);
        }
        int res = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                res += entry.getKey();
            }
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(4, sumOfUnique(new int[]{1, 2, 3, 2}));
        CheckAnswerUtil.check(0, sumOfUnique(new int[]{1, 1, 1, 1, 1}));
        CheckAnswerUtil.check(15, sumOfUnique(new int[]{1, 2, 3, 4, 5}));
    }
}
