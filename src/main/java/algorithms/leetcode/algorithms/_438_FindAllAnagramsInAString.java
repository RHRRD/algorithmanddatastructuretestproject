package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/find-all-anagrams-in-a-string/
 * <p>
 * Given two strings s and p, return an array of all the start indices of p's anagrams in s. You may return the answer in any order.
 * <p>
 * An Anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "cbaebabacd", p = "abc"
 * Output: [0,6]
 * Explanation:
 * The substring with start index = 0 is "cba", which is an anagram of "abc".
 * The substring with start index = 6 is "bac", which is an anagram of "abc".
 * Example 2:
 * <p>
 * Input: s = "abab", p = "ab"
 * Output: [0,1,2]
 * Explanation:
 * The substring with start index = 0 is "ab", which is an anagram of "ab".
 * The substring with start index = 1 is "ba", which is an anagram of "ab".
 * The substring with start index = 2 is "ab", which is an anagram of "ab".
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length, p.length <= 3 * 10^4
 * s and p consist of lowercase English letters.
 */
public class _438_FindAllAnagramsInAString {

    public static List<Integer> findAnagrams(String s, String p) {
        List<Integer> result = new ArrayList<>();
        if (s.length() < p.length()) {
            return result;
        }
        char[] array = new char[26];
        for (int i = 0; i < p.length(); i++) {
            array[p.charAt(i) - 'a']++;
            array[s.charAt(i) - 'a']--;
        }
        if (isZeros(array)) {
            result.add(0);
        }
        for (int i = 1; i < s.length() - p.length() + 1; i++) {
            array[s.charAt(i - 1) - 'a']++;
            array[s.charAt(i + p.length() - 1) - 'a']--;
            if (isZeros(array)) {
                result.add(i);
            }
        }
        return result;
    }

    private static boolean isZeros(char[] array) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("[0, 6]: " + findAnagrams("cbaebabacd", "abc"));
        System.out.println("[0, 1, 2]: " + findAnagrams("abab", "ab"));
    }
}
