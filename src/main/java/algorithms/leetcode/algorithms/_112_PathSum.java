package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

/**
 * https://leetcode.com/problems/path-sum/
 * <p>
 * Given the root of a binary tree and an integer targetSum, return true if the tree has a root-to-leaf path such that adding up all the values along the path equals targetSum.
 * <p>
 * A leaf is a node with no children.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [5,4,8,11,null,13,4,7,2,null,null,null,1], targetSum = 22
 * Output: true
 * Explanation: The root-to-leaf path with the target sum is shown.
 * Example 2:
 * <p>
 * <p>
 * Input: root = [1,2,3], targetSum = 5
 * Output: false
 * Explanation: There two root-to-leaf paths in the tree:
 * (1 --> 2): The sum is 3.
 * (1 --> 3): The sum is 4.
 * There is no root-to-leaf path with sum = 5.
 * Example 3:
 * <p>
 * Input: root = [], targetSum = 0
 * Output: false
 * Explanation: Since the tree is empty, there are no root-to-leaf paths.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [0, 5000].
 * -1000 <= Node.val <= 1000
 * -1000 <= targetSum <= 1000
 */
public class _112_PathSum {

    public static boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null) {
            return false;
        } else if (root.left == null && root.right == null) {
            return root.val == targetSum;
        }
        return hasPathSum(root, targetSum, 0);
    }

    private static boolean hasPathSum(TreeNode node, int targetSum, int currentSum) {
        if (node.right == null && node.left == null) {
            return targetSum == currentSum + node.val;
        }
        if (node.left != null && node.right != null) {
            return hasPathSum(node.left, targetSum, currentSum + node.val) || hasPathSum(node.right, targetSum, currentSum + node.val);
        } else if (node.left != null) {
            return hasPathSum(node.left, targetSum, currentSum + node.val);
        } else {
            return hasPathSum(node.right, targetSum, currentSum + node.val);
        }
    }

    public static void main(String[] args) {
        System.out.println(hasPathSum(new TreeNode(1, new TreeNode(2), new TreeNode(3)), 5));
        System.out.println(false);
        System.out.println();

        System.out.println(hasPathSum(new TreeNode(5, new TreeNode(4, new TreeNode(11, new TreeNode(7), new TreeNode(2)), null), new TreeNode(8, new TreeNode(13), new TreeNode(4, null, new TreeNode(1)))), 22));
        System.out.println(true);
        System.out.println();

        System.out.println(hasPathSum(null, 0));
        System.out.println(false);
        System.out.println();

        System.out.println(hasPathSum(new TreeNode(1), 1));
        System.out.println(true);
        System.out.println();

        System.out.println(hasPathSum(new TreeNode(1, new TreeNode(2), null), 1));
        System.out.println(false);
        System.out.println();
    }
}
