package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

/**
 * https://leetcode.com/problems/merge-k-sorted-lists/
 * <p>
 * You are given an array of k linked-lists lists, each linked-list is sorted in ascending order.
 * <p>
 * Merge all the linked-lists into one sorted linked-list and return it.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: lists = [[1,4,5],[1,3,4],[2,6]]
 * Output: [1,1,2,3,4,4,5,6]
 * Explanation: The linked-lists are:
 * [
 * 1->4->5,
 * 1->3->4,
 * 2->6
 * ]
 * merging them into one sorted list:
 * 1->1->2->3->4->4->5->6
 * Example 2:
 * <p>
 * Input: lists = []
 * Output: []
 * Example 3:
 * <p>
 * Input: lists = [[]]
 * Output: []
 * <p>
 * <p>
 * Constraints:
 * <p>
 * k == lists.length
 * 0 <= k <= 10^4
 * 0 <= lists[i].length <= 500
 * -10^4 <= lists[i][j] <= 10^4
 * lists[i] is sorted in ascending order.
 * The sum of lists[i].length will not exceed 10^4.
 */
public class _23_MergeKSortedLists {

    public static ListNode mergeKLists(ListNode[] lists) {
        if (lists.length == 0) {
            return null;
        }
        if (lists.length == 1) {
            return lists[0];
        }
        if (lists.length == 2) {
            return mergeTwoLists(lists[0], lists[1]);
        }
        ListNode result = null;
        for (int i = 0; i < lists.length; i++) {
            result = mergeTwoLists(result, lists[i]);
        }
        return result;
    }

    public static ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode head = new ListNode();
        ListNode current = head;
        if (list1 == null) {
            while (list2 != null) {
                current.next = list2;
                current = current.next;
                list2 = list2.next;
            }
        } else if (list2 == null) {
            while (list1 != null) {
                current.next = list1;
                current = current.next;
                list1 = list1.next;
            }
        } else {
            while (list1 != null || list2 != null) {
                if (list1 == null) {
                    current.next = list2;
                    current = current.next;
                    list2 = list2.next;
                } else if (list2 == null) {
                    current.next = list1;
                    current = current.next;
                    list1 = list1.next;
                } else {
                    if (list1.val < list2.val) {
                        current.next = list1;
                        current = current.next;
                        list1 = list1.next;
                    } else {
                        current.next = list2;
                        current = current.next;
                        list2 = list2.next;
                    }
                }
            }
        }
        return head.next;
    }

    public static void main(String[] args) {
        System.out.println(mergeTwoLists(new ListNode(1, new ListNode(2, new ListNode(3))), new ListNode(4, new ListNode(5, new ListNode(6)))));

        System.out.println(mergeTwoLists(new ListNode(1, new ListNode(3, new ListNode(5))), new ListNode(2, new ListNode(4, new ListNode(6)))));

        System.out.println(mergeTwoLists(new ListNode(1, new ListNode(3, new ListNode(5))), new ListNode(2, new ListNode(4, new ListNode(6, new ListNode(8))))));

        System.out.println(mergeTwoLists(new ListNode(1, new ListNode(3, new ListNode(5, new ListNode(7, new ListNode(9))))), new ListNode(2, new ListNode(4, new ListNode(6, new ListNode(8))))));

        System.out.println(mergeTwoLists(new ListNode(1), new ListNode(2)));

        System.out.println(mergeTwoLists(new ListNode(1, new ListNode(4, new ListNode(5))), new ListNode(1, new ListNode(3, new ListNode(4)))));

        System.out.println(mergeKLists(new ListNode[]{
                new ListNode(1, new ListNode(4, new ListNode(5))),
                new ListNode(1, new ListNode(3, new ListNode(4))),
                new ListNode(2, new ListNode(6))
        }));

        System.out.println(mergeKLists(new ListNode[]{
                new ListNode(1, new ListNode(4, new ListNode(5))),
                new ListNode(1, new ListNode(5, new ListNode(10))),
                new ListNode(2, new ListNode(6)),
                new ListNode(-1)
        }));

        System.out.println(mergeKLists(new ListNode[]{}));

        System.out.println(mergeKLists(new ListNode[]{
                null
        }));

        System.out.println(mergeKLists(new ListNode[]{
                new ListNode()
        }));
    }
}
