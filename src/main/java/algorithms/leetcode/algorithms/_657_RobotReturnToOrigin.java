package algorithms.leetcode.algorithms;

public class _657_RobotReturnToOrigin {

    public static void main(String[] args) {
        System.out.println(judgeCircle("UDRL"));
        System.out.println(judgeCircle("LL"));
        System.out.println(judgeCircle("UD"));
    }

    private static boolean judgeCircle(String moves) {
        int vert = 0, gor = 0;
        for (char c : moves.toCharArray()) {
            if (c == 'U') {
                vert++;
            }
            if (c == 'D') {
                vert--;
            }
            if (c == 'L') {
                gor--;
            }
            if (c == 'R') {
                gor++;
            }
        }
        return vert == 0 && gor == 0;
    }
}
