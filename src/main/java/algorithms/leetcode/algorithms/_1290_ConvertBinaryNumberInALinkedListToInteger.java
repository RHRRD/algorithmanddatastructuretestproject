package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

public class _1290_ConvertBinaryNumberInALinkedListToInteger {

    public int getDecimalValue(ListNode head) {
        String s = "";
        do {
            s += head.val;
            head = head.next;
        } while (head != null);
        return Integer.parseInt(s, 2);
    }
}
