package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/balanced-binary-tree/
 * <p>
 * Given a binary tree, determine if it is
 * height-balanced
 * .
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [3,9,20,null,null,15,7]
 * Output: true
 * Example 2:
 * <p>
 * <p>
 * Input: root = [1,2,2,3,3,null,null,4,4]
 * Output: false
 * Example 3:
 * <p>
 * Input: root = []
 * Output: true
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [0, 5000].
 * -10^4 <= Node.val <= 10^4
 */
public class _110_BalancedBinaryTree {

    public static boolean isBalanced(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) {
            return true;
        }
        return checkHeight(root) && isBalanced(root.left) && isBalanced(root.right);
    }

    private static boolean checkHeight(TreeNode root) {
        return Math.abs(getHeight(root.left, 1) - getHeight(root.right, 1)) <= 1;
    }

    private static int getHeight(TreeNode node, int currentHeight) {
        if (node == null) {
            return currentHeight;
        }
        return Math.max(getHeight(node.left, currentHeight + 1), getHeight(node.right, currentHeight + 1));
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, isBalanced(new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)))));
        CheckAnswerUtil.check(false, isBalanced(new TreeNode(1, new TreeNode(2, new TreeNode(3, new TreeNode(4), new TreeNode(4)), new TreeNode(3)), new TreeNode(2))));
        CheckAnswerUtil.check(true, isBalanced(new TreeNode()));
        CheckAnswerUtil.check(true, isBalanced(new TreeNode(1)));
        CheckAnswerUtil.check(true, isBalanced(new TreeNode(1, new TreeNode(2), new TreeNode(3))));
        CheckAnswerUtil.check(false, isBalanced(new TreeNode(1, new TreeNode(2, new TreeNode(3, new TreeNode(4), null), null), new TreeNode(2, null, new TreeNode(3, null, new TreeNode(4))))));
    }
}
