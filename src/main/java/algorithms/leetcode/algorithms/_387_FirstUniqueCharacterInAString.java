package algorithms.leetcode.algorithms;

import java.util.HashMap;
import java.util.Map;

public class _387_FirstUniqueCharacterInAString {

    public static void main(String[] args) {
        assert 0 == firstUniqChar("leetcode");
        assert 0 == firstUniqChar("a");
        assert -1 == firstUniqChar("");
        assert -1 == firstUniqChar("abab");
        assert 2 == firstUniqChar("loveleetcode");
    }

    private static int firstUniqChar(String s) {
        char[] array = s.toCharArray();
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < array.length; i++) {
            map.put(array[i], map.getOrDefault(array[i], 0) + 1);
        }
        for (int i = 0; i < array.length; i++) {
            if (map.get(array[i]) == 1) {
                return i;
            }
        }
        return -1;
    }

}
