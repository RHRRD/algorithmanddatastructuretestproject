package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

/**
 * https://leetcode.com/problems/convert-the-temperature/
 * <p>
 * You are given a non-negative floating point number rounded to two decimal places celsius, that denotes the temperature in Celsius.
 * <p>
 * You should convert Celsius into Kelvin and Fahrenheit and return it as an array ans = [kelvin, fahrenheit].
 * <p>
 * Return the array ans. Answers within 10-5 of the actual answer will be accepted.
 * <p>
 * Note that:
 * <p>
 * Kelvin = Celsius + 273.15
 * Fahrenheit = Celsius * 1.80 + 32.00
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: celsius = 36.50
 * Output: [309.65000,97.70000]
 * Explanation: Temperature at 36.50 Celsius converted in Kelvin is 309.65 and converted in Fahrenheit is 97.70.
 * Example 2:
 * <p>
 * Input: celsius = 122.11
 * Output: [395.26000,251.79800]
 * Explanation: Temperature at 122.11 Celsius converted in Kelvin is 395.26 and converted in Fahrenheit is 251.798.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 0 <= celsius <= 1000
 */
public class _2469_ConvertTheTemperature {

    public static double[] convertTemperature(double celsius) {
        double[] result = new double[2];
        result[0] = (double) Math.round((celsius + 273.15) * 100000d) / 100000d;
        result[1] = (double) Math.round((celsius * 1.80 + 32.00) * 100000d) / 100000d;
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(convertTemperature(36.50)));
        System.out.println("must be: [309.65000,97.70000]");
        System.out.println();

        System.out.println(Arrays.toString(convertTemperature(122.11)));
        System.out.println("must be: [395.26000,251.79800]");
        System.out.println();
    }

}
