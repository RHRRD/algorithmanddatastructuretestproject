package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/binary-tree-level-order-traversal/
 * <p>
 * Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [3,9,20,null,null,15,7]
 * Output: [[3],[9,20],[15,7]]
 * Example 2:
 * <p>
 * Input: root = [1]
 * Output: [[1]]
 * Example 3:
 * <p>
 * Input: root = []
 * Output: []
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [0, 2000].
 * -1000 <= Node.val <= 1000
 */
public class _102_BinaryTreeLevelOrderTraversal {

    public static List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> result = new ArrayList<>();
        levelOrder(root, result, 1);
        return result;
    }

    private static void levelOrder(TreeNode root, List<List<Integer>> result, int level) {
        if (root == null) {
            return;
        }
        if (result.size() < level) {
            ArrayList<Integer> tmp = new ArrayList<>();
            tmp.add(root.val);
            result.add(tmp);
        } else {
            List<Integer> list = result.get(level - 1);
            list.add(root.val);
        }
        levelOrder(root.left, result, level + 1);
        levelOrder(root.right, result, level + 1);
    }

    private static void levelOrder_1(TreeNode root) {
        if (root == null) {
//            System.out.println("null");
            return;
        }
        levelOrder_1(root, 0);
    }

    private static void levelOrder_1(TreeNode root, int level) {
        if (root == null) {
            return;
        }
        System.out.println(root.val + " " + level);
        levelOrder_1(root.left, level + 1);
        levelOrder_1(root.right, level + 1);
    }

    public static void main(String[] args) {
//        levelOrder_1(new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7))));
        System.out.println(levelOrder(new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)))));

        System.out.println(levelOrder(new TreeNode(1)));

        System.out.println(levelOrder(null));

        System.out.println(levelOrder(new TreeNode(1, null, new TreeNode(3))));

        System.out.println(levelOrder(new TreeNode(1, new TreeNode(2), null)));

        System.out.println(levelOrder(new TreeNode(1, new TreeNode(2, new TreeNode(4), null), new TreeNode(3, new TreeNode(5, new TreeNode(6), null), null))));

        System.out.println(levelOrder(new TreeNode(1, new TreeNode(2, new TreeNode(4, new TreeNode(8), new TreeNode(9)), new TreeNode(5, new TreeNode(10), new TreeNode(11))), new TreeNode(3, new TreeNode(6, new TreeNode(12), new TreeNode(13)), new TreeNode(7, new TreeNode(14), new TreeNode(15))))));
    }
}
