package algorithms.leetcode.algorithms;

import java.util.stream.Stream;

public class _557_ReverseWordsInAStringIII {

    public static void main(String[] args) {
        assert "s'teL ekat edoCteeL tsetnoc".equals(reverseWords("Let's take LeetCode contest"));
        assert "aholA".equals(reverseWords("Aloha"));
    }

    private static String reverseWords(String s) {
        StringBuilder sb = new StringBuilder();
        Stream.of(s.split(" ")).forEach(it -> sb.append(new StringBuilder(it).reverse().toString()).append(" "));
        return sb.substring(0, sb.length() - 1);
    }
}
