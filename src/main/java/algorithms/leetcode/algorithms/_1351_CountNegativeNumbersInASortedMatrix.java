package algorithms.leetcode.algorithms;

public class _1351_CountNegativeNumbersInASortedMatrix {


    public static void main(String[] args) {
        assert 8 == countNegatives(new int[][]{{4, 3, 2, -1}, {3, 2, 1, -1}, {1, 1, -1, -2}, {-1, -1, -2, -3}});
        assert 0 == countNegatives(new int[][]{{1, 1}, {0, 0}});
        assert 1 == countNegatives(new int[][]{{-1}});
    }

    private static int countNegatives(int[][] grid) {
        int count = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] < 0) count++;
            }
        }
        return count;
    }

}
