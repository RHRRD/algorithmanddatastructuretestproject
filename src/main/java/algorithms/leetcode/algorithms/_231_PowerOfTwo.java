package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/power-of-two/
 * <p>
 * Given an integer n, return true if it is a power of two. Otherwise, return false.
 * <p>
 * An integer n is a power of two, if there exists an integer x such that n == 2x.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: n = 1
 * Output: true
 * Explanation: 20 = 1
 * Example 2:
 * <p>
 * Input: n = 16
 * Output: true
 * Explanation: 24 = 16
 * Example 3:
 * <p>
 * Input: n = 3
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * -2^31 <= n <= 2^31 - 1
 */
public class _231_PowerOfTwo {

    public static boolean isPowerOfTwo(int n) {
        String bits = Integer.toString(n, 2);
        return n > 0 && bits.charAt(0) == '1' && (bits.length() == 1 || !bits.substring(1).contains("1"));
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(false, isPowerOfTwo(-2), -2);
        CheckAnswerUtil.check(false, isPowerOfTwo(0), 0);
        CheckAnswerUtil.check(true, isPowerOfTwo(1), 1);
        CheckAnswerUtil.check(true, isPowerOfTwo(2), 2);
        CheckAnswerUtil.check(true, isPowerOfTwo(16), 16);
        CheckAnswerUtil.check(true, isPowerOfTwo(1024), 1024);
        CheckAnswerUtil.check(false, isPowerOfTwo(3), 3);
        CheckAnswerUtil.check(false, isPowerOfTwo(1000), 1000);
    }
}
