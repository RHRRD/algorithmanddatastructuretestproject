package algorithms.leetcode.algorithms;

public class _461_HammingDistance {

    public static void main(String[] args) {
        System.out.println(hammingDistance(1, 4));
        System.out.println(hammingDistance(4, 1));
        System.out.println(hammingDistance(4, 2));
    }

    private static int hammingDistance(int x, int y) {
        char[] x2 = Integer.toString(x, 2).toCharArray();
        char[] y2 = Integer.toString(y, 2).toCharArray();
        int diff = 0;
        for (int i = 1; i <= Math.max(x2.length, y2.length); i++) {
            if (x2.length < i) {
                if (y2[y2.length - i] == '1') {
                    diff++;
                }
            } else if (y2.length < i) {
                if (x2[x2.length - i] == '1') {
                    diff++;
                }
            } else if (x2[x2.length - i] != y2[y2.length - i]) {
                diff++;
            }
        }
        return diff;
    }
}
