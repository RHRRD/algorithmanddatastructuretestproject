package algorithms.leetcode.algorithms;

import java.util.HashMap;
import java.util.Map;

public class _1189_MaximumNumberOfBalloons {

    public static void main(String[] args) {
        assert 1 == maxNumberOfBalloons("nlaebolko");
        assert 2 == maxNumberOfBalloons("loonbalxballpoon");
        assert 0 == maxNumberOfBalloons("leetcode");
    }

    private static int maxNumberOfBalloons(String text) {
        int[] cnt = new int[]{0, 0, 0, 0, 0, 0, 0};
        Map<String, Integer> map = new HashMap<>();
        map.put("b", 0);
        map.put("a", 1);
        map.put("l1", 2);
        map.put("l2", 3);
        map.put("o1", 4);
        map.put("o2", 5);
        map.put("n", 6);
        for (char c : text.toCharArray()) {
            String key = String.valueOf(c);
            if (c == 'l') {
                if (cnt[map.get("l1")] < cnt[map.get("l2")]) {
                    cnt[2]++;
                } else {
                    cnt[3]++;
                }
            } else if (c == 'o') {
                if (cnt[map.get("o1")] < cnt[map.get("o2")]) {
                    cnt[4]++;
                } else {
                    cnt[5]++;
                }
            } else {
                if (map.get(key) != null) {
                    cnt[map.get(key)]++;
                }
            }
        }
        int min = cnt[0];
        for (int i = 1; i < cnt.length; i++) {
            if (min > cnt[i]) min = cnt[i];
        }
        return min;
    }
}
