package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/remove-linked-list-elements/
 * <p>
 * Given the head of a linked list and an integer val, remove all the nodes of the linked list that has Node.val == val, and return the new head.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,2,6,3,4,5,6], val = 6
 * Output: [1,2,3,4,5]
 * Example 2:
 * <p>
 * Input: head = [], val = 1
 * Output: []
 * Example 3:
 * <p>
 * Input: head = [7,7,7,7], val = 7
 * Output: []
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is in the range [0, 10^4].
 * 1 <= Node.val <= 50
 * 0 <= val <= 50
 */
public class _203_RemoveLinkedListElements {

    public static ListNode removeElements(ListNode head, int val) {
        if (head == null) {
            return head;
        }
        ListNode curr = head;
        ListNode prev = null;
        do {
            if (curr.val == val) {
                if (prev == null) {
                    head = curr.next;
                    curr = curr.next;
                } else {
                    prev.next = curr.next;
                    curr = curr.next;
                }
            } else {
                prev = curr;
                curr = curr.next;
            }
        } while (curr != null);
        return head;
    }

    public static void main(String[] args) {
        System.out.println(removeElements(new ListNode(1, new ListNode(2, new ListNode(6, new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(6))))))), 6));
        System.out.println(removeElements(new ListNode(), 1));
        System.out.println(removeElements(new ListNode(7, new ListNode(7, new ListNode(7, new ListNode(7)))), 7));
        System.out.println(removeElements(new ListNode(7, new ListNode(7, new ListNode(7, new ListNode(7, new ListNode(1))))), 7));
        System.out.println(removeElements(new ListNode(1, new ListNode(2, new ListNode(2, new ListNode(1)))), 2));
    }
}
