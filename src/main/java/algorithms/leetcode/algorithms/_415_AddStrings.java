package algorithms.leetcode.algorithms;

public class _415_AddStrings {

    public static void main(String[] args) {
        assert "444".equals(addStrings("123", "321"));
        assert "198".equals(addStrings("99", "99"));
        assert "507".equals(addStrings("6", "501"));
        assert "10".equals(addStrings("1", "9"));
    }

    private static String addStrings(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        char[] arrayA = num1.toCharArray();
        char[] arrayB = num2.toCharArray();
        int i = arrayA.length - 1, j = arrayB.length - 1, buffer = 0;
        while (i >= 0 || j >= 0) {
            if (i >= 0 && j >= 0) {
                int tt = addStringParse(arrayA[i]) + addStringParse(arrayB[j]) + buffer;
                sb.append(tt % 10);
                buffer = tt / 10;
                i--;
                j--;
            } else if (i >= 0) {
                int tt = addStringParse(arrayA[i]) + buffer;
                sb.append(tt % 10);
                buffer = tt / 10;
                i--;
            } else {
                int tt = addStringParse(arrayB[j]) + buffer;
                sb.append(tt % 10);
                buffer = tt / 10;
                j--;
            }
        }
        if (buffer != 0) sb.append(buffer);

        return sb.reverse().toString();
    }

    private static int addStringParse(char c) {
        return Integer.parseInt(String.valueOf(c));
    }


}
