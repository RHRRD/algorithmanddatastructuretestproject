package algorithms.leetcode.algorithms;

public class _67_AddBinary {

    public static void main(String[] args) {
        assert "110".equals(addBinary("11", "11"));
        assert "100".equals(addBinary("11", "1"));
        assert "10101".equals(addBinary("1010", "1011"));
    }

    private static String addBinary(String a, String b) {
//        return Integer.toBinaryString(Integer.parseInt(a, 2) + Integer.parseInt(b, 2));
        StringBuilder sb = new StringBuilder();
        char[] arrayA = a.toCharArray();
        char[] arrayB = b.toCharArray();
        int i = arrayA.length - 1, j = arrayB.length - 1, buffer = 0;
        while (i >= 0 || j >= 0) {
            if (i >= 0 && j >= 0) {
                int tt = addBinaryParse(arrayA[i]) + addBinaryParse(arrayB[j]) + buffer;
                sb.append(tt % 2);
                buffer = tt > 1 ? 1 : 0;
                i--;
                j--;
            } else if (i >= 0) {
                int tt = addBinaryParse(arrayA[i]) + buffer;
                sb.append(tt % 2);
                buffer = tt > 1 ? 1 : 0;
                i--;
            } else {
                int tt = addBinaryParse(arrayB[j]) + buffer;
                sb.append(tt % 2);
                buffer = tt > 1 ? 1 : 0;
                j--;
            }
        }
        if (buffer != 0) sb.append(1);

        return sb.reverse().toString();
    }

    private static int addBinaryParse(char c) {
        return c == '1' ? 1 : 0;
    }

}
