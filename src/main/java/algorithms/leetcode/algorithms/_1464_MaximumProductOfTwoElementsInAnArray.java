package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/maximum-product-of-two-elements-in-an-array/
 * <p>
 * Given the array of integers nums, you will choose two different indices i and j of that array. Return the maximum value of (nums[i]-1)*(nums[j]-1).
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [3,4,5,2]
 * Output: 12
 * Explanation: If you choose the indices i=1 and j=2 (indexed from 0), you will get the maximum value, that is, (nums[1]-1)*(nums[2]-1) = (4-1)*(5-1) = 3*4 = 12.
 * Example 2:
 * <p>
 * Input: nums = [1,5,4,5]
 * Output: 16
 * Explanation: Choosing the indices i=1 and j=3 (indexed from 0), you will get the maximum value of (5-1)*(5-1) = 16.
 * Example 3:
 * <p>
 * Input: nums = [3,7]
 * Output: 12
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 2 <= nums.length <= 500
 * 1 <= nums[i] <= 10^3
 */
public class _1464_MaximumProductOfTwoElementsInAnArray {

    public static int maxProduct(int[] nums) {
        Arrays.sort(nums);
        return (nums[nums.length - 1] - 1) * (nums[nums.length - 2] - 1);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(12, maxProduct(new int[]{3, 4, 5, 2}));
        CheckAnswerUtil.check(16, maxProduct(new int[]{1, 5, 4, 5}));
        CheckAnswerUtil.check(12, maxProduct(new int[]{3, 7}));
        CheckAnswerUtil.check(0, maxProduct(new int[]{1, 1}));
        CheckAnswerUtil.check(0, maxProduct(new int[]{0, 1, 2}));
    }
}
