package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/find-greatest-common-divisor-of-array/
 * <p>
 * Given an integer array nums, return the greatest common divisor of the smallest number and largest number in nums.
 * <p>
 * The greatest common divisor of two numbers is the largest positive integer that evenly divides both numbers.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [2,5,6,9,10]
 * Output: 2
 * Explanation:
 * The smallest number in nums is 2.
 * The largest number in nums is 10.
 * The greatest common divisor of 2 and 10 is 2.
 * Example 2:
 * <p>
 * Input: nums = [7,5,6,8,3]
 * Output: 1
 * Explanation:
 * The smallest number in nums is 3.
 * The largest number in nums is 8.
 * The greatest common divisor of 3 and 8 is 1.
 * Example 3:
 * <p>
 * Input: nums = [3,3]
 * Output: 3
 * Explanation:
 * The smallest number in nums is 3.
 * The largest number in nums is 3.
 * The greatest common divisor of 3 and 3 is 3.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 2 <= nums.length <= 1000
 * 1 <= nums[i] <= 1000
 */
public class _1979_FindGreatestCommonDivisorOfArray {

    public static int findGCD(int[] nums) {
        Arrays.sort(nums);
        int a = nums[0], b = nums[nums.length - 1], res = 1;
        for (int i = Math.min(a, b); i > 1; i--) {
            if (a % i == 0 && b % i == 0) {
                return i;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, findGCD(new int[]{2, 5, 6, 9, 10}));
        CheckAnswerUtil.check(1, findGCD(new int[]{7, 5, 6, 8, 3}));
        CheckAnswerUtil.check(3, findGCD(new int[]{3, 3}));
    }
}
