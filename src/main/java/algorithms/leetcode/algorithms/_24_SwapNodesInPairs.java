package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/swap-nodes-in-pairs/
 * <p>
 * Given a linked list, swap every two adjacent nodes and return its head. You must solve the problem without modifying the values in the list's nodes (i.e., only nodes themselves may be changed.)
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,2,3,4]
 * Output: [2,1,4,3]
 * Example 2:
 * <p>
 * Input: head = []
 * Output: []
 * Example 3:
 * <p>
 * Input: head = [1]
 * Output: [1]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is in the range [0, 100].
 * 0 <= Node.val <= 100
 */
public class _24_SwapNodesInPairs {

    public static ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode prev = null;
        ListNode current = head;
        ListNode next = head.next;
        ListNode answer = next;
        do {
            current.next = next.next;
            next.next = current;
            if (prev != null) {
                prev.next = next;
            }
            prev = current;
            current = current.next;
            next = current == null ? null : current.next;
        } while (current != null && next != null);
        return answer;
    }

    public static void main(String[] args) {
        System.out.println(swapPairs(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4))))));
        System.out.println();

        System.out.println(swapPairs(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))))));
        System.out.println();

        System.out.println(swapPairs(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(6))))))));
        System.out.println();

        System.out.println(swapPairs(new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5, new ListNode(6, new ListNode(7, new ListNode(8, new ListNode(9, new ListNode(10))))))))))));
        System.out.println();

        System.out.println(swapPairs(new ListNode(1)));
        System.out.println();

        System.out.println(swapPairs(new ListNode(1, new ListNode(2))));
        System.out.println();

        System.out.println(swapPairs(new ListNode()));
        System.out.println();
    }
}
