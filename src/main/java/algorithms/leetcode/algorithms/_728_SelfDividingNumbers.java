package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.List;

public class _728_SelfDividingNumbers {

    public List<Integer> selfDividingNumbers(int left, int right) {
        List<Integer> list = new ArrayList<>();
        for (int i = left; i <= right; i++) {
            if (isDivide(i)) {
                list.add(i);
            }
        }
        return list;
    }

    private boolean isDivide(int i) {
        for (char c : String.valueOf(i).toCharArray()) {
            if (c == '0' || i % Integer.parseInt(String.valueOf(c)) != 0) {
                return false;
            }
        }
        return true;
    }
}
