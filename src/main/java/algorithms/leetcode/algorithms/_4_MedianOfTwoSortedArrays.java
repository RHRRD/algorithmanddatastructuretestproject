package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/median-of-two-sorted-arrays/
 * <p>
 * Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
 * <p>
 * The overall run time complexity should be O(log (m+n)).
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums1 = [1,3], nums2 = [2]
 * Output: 2.00000
 * Explanation: merged array = [1,2,3] and median is 2.
 * Example 2:
 * <p>
 * Input: nums1 = [1,2], nums2 = [3,4]
 * Output: 2.50000
 * Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * nums1.length == m
 * nums2.length == n
 * 0 <= m <= 1000
 * 0 <= n <= 1000
 * 1 <= m + n <= 2000
 * -10^6 <= nums1[i], nums2[i] <= 10^6
 */
public class _4_MedianOfTwoSortedArrays {

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int length = nums1.length + nums2.length;
        double res = 0.0;
        if (length == 0) {
            return res;
        }
        boolean isEven = length % 2 == 0;
        int i1 = 0, i2 = 0;
        if (isEven) {
            int medIndex2 = length / 2 + 1;
            int prev = 0, curr = 0;
            for (int i = 0; i < medIndex2; i++) {
                if (i1 >= nums1.length) {
                    curr = nums2[i2++];
                } else if (i2 >= nums2.length) {
                    curr = nums1[i1++];
                } else if (nums1[i1] > nums2[i2]) {
                    curr = nums2[i2++];
                } else {
                    curr = nums1[i1++];
                }
                prev = i < medIndex2 - 1 ? curr : prev;
            }
            res = (double) (prev + curr) / 2;
        } else {
            int medIndex = length / 2 + 1;
            int curr = 0;
            for (int i = 0; i < medIndex; i++) {
                if (i1 >= nums1.length) {
                    curr = nums2[i2++];
                } else if (i2 >= nums2.length) {
                    curr = nums1[i1++];
                } else if (nums1[i1] > nums2[i2]) {
                    curr = nums2[i2++];
                } else {
                    curr = nums1[i1++];
                }
            }
            res = curr;
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(0.0, findMedianSortedArrays(new int[]{}, new int[]{}));
        CheckAnswerUtil.check(0.0, findMedianSortedArrays(new int[]{0}, new int[]{0}));
        CheckAnswerUtil.check(1.0, findMedianSortedArrays(new int[]{1}, new int[]{}));
        CheckAnswerUtil.check(1.0, findMedianSortedArrays(new int[]{}, new int[]{1}));
        CheckAnswerUtil.check(0.5, findMedianSortedArrays(new int[]{0}, new int[]{1}));
        CheckAnswerUtil.check(1.0, findMedianSortedArrays(new int[]{1}, new int[]{1}));
        CheckAnswerUtil.check(2.0, findMedianSortedArrays(new int[]{1, 3}, new int[]{2}));
        CheckAnswerUtil.check(2.5, findMedianSortedArrays(new int[]{1, 2}, new int[]{3, 4}));
    }
}
