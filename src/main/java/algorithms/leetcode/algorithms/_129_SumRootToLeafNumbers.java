package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/sum-root-to-leaf-numbers/
 * <p>
 * You are given the root of a binary tree containing digits from 0 to 9 only.
 * <p>
 * Each root-to-leaf path in the tree represents a number.
 * <p>
 * For example, the root-to-leaf path 1 -> 2 -> 3 represents the number 123.
 * Return the total sum of all root-to-leaf numbers. Test cases are generated so that the answer will fit in a 32-bit integer.
 * <p>
 * A leaf node is a node with no children.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [1,2,3]
 * Output: 25
 * Explanation:
 * The root-to-leaf path 1->2 represents the number 12.
 * The root-to-leaf path 1->3 represents the number 13.
 * Therefore, sum = 12 + 13 = 25.
 * Example 2:
 * <p>
 * <p>
 * Input: root = [4,9,0,5,1]
 * Output: 1026
 * Explanation:
 * The root-to-leaf path 4->9->5 represents the number 495.
 * The root-to-leaf path 4->9->1 represents the number 491.
 * The root-to-leaf path 4->0 represents the number 40.
 * Therefore, sum = 495 + 491 + 40 = 1026.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 1000].
 * 0 <= Node.val <= 9
 * The depth of the tree will not exceed 10.
 */
public class _129_SumRootToLeafNumbers {

    public static int sumNumbers(TreeNode root) {
        if (root == null) {
            return 0;
        }
        return sumNumbers(root, 0);
    }

    public static int sumNumbers(TreeNode node, int sum) {
        if (node == null) {
            return 0;
        }
        if (node.left == null && node.right == null) {
            return sum + node.val;
        }
        int newSum = 10 * (sum + node.val);
        return sumNumbers(node.left, newSum) + sumNumbers(node.right, newSum);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(25, sumNumbers(new TreeNode(1, new TreeNode(2), new TreeNode(3))));

        CheckAnswerUtil.check(1026, sumNumbers(new TreeNode(4, new TreeNode(9, new TreeNode(5), new TreeNode(1)), new TreeNode(0))));

        CheckAnswerUtil.check(111, sumNumbers(new TreeNode(1, new TreeNode(0, null, new TreeNode(1)), new TreeNode(0))));

        CheckAnswerUtil.check(10, sumNumbers(new TreeNode(10)));
    }
}
