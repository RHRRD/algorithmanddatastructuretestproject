package algorithms.leetcode.algorithms;

import java.util.Arrays;

public class _561_ArrayPartitionI {

    public static void main(String[] args) {

    }

    public static int arrayPairSum(int[] nums) {
        int sum = 0;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length; i += 2) {
            sum += nums[i];
        }
        return sum;
    }
}
