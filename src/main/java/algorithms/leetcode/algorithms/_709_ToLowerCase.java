package algorithms.leetcode.algorithms;

public class _709_ToLowerCase {

    public String toLowerCase(String str) {
        return str.toLowerCase();
    }

}
