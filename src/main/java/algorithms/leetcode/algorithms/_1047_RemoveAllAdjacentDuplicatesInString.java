package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/remove-all-adjacent-duplicates-in-string/
 * <p>
 * You are given a string s consisting of lowercase English letters. A duplicate removal consists of choosing two adjacent and equal letters and removing them.
 * <p>
 * We repeatedly make duplicate removals on s until we no longer can.
 * <p>
 * Return the final string after all such duplicate removals have been made. It can be proven that the answer is unique.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "abbaca"
 * Output: "ca"
 * Explanation:
 * For example, in "abbaca" we could remove "bb" since the letters are adjacent and equal, and this is the only possible move.  The result of this move is that the string is "aaca", of which only "aa" is possible, so the final string is "ca".
 * Example 2:
 * <p>
 * Input: s = "azxxzy"
 * Output: "ay"
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 10^5
 * s consists of lowercase English letters.
 */
public class _1047_RemoveAllAdjacentDuplicatesInString {

    public static String removeDuplicates(String s) {
        if (s.length() == 1) {
            return s;
        }
        StringBuilder tmp = new StringBuilder();
        for (char c : s.toCharArray()) {
            int size = tmp.length();
            if (size > 0 && tmp.charAt(size - 1) == c) {
                tmp.deleteCharAt(size - 1);
            } else {
                tmp.append(c);
            }
        }
        return tmp.toString();
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check("ca", removeDuplicates("abbaca"), "abbaca");
        CheckAnswerUtil.check("ay", removeDuplicates("azxxzy"), "azxxzy");
        CheckAnswerUtil.check("a", removeDuplicates("a"), "a");
        CheckAnswerUtil.check("", removeDuplicates("aa"), "aa");
    }
}
