package algorithms.leetcode.algorithms;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/sort-an-array/
 * <p>
 * Given an array of integers nums, sort the array in ascending order and return it.
 * <p>
 * You must solve the problem without using any built-in functions in O(nlog(n)) time complexity and with the smallest space complexity possible.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [5,2,3,1]
 * Output: [1,2,3,5]
 * Explanation: After sorting the array, the positions of some numbers are not changed (for example, 2 and 3), while the positions of other numbers are changed (for example, 1 and 5).
 * Example 2:
 * <p>
 * Input: nums = [5,1,1,2,0,0]
 * Output: [0,0,1,1,2,5]
 * Explanation: Note that the values of nums are not necessairly unique.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 5 * 104
 * -5 * 104 <= nums[i] <= 5 * 104
 */
public class _912_SortAnArray {

    public static int[] sortArray(int[] nums) {
        fastSort(nums, 0, nums.length - 1);
        return nums;
    }

    private static void fastSort(int[] array, int left, int right) {
        if (left >= right) {
            return;
        }
        int pivot = array[right];
        int partition = partition(array, left, right, pivot);
        fastSort(array, left, partition - 1);
        fastSort(array, partition + 1, right);
    }

    private static int partition(int[] array, int left, int right, int pivot) {
        int leftPtr = left - 1;
        int rightPtr = right;
        while (true) {
            while (array[++leftPtr] < pivot);
            while (rightPtr > 0 && array[--rightPtr] > pivot);

            if (leftPtr >= rightPtr) {
                break;
            }
            swap(array, leftPtr, rightPtr);
        }
        swap(array, leftPtr, right);
        return leftPtr;
    }

    private static void swap(int[] array, int leftPtr, int rightPtr) {
        int tmp = array[leftPtr];
        array[leftPtr] = array[rightPtr];
        array[rightPtr] = tmp;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new int[]{5, 2, 3, 1}));
        System.out.println(Arrays.toString(sortArray(new int[]{5, 2, 3, 1})));
        System.out.println();

        System.out.println(Arrays.toString(new int[]{5,1,1,2,0,0}));
        System.out.println(Arrays.toString(sortArray(new int[]{5,1,1,2,0,0})));
    }

}
