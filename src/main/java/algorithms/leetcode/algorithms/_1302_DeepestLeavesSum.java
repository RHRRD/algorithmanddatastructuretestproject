package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/deepest-leaves-sum/
 * <p>
 * Given the root of a binary tree, return the sum of values of its deepest leaves.
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [1,2,3,4,5,null,6,7,null,null,null,null,8]
 * Output: 15
 * Example 2:
 * <p>
 * Input: root = [6,7,8,2,7,1,3,9,null,1,4,null,null,null,5]
 * Output: 19
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 104].
 * 1 <= Node.val <= 100
 */
public class _1302_DeepestLeavesSum {

    public static int deepestLeavesSum(TreeNode root) {
        int deep = findDeep(root);
        return deepestLeavesSum(root, deep, 1);
    }

    private static int deepestLeavesSum(TreeNode root, int deep, int curDeep) {
        if (root == null) {
            return 0;
        }
        if (curDeep == deep) {
            return root.val;
        }
        return deepestLeavesSum(root.left, deep, curDeep + 1) + deepestLeavesSum(root.right, deep, curDeep + 1);
    }

    private static int findDeep(TreeNode root) {
        if (root == null) {
            return 0;
        }
        if (root.left == null && root.right == null) {
            return 1;
        }
        return 1 + Math.max(findDeep(root.left), findDeep(root.right));
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(1, findDeep(new TreeNode(1)));
        CheckAnswerUtil.check(2, findDeep(new TreeNode(1, new TreeNode(2), new TreeNode(3))));
        CheckAnswerUtil.check(2, findDeep(new TreeNode(1, new TreeNode(2), null)));
        CheckAnswerUtil.check(2, findDeep(new TreeNode(1, null, new TreeNode(3))));
        CheckAnswerUtil.check(3, findDeep(new TreeNode(1, new TreeNode(2, null, new TreeNode(4)), new TreeNode(3))));
        CheckAnswerUtil.check(4, findDeep(new TreeNode(1, new TreeNode(2, new TreeNode(4, new TreeNode(7), null), new TreeNode(5)), new TreeNode(3, null, new TreeNode(6, null, new TreeNode(8))))));
        CheckAnswerUtil.check(4, findDeep(new TreeNode(6, new TreeNode(7, new TreeNode(2, new TreeNode(9), null), new TreeNode(7, new TreeNode(1), new TreeNode(4))), new TreeNode(8, new TreeNode(1), new TreeNode(3, null, new TreeNode(5))))));
        CheckAnswerUtil.check(15, deepestLeavesSum(new TreeNode(1, new TreeNode(2, new TreeNode(4, new TreeNode(7), null), new TreeNode(5)), new TreeNode(3, null, new TreeNode(6, null, new TreeNode(8))))));
        CheckAnswerUtil.check(19, deepestLeavesSum(new TreeNode(6, new TreeNode(7, new TreeNode(2, new TreeNode(9), null), new TreeNode(7, new TreeNode(1), new TreeNode(4))), new TreeNode(8, new TreeNode(1), new TreeNode(3, null, new TreeNode(5))))));
    }
}
