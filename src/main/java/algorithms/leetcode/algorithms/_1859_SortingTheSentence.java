package algorithms.leetcode.algorithms;

/**
 * https://leetcode.com/problems/sorting-the-sentence/
 * <p>
 * A sentence is a list of words that are separated by a single space with no leading or trailing spaces. Each word consists of lowercase and uppercase English letters.
 * <p>
 * A sentence can be shuffled by appending the 1-indexed word position to each word then rearranging the words in the sentence.
 * <p>
 * For example, the sentence "This is a sentence" can be shuffled as "sentence4 a3 is2 This1" or "is2 sentence4 This1 a3".
 * Given a shuffled sentence s containing no more than 9 words, reconstruct and return the original sentence.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "is2 sentence4 This1 a3"
 * Output: "This is a sentence"
 * Explanation: Sort the words in s to their original positions "This1 is2 a3 sentence4", then remove the numbers.
 * Example 2:
 * <p>
 * Input: s = "Myself2 Me1 I4 and3"
 * Output: "Me Myself and I"
 * Explanation: Sort the words in s to their original positions "Me1 Myself2 and3 I4", then remove the numbers.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 2 <= s.length <= 200
 * s consists of lowercase and uppercase English letters, spaces, and digits from 1 to 9.
 * The number of words in s is between 1 and 9.
 * The words in s are separated by a single space.
 * s contains no leading or trailing spaces.
 */
public class _1859_SortingTheSentence {

    public static String sortSentence(String s) {
        String[] wordWithNumberArray = s.split(" ");
        String[] tmp = new String[wordWithNumberArray.length];
        for (int i = 0; i < wordWithNumberArray.length; i++) {
            String[] split = wordWithNumberArray[i].split("(?<=\\D)(?=\\d)");
            tmp[Integer.parseInt(split[1]) - 1] = split[0];
        }
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < tmp.length; i++) {
            res.append(tmp[i]);
            if (i < tmp.length - 1) {
                res.append(" ");
            }
        }
        return res.toString();
    }

    public static void main(String[] args) {
        System.out.println(sortSentence("is2 sentence4 This1 a3"));
        System.out.println("This is a sentence");
        System.out.println();

        System.out.println(sortSentence("Myself2 Me1 I4 and3"));
        System.out.println("Me Myself and I");
    }
}
