package algorithms.leetcode.algorithms.common;

public class ListNode {

    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append(val);
        ListNode nx = next;
        while (nx != null) {
            res.append(" -> ");
            res.append(nx.val);
            nx = nx.next;
        }
        return res.toString();
    }
}
