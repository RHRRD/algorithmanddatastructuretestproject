package algorithms.leetcode.algorithms;

public class _509_FibonacciNumber {

    public static void main(String[] args) {
        assert 0 == fib(0);
        assert 1 == fib(1);
        assert 1 == fib(2);
        assert 2 == fib(3);
        assert 3 == fib(4);
        assert 832040 == fib(30);
        for (int i = 0; i < 31; i++) {
            assert fib(i) == fib2(i);
        }
    }

    private static int fib(int N) {
        if (N == 0) return 0;
        if (N == 1 || N == 2) return 1;
        return fib(N - 1) + fib(N - 2);
    }

    private static int fib2(int N) {
        if (N == 0) return 0;
        if (N == 1 || N == 2) return 1;

        int prev1 = 1, prev2 = 1, current = 0;
        for (int i = 3; i <= N; i++) {
            current = prev1 + prev2;
            prev2 = prev1;
            prev1 = current;
        }
        return current;
    }


}
