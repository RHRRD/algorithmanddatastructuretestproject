package algorithms.leetcode.algorithms;

public class _7_ReverseInteger {

    public static void main(String[] args) {
        assert 321 == reverse(123);
        assert -321 == reverse(-123);
        assert 21 == reverse(120);
        assert 1 == reverse(100);
        assert 0 == reverse(1534236469);
    }

    private static int reverse(int x) {
        String str = String.valueOf(x);
        StringBuilder res;
        boolean isNegative = false;
        if (str.charAt(0) == '-') {
            isNegative = true;
            res = new StringBuilder(str.substring(1)).reverse();
        } else {
            res = new StringBuilder(str).reverse();
        }

        String s = isNegative ? "-" + res.toString() : res.toString();
        try {
            return Integer.parseInt(s);
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

}
