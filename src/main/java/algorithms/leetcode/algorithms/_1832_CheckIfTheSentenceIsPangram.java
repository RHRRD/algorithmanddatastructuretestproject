package algorithms.leetcode.algorithms;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/check-if-the-sentence-is-pangram/
 * <p>
 * A pangram is a sentence where every letter of the English alphabet appears at least once.
 * <p>
 * Given a string sentence containing only lowercase English letters, return true if sentence is a pangram, or false otherwise.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: sentence = "thequickbrownfoxjumpsoverthelazydog"
 * Output: true
 * Explanation: sentence contains at least one of every letter of the English alphabet.
 * Example 2:
 * <p>
 * Input: sentence = "leetcode"
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= sentence.length <= 1000
 * sentence consists of lowercase English letters.
 */
public class _1832_CheckIfTheSentenceIsPangram {

    public static boolean checkIfPangram(String sentence) {
        int[] res = new int[26];
        Arrays.fill(res, 1);
        for (char c : sentence.toCharArray()) {
            res[c - 'a'] = 0;
        }
        for (int i = 0; i < res.length; i++) {
            if (res[i] == 1) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(checkIfPangram("thequickbrownfoxjumpsoverthelazydog"));
        System.out.println(true);
        System.out.println();

        System.out.println(checkIfPangram("leetcode"));
        System.out.println(false);
    }
}
