package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;
import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/sum-of-left-leaves/description/
 * <p>
 * Given the root of a binary tree, return the sum of all left leaves.
 * <p>
 * A leaf is a node with no children. A left leaf is a leaf that is the left child of another node.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [3,9,20,null,null,15,7]
 * Output: 24
 * Explanation: There are two left leaves in the binary tree, with values 9 and 15 respectively.
 * Example 2:
 * <p>
 * Input: root = [1]
 * Output: 0
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 1000].
 * -1000 <= Node.val <= 1000
 */
public class _404_SumOfLeftLeaves {

    public static int sumOfLeftLeaves(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) {
            return 0;
        }
        return sumOfLeftLeaves(root.left, true, 0) + sumOfLeftLeaves(root.right, false, 0);
    }

    private static int sumOfLeftLeaves(TreeNode node, boolean isLeft, int sum) {
        if (node == null) {
            return sum;
        }
        if ((node.left == null && node.right == null)) {
            return isLeft ? sum + node.val : sum;
        }
        return sumOfLeftLeaves(node.left, true, sum) + sumOfLeftLeaves(node.right, false, sum);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(24, sumOfLeftLeaves(new TreeNode(3, new TreeNode(9), new TreeNode(20, new TreeNode(15), new TreeNode(7)))));
        CheckAnswerUtil.check(0, sumOfLeftLeaves(new TreeNode(1)));
        CheckAnswerUtil.check(0, sumOfLeftLeaves(new TreeNode(1, null, new TreeNode(3))));
        CheckAnswerUtil.check(2, sumOfLeftLeaves(new TreeNode(1, new TreeNode(2), null)));
        CheckAnswerUtil.check(2, sumOfLeftLeaves(new TreeNode(1, new TreeNode(2), new TreeNode(3))));
    }
}
