package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/can-place-flowers/
 * <p>
 * You have a long flowerbed in which some of the plots are planted, and some are not. However, flowers cannot be planted in adjacent plots.
 * <p>
 * Given an integer array flowerbed containing 0's and 1's, where 0 means empty and 1 means not empty, and an integer n, return if n new flowers can be planted in the flowerbed without violating the no-adjacent-flowers rule.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: flowerbed = [1,0,0,0,1], n = 1
 * Output: true
 * Example 2:
 * <p>
 * Input: flowerbed = [1,0,0,0,1], n = 2
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= flowerbed.length <= 2 * 10^4
 * flowerbed[i] is 0 or 1.
 * There are no two adjacent flowers in flowerbed.
 * 0 <= n <= flowerbed.length
 */
public class _605_CanPlaceFlower {

    public static boolean canPlaceFlowers(int[] flowerbed, int n) {
        int prev = 0, next;
        for (int i = 0; i < flowerbed.length; i++) {
            next = i < flowerbed.length - 1 ? flowerbed[i + 1] : 0;
            if (prev == 0 && flowerbed[i] == 0 && next == 0) {
                n--;
                prev = 1;
            } else {
                prev = flowerbed[i];
            }
        }
        return n <= 0;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 1));

        CheckAnswerUtil.check(false, canPlaceFlowers(new int[]{1, 0, 0, 0, 1}, 2));

        CheckAnswerUtil.check(true, canPlaceFlowers(new int[]{0, 0, 0, 0, 1}, 2));

        CheckAnswerUtil.check(true, canPlaceFlowers(new int[]{1, 0, 0, 0, 0}, 2));

        CheckAnswerUtil.check(false, canPlaceFlowers(new int[]{0, 0, 0, 0, 1}, 3));

        CheckAnswerUtil.check(false, canPlaceFlowers(new int[]{1, 0, 0, 0, 0}, 3));

        CheckAnswerUtil.check(true, canPlaceFlowers(new int[]{1, 0, 0, 0, 0, 0, 0}, 3));

        CheckAnswerUtil.check(true, canPlaceFlowers(new int[]{0, 0, 0, 0, 0, 0, 1}, 3));

        CheckAnswerUtil.check(false, canPlaceFlowers(new int[]{1, 0, 0, 0, 0, 1}, 2));

        CheckAnswerUtil.check(true, canPlaceFlowers(new int[]{0, 0, 1, 0, 0}, 1));
    }
}
