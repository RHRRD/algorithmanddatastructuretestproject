package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

/**
 * https://leetcode.com/problems/increasing-order-search-tree/
 * <p>
 * Given the root of a binary search tree, rearrange the tree in in-order so that the leftmost node in the tree is now the root of the tree, and every node has no left child and only one right child.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [5,3,6,2,4,null,8,1,null,null,null,7,9]
 * Output: [1,null,2,null,3,null,4,null,5,null,6,null,7,null,8,null,9]
 * Example 2:
 * <p>
 * <p>
 * Input: root = [5,1,7]
 * Output: [1,null,5,null,7]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the given tree will be in the range [1, 100].
 * 0 <= Node.val <= 1000
 */
public class _897_IncreasingOrderSearchTree {

    public static TreeNode increasingBST(TreeNode root) {
        if (root == null) {
            return null;
        }
        if (root.left == null && root.right == null) {
            return new TreeNode(root.val);
        }
        TreeNode left = increasingBST(root.left);
        TreeNode right = increasingBST(root.right);
        if (left == null) {
            return new TreeNode(root.val, null, right);
        }
        TreeNode tmp = left;
        while (tmp.right != null) {
            tmp = tmp.right;
        }
        tmp.right = new TreeNode(root.val, null, right);
        return left;
    }

    public static void main(String[] args) {
        System.out.println(increasingBST(new TreeNode(5, new TreeNode(3, new TreeNode(2, new TreeNode(1), null), new TreeNode(4)), new TreeNode(6, null, new TreeNode(8, new TreeNode(7), new TreeNode(9))))));
        System.out.println(increasingBST(new TreeNode(5, new TreeNode(1), new TreeNode(7))));
        System.out.println(increasingBST(new TreeNode(5, null, new TreeNode(7))));
        System.out.println(increasingBST(new TreeNode(5, new TreeNode(1), null)));
        System.out.println(increasingBST(new TreeNode(5)));
        System.out.println(increasingBST(null));
    }
}
