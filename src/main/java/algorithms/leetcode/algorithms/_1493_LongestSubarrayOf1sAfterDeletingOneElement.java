package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/longest-subarray-of-1s-after-deleting-one-element/
 * <p>
 * Given a binary array nums, you should delete one element from it.
 * <p>
 * Return the size of the longest non-empty subarray containing only 1's in the resulting array. Return 0 if there is no such subarray.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [1,1,0,1]
 * Output: 3
 * Explanation: After deleting the number in position 2, [1,1,1] contains 3 numbers with value of 1's.
 * Example 2:
 * <p>
 * Input: nums = [0,1,1,1,0,1,1,0,1]
 * Output: 5
 * Explanation: After deleting the number in position 4, [0,1,1,1,1,1,0,1] longest subarray with value of 1's is [1,1,1,1,1].
 * Example 3:
 * <p>
 * Input: nums = [1,1,1]
 * Output: 2
 * Explanation: You must delete one element.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 105
 * nums[i] is either 0 or 1.
 */
public class _1493_LongestSubarrayOf1sAfterDeletingOneElement {

    public static int longestSubarray(int[] nums) {
        int prevCnt = 0, cnt = 0, result = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                result = Math.max(result, prevCnt + cnt);
                prevCnt = cnt;
                cnt = 0;
            } else {
                cnt++;
            }
        }
        result = Math.max(result, prevCnt + cnt);
        return result == nums.length ? result - 1 : result;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(3, longestSubarray(new int[]{1, 1, 0, 1}));

        CheckAnswerUtil.check(5, longestSubarray(new int[]{0, 1, 1, 1, 0, 1, 1, 0,}));

        CheckAnswerUtil.check(2, longestSubarray(new int[]{1, 1, 1}));

        CheckAnswerUtil.check(0, longestSubarray(new int[]{1}));

        CheckAnswerUtil.check(0, longestSubarray(new int[]{0}));

        CheckAnswerUtil.check(0, longestSubarray(new int[]{0, 0}));
    }
}
