package algorithms.leetcode.algorithms;

public class _1266_MinimumTimeVisitingAllPoints {

    public static void main(String[] args) {
        System.out.println(minTimeToVisitAllPoints(new int[][]{{1, 1}, {3, 4}, {-1, 0}}));
        System.out.println(minTimeToVisitAllPoints(new int[][]{{3, 2}, {-2, 2}}));
        System.out.println(minTimeToVisitAllPoints(new int[][]{{1, 1}}));
        System.out.println(minTimeToVisitAllPoints(new int[][]{{1, 1}, {2, 2}}));
        System.out.println(minTimeToVisitAllPoints(new int[][]{{-1, -1}, {2, 2}}));
        System.out.println(minTimeToVisitAllPoints(new int[][]{{1, 1}, {-2, -2}}));
    }

    private static int minTimeToVisitAllPoints(int[][] points) {
        int sec = 0;
        for (int i = 0; i < points.length - 1; i++) {
            sec += Math.max(Math.abs(points[i][0] - points[i + 1][0]), Math.abs(points[i][1] - points[i + 1][1]));
        }
        return sec;
    }
}
