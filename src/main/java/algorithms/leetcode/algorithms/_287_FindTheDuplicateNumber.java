package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/find-the-duplicate-number/
 * <p>
 * Given an array of integers nums containing n + 1 integers where each integer is in the range [1, n] inclusive.
 * <p>
 * There is only one repeated number in nums, return this repeated number.
 * <p>
 * You must solve the problem without modifying the array nums and uses only constant extra space.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [1,3,4,2,2]
 * Output: 2
 * Example 2:
 * <p>
 * Input: nums = [3,1,3,4,2]
 * Output: 3
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= n <= 105
 * nums.length == n + 1
 * 1 <= nums[i] <= n
 * All the integers in nums appear only once except for precisely one integer which appears two or more times.
 * <p>
 * <p>
 * Follow up:
 * <p>
 * How can we prove that at least one duplicate number must exist in nums?
 * Can you solve the problem in linear runtime complexity?
 */
public class _287_FindTheDuplicateNumber {

    public static int findDuplicate(int[] nums) {
        if (nums.length > 1) {
            int slow = nums[0], fast = nums[nums[0]];
            while (slow != fast) {
                slow = nums[slow];
                fast = nums[nums[fast]];
            }
            fast = 0;
            while (slow != fast) {
                slow = nums[slow];
                fast = nums[fast];
            }
            return slow;
        }
        return -1;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, findDuplicate(new int[]{1, 3, 4, 2, 2}));

        CheckAnswerUtil.check(3, findDuplicate(new int[]{3, 1, 3, 4, 2}));

        CheckAnswerUtil.check(10, findDuplicate(new int[]{1, 2, 10, 3, 4, 5, 6, 7, 8, 9, 10}));

        CheckAnswerUtil.check(10, findDuplicate(new int[]{10, 2, 1, 3, 4, 5, 6, 7, 8, 9, 10}));

        CheckAnswerUtil.check(10, findDuplicate(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10}));

        CheckAnswerUtil.check(5, findDuplicate(new int[]{1, 2, 3, 4, 5, 5, 6, 7, 8, 9, 10}));

        CheckAnswerUtil.check(9, findDuplicate(new int[]{2, 5, 9, 6, 9, 3, 8, 9, 7, 1}));
    }
}
