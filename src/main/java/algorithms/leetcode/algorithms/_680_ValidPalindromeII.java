package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/valid-palindrome-ii/
 * <p>
 * Given a string s, return true if the s can be palindrome after deleting at most one character from it.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "aba"
 * Output: true
 * Example 2:
 * <p>
 * Input: s = "abca"
 * Output: true
 * Explanation: You could delete the character 'c'.
 * Example 3:
 * <p>
 * Input: s = "abc"
 * Output: false
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 10^5
 * s consists of lowercase English letters.
 */
public class _680_ValidPalindromeII {

    public static boolean validPalindrome(String s) {
        if (s.length() <= 2) {
            return true;
        }
        boolean isDelete = false;
        boolean f1 = true, f2 = true;
        for (int i = 0, j = s.length() - 1; i < s.length() / 2; i++, j--) {
            if (s.charAt(i) != s.charAt(j)) {
                if (isDelete) {
                    f1 = false;
                    break;
                }
                if (s.charAt(i + 1) == s.charAt(j)) {
                    i++;
                    isDelete = true;
                } else {
                    f1 = false;
                    break;
                }
            }
        }
        isDelete = false;
        for (int i = 0, j = s.length() - 1; i < s.length() / 2; i++, j--) {
            if (s.charAt(i) != s.charAt(j)) {
                if (isDelete) {
                    f2 = false;
                    break;
                }
                if (s.charAt(i) == s.charAt(j - 1)) {
                    j--;
                    isDelete = true;
                } else {
                    f2 = false;
                    break;
                }
            }
        }
        return f1 || f2;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, validPalindrome("aba"));
        CheckAnswerUtil.check(true, validPalindrome("abca"));
        CheckAnswerUtil.check(false, validPalindrome("abc"));
        CheckAnswerUtil.check(true, validPalindrome("abccba"));
        CheckAnswerUtil.check(true, validPalindrome("abcdcba"));
        CheckAnswerUtil.check(true, validPalindrome("abccdba"));
        CheckAnswerUtil.check(true, validPalindrome("abccbda"));
        CheckAnswerUtil.check(true, validPalindrome("abccbad"));
        CheckAnswerUtil.check(true, validPalindrome("abccba"));
        CheckAnswerUtil.check(true, validPalindrome("dabccba"));
        CheckAnswerUtil.check(true, validPalindrome("adbccba"));
        CheckAnswerUtil.check(true, validPalindrome("abdccba"));
        CheckAnswerUtil.check(false, validPalindrome("abecddebca"));
        CheckAnswerUtil.check(true, validPalindrome("aabccbcaa"));
        CheckAnswerUtil.check(true, validPalindrome("aguokepatgbnvfqmgmlcupuufxoohdfpgjdmysgvhmvffcnqxjjxqncffvmhvgsymdjgpfdhooxfuupuculmgmqfvnbgtapekouga"));
    }
}
