package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

/**
 * https://leetcode.com/problems/find-a-corresponding-node-of-a-binary-tree-in-a-clone-of-that-tree/
 * <p>
 * Given two binary trees original and cloned and given a reference to a node target in the original tree.
 * <p>
 * The cloned tree is a copy of the original tree.
 * <p>
 * Return a reference to the same node in the cloned tree.
 * <p>
 * Note that you are not allowed to change any of the two trees or the target node and the answer must be a reference to a node in the cloned tree.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: tree = [7,4,3,null,null,6,19], target = 3
 * Output: 3
 * Explanation: In all examples the original and cloned trees are shown. The target node is a green node from the original tree. The answer is the yellow node from the cloned tree.
 * Example 2:
 * <p>
 * <p>
 * Input: tree = [7], target =  7
 * Output: 7
 * Example 3:
 * <p>
 * <p>
 * Input: tree = [8,null,6,null,5,null,4,null,3,null,2,null,1], target = 4
 * Output: 4
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [1, 10^4].
 * The values of the nodes of the tree are unique.
 * target node is a node from the original tree and is not null.
 */
public class _1379_FindACorrespondingNodeOfABinaryTreeInACloneOfThatTree {

    public static final TreeNode getTargetCopy(final TreeNode original, final TreeNode cloned, final TreeNode target) {
        if (cloned == null) {
            return null;
        }
        if (cloned.val == target.val) {
            return cloned;
        }
        TreeNode left = getTargetCopy(original, cloned.left, target);
        return left != null ? left : getTargetCopy(original, cloned.right, target);
    }

    public static void main(String[] args) {
        TreeNode node = new TreeNode(3, new TreeNode(6), new TreeNode(19));
        TreeNode original = new TreeNode(7, new TreeNode(4), node);
        System.out.println(getTargetCopy(original, original, node));
        System.out.println(getTargetCopy(new TreeNode(7), new TreeNode(7), new TreeNode(7)));
        node = new TreeNode(4, null, new TreeNode(3, null, new TreeNode(2, null, new TreeNode(1))));
        original = new TreeNode(8, null, new TreeNode(6, null, new TreeNode(5, null, node)));
        System.out.println(getTargetCopy(original, original, node));

    }
}
