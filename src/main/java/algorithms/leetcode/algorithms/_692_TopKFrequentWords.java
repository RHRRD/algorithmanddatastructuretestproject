package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * https://leetcode.com/problems/top-k-frequent-words/
 * <p>
 * Given an array of strings words and an integer k, return the k most frequent strings.
 * <p>
 * Return the answer sorted by the frequency from highest to lowest. Sort the words with the same frequency by their lexicographical order.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: words = ["i","love","leetcode","i","love","coding"], k = 2
 * Output: ["i","love"]
 * Explanation: "i" and "love" are the two most frequent words.
 * Note that "i" comes before "love" due to a lower alphabetical order.
 * Example 2:
 * <p>
 * Input: words = ["the","day","is","sunny","the","the","the","sunny","is","is"], k = 4
 * Output: ["the","is","sunny","day"]
 * Explanation: "the", "is", "sunny" and "day" are the four most frequent words, with the number of occurrence being 4, 3, 2 and 1 respectively.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= words.length <= 500
 * 1 <= words[i].length <= 10
 * words[i] consists of lowercase English letters.
 * k is in the range [1, The number of unique words[i]]
 * <p>
 * <p>
 * Follow-up: Could you solve it in O(n log(k)) time and O(n) extra space?
 */
public class _692_TopKFrequentWords {

    public static List<String> topKFrequent(String[] words, int k) {
        Map<String, Integer> map = new HashMap<>();
        List<String> res = new ArrayList<>();
        for (String word : words) {
            map.put(word, map.getOrDefault(word, 0) + 1);
        }
        Node[] nodes = new Node[map.size()];
        int i = 0;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            nodes[i++] = new Node(entry.getValue(), entry.getKey());
        }
        Arrays.sort(nodes);
        for (int j = 0; j < k; j++) {
            res.add(nodes[j].word);
        }
        return res;
    }

    private static class Node implements Comparable {
        int count;
        String word;

        public Node(int count, String word) {
            this.count = count;
            this.word = word;
        }

        @Override
        public int compareTo(@NotNull Object other) {
            if (other instanceof Node) {
                int countCompare = -1 * Integer.compare(count, ((Node) other).count);
                int wordCompare = word.compareTo(((Node) other).word);
                return countCompare == 0 ? wordCompare : countCompare;
            } else {
                return 0;
            }
        }
    }

    public static void main(String[] args) {
        List<String> res = new ArrayList<>();
        res.add("i");
        res.add("love");
        CheckAnswerUtil.check(res, topKFrequent(new String[]{"i", "love", "leetcode", "i", "love", "coding"}, 2));
        res = new ArrayList<>();
        res.add("the");
        res.add("is");
        res.add("sunny");
        res.add("day");
        CheckAnswerUtil.check(res, topKFrequent(new String[]{"the", "day", "is", "sunny", "the", "the", "the", "sunny", "is", "is"}, 4));
    }
}
