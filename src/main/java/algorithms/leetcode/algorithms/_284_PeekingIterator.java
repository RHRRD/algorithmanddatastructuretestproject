package algorithms.leetcode.algorithms;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * https://leetcode.com/problems/peeking-iterator/
 * <p>
 * Design an iterator that supports the peek operation on an existing iterator in addition to the hasNext and the next operations.
 * <p>
 * Implement the PeekingIterator class:
 * <p>
 * PeekingIterator(Iterator<int> nums) Initializes the object with the given integer iterator iterator.
 * int next() Returns the next element in the array and moves the pointer to the next element.
 * boolean hasNext() Returns true if there are still elements in the array.
 * int peek() Returns the next element in the array without moving the pointer.
 * Note: Each language may have a different implementation of the constructor and Iterator, but they all support the int next() and boolean hasNext() functions.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input
 * ["PeekingIterator", "next", "peek", "next", "next", "hasNext"]
 * [[[1, 2, 3]], [], [], [], [], []]
 * Output
 * [null, 1, 2, 2, 3, false]
 * <p>
 * Explanation
 * PeekingIterator peekingIterator = new PeekingIterator([1, 2, 3]); // [1,2,3]
 * peekingIterator.next();    // return 1, the pointer moves to the next element [1,2,3].
 * peekingIterator.peek();    // return 2, the pointer does not move [1,2,3].
 * peekingIterator.next();    // return 2, the pointer moves to the next element [1,2,3]
 * peekingIterator.next();    // return 3, the pointer moves to the next element [1,2,3]
 * peekingIterator.hasNext(); // return False
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 1000
 * 1 <= nums[i] <= 1000
 * All the calls to next and peek are valid.
 * At most 1000 calls will be made to next, hasNext, and peek.
 * <p>
 * <p>
 * Follow up: How would you extend your design to be generic and work with all types, not just integer?
 */
public class _284_PeekingIterator implements Iterator<Integer> {

    private Iterator<Integer> iterator;
    private boolean flag;
    private Integer currentValue;

    public _284_PeekingIterator(Iterator<Integer> iterator) {
        this.iterator = iterator;
        updateCurrentValue();
    }

    private void updateCurrentValue() {
        if (iterator.hasNext()) {
            flag = true;
            currentValue = iterator.next();
        } else {
            flag = false;
            currentValue = null;
        }
    }

    // Returns the next element in the iteration without advancing the iterator.
    public Integer peek() {
        return currentValue;
    }

    // hasNext() and next() should behave the same as in the Iterator interface.
    // Override them if needed.
    @Override
    public Integer next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        Integer tmp = currentValue;
        updateCurrentValue();
        return tmp;
    }

    @Override
    public boolean hasNext() {
        return flag;
    }

    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3);
        _284_PeekingIterator iterator = new _284_PeekingIterator(list.listIterator());
        System.out.println(iterator.next());
        System.out.println(iterator.peek());
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.hasNext());
        System.out.println();

        List<Integer> list2 = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        _284_PeekingIterator iterator2 = new _284_PeekingIterator(list2.listIterator());
        System.out.println(iterator2.next());
        System.out.println(iterator2.peek());
        System.out.println(iterator2.next());
        System.out.println(iterator2.next());
        System.out.println(iterator2.hasNext());
    }
}
