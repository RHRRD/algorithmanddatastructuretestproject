package algorithms.leetcode.algorithms;

public class _1221_SplitAStringInBalancedStrings {

    public int balancedStringSplit(String s) {
        int count = 0;
        int tt = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == 'R') {
                tt++;
            } else if (s.charAt(i) == 'L') {
                tt--;
            }
            if (tt == 0) {
                count++;
            }
        }
        return count;
    }
}
