package algorithms.leetcode.algorithms

import java.util.*

fun main() {
    println(sortedSquares(intArrayOf(1, 4, 2, 3)).contentToString())
}

fun sortedSquares(A: IntArray): IntArray {
    return A.map { it * it }.sorted().toIntArray()
}
