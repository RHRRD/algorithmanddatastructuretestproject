package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/count-asterisks/
 * <p>
 * You are given a string s, where every two consecutive vertical bars '|' are grouped into a pair. In other words, the 1st and 2nd '|' make a pair, the 3rd and 4th '|' make a pair, and so forth.
 * <p>
 * Return the number of '*' in s, excluding the '*' between each pair of '|'.
 * <p>
 * Note that each '|' will belong to exactly one pair.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "l|*e*et|c**o|*de|"
 * Output: 2
 * Explanation: The considered characters are underlined: "l|*e*et|c**o|*de|".
 * The characters between the first and second '|' are excluded from the answer.
 * Also, the characters between the third and fourth '|' are excluded from the answer.
 * There are 2 asterisks considered. Therefore, we return 2.
 * Example 2:
 * <p>
 * Input: s = "iamprogrammer"
 * Output: 0
 * Explanation: In this example, there are no asterisks in s. Therefore, we return 0.
 * Example 3:
 * <p>
 * Input: s = "yo|uar|e**|b|e***au|tifu|l"
 * Output: 5
 * Explanation: The considered characters are underlined: "yo|uar|e**|b|e***au|tifu|l". There are 5 asterisks considered. Therefore, we return 5.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= s.length <= 1000
 * s consists of lowercase English letters, vertical bars '|', and asterisks '*'.
 * s contains an even number of vertical bars '|'.
 */
public class _2315_CountAsterisks {

    public static int countAsterisks(String s) {
        char[] charArray = s.toCharArray();
        int res = 0, pair = 0;
        for (int i = 0; i < charArray.length; i++) {
            if (charArray[i] == '|') {
                pair++;
            }
            if (pair % 2 == 0 && charArray[i] == '*') {
                res++;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, countAsterisks("l|*e*et|c**o|*de|"), "l|*e*et|c**o|*de|");
        CheckAnswerUtil.check(0, countAsterisks("iamprogrammer"), "iamprogrammer");
        CheckAnswerUtil.check(5, countAsterisks("yo|uar|e**|b|e***au|tifu|l"), "yo|uar|e**|b|e***au|tifu|l");
    }
}
