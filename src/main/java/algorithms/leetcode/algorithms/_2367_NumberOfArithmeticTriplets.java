package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/number-of-arithmetic-triplets/
 * <p>
 * You are given a 0-indexed, strictly increasing integer array nums and a positive integer diff. A triplet (i, j, k) is an arithmetic triplet if the following conditions are met:
 * <p>
 * i < j < k,
 * nums[j] - nums[i] == diff, and
 * nums[k] - nums[j] == diff.
 * Return the number of unique arithmetic triplets.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [0,1,4,6,7,10], diff = 3
 * Output: 2
 * Explanation:
 * (1, 2, 4) is an arithmetic triplet because both 7 - 4 == 3 and 4 - 1 == 3.
 * (2, 4, 5) is an arithmetic triplet because both 10 - 7 == 3 and 7 - 4 == 3.
 * Example 2:
 * <p>
 * Input: nums = [4,5,6,7,8,9], diff = 2
 * Output: 2
 * Explanation:
 * (0, 2, 4) is an arithmetic triplet because both 8 - 6 == 2 and 6 - 4 == 2.
 * (1, 3, 5) is an arithmetic triplet because both 9 - 7 == 2 and 7 - 5 == 2.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 3 <= nums.length <= 200
 * 0 <= nums[i] <= 200
 * 1 <= diff <= 50
 * nums is strictly increasing.
 */
public class _2367_NumberOfArithmeticTriplets {

    public static int arithmeticTriplets(int[] nums, int diff) {
        int res = 0;
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length && (nums[j] - nums[i] <= diff); j++) {
                for (int k = j + 1; k < nums.length && (nums[k] - nums[j] <= diff); k++) {
                    if (nums[k] - nums[j] == diff && nums[j] - nums[i] == diff) {
                        res++;
                    }
                }
            }
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, arithmeticTriplets(new int[]{0, 1, 4, 6, 7, 10}, 3));
        CheckAnswerUtil.check(2, arithmeticTriplets(new int[]{4, 5, 6, 7, 8, 9}, 2));
    }
}
