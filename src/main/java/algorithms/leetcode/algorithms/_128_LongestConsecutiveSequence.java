package algorithms.leetcode.algorithms;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode.com/problems/longest-consecutive-sequence/description/
 * Given an unsorted array of integers nums, return the length of the longest consecutive elements sequence.
 * <p>
 * You must write an algorithm that runs in O(n) time.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [100,4,200,1,3,2]
 * Output: 4
 * Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
 * Example 2:
 * <p>
 * Input: nums = [0,3,7,2,5,8,4,6,0,1]
 * Output: 9
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 0 <= nums.length <= 10^5
 * -10^9 <= nums[i] <= 10^9
 */
public class _128_LongestConsecutiveSequence {

    public static int longestConsecutive(int[] nums) {
        Set<Integer> set = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            set.add(nums[i]);
        }
        int result = 0;
        for (int i = 0; i < nums.length; i++) {
            int current = nums[i];
            if (!set.contains(current - 1)) {
                int tmp = 1;
                while (set.contains(current + 1)) {
                    current++;
                    tmp++;
                }
                result = Math.max(result, tmp);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("4: " + longestConsecutive(new int[]{100, 4, 200, 1, 3, 2}));
        System.out.println("5: " + longestConsecutive(new int[]{100, 4, 200, 1, 3, 2, 6, 6, 7, 8, 9, 10}));
        System.out.println("9: " + longestConsecutive(new int[]{0, 3, 7, 2, 5, 8, 4, 6, 0, 1}));
    }
}
