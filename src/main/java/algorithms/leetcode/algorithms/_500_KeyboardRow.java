package algorithms.leetcode.algorithms;

import java.util.*;

/**
 * https://leetcode.com/problems/keyboard-row/
 * <p>
 * Given an array of strings words, return the words that can be typed using letters of the alphabet on only one row of American keyboard like the image below.
 * <p>
 * In the American keyboard:
 * <p>
 * the first row consists of the characters "qwertyuiop",
 * the second row consists of the characters "asdfghjkl", and
 * the third row consists of the characters "zxcvbnm".
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: words = ["Hello","Alaska","Dad","Peace"]
 * Output: ["Alaska","Dad"]
 * Example 2:
 * <p>
 * Input: words = ["omk"]
 * Output: []
 * Example 3:
 * <p>
 * Input: words = ["adsdf","sfd"]
 * Output: ["adsdf","sfd"]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= words.length <= 20
 * 1 <= words[i].length <= 100
 * words[i] consists of English letters (both lowercase and uppercase).
 */
public class _500_KeyboardRow {

    public static String[] findWords(String[] words) {
        Set<String> set1 = new HashSet<>(Arrays.asList("qwertyuiop".split("")));
        Set<String> set2 = new HashSet<>(Arrays.asList("asdfghjkl".split("")));
        Set<String> set3 = new HashSet<>(Arrays.asList("zxcvbnm".split("")));
        List<String> list = new ArrayList<>();
        for (String word : words) {
            List<String> tmp = Arrays.asList(word.toLowerCase().split(""));
            if (set1.containsAll(tmp) || set2.containsAll(tmp) || set3.containsAll(tmp)) {
                list.add(word);
            }
        }
        return list.toArray(new String[0]);
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(findWords(new String[]{"Hello", "Alaska", "Dad", "Peace"})));
        System.out.println(Arrays.toString(new String[]{"Alaska", "Dad"}));
        System.out.println();

        System.out.println(Arrays.toString(findWords(new String[]{"adsdf", "sfd"})));
        System.out.println(Arrays.toString(new String[]{"adsdf", "sfd"}));
        System.out.println();

        System.out.println(Arrays.toString(findWords(new String[]{"omk"})));
        System.out.println();
        System.out.println();

    }
}
