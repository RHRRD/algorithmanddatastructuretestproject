package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/
 * <p>
 * Given a string s, find the length of the longest substring without repeating characters.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "abcabcbb"
 * Output: 3
 * Explanation: The answer is "abc", with the length of 3.
 * Example 2:
 * <p>
 * Input: s = "bbbbb"
 * Output: 1
 * Explanation: The answer is "b", with the length of 1.
 * Example 3:
 * <p>
 * Input: s = "pwwkew"
 * Output: 3
 * Explanation: The answer is "wke", with the length of 3.
 * Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 0 <= s.length <= 5 * 10^4
 * s consists of English letters, digits, symbols and spaces.
 */
public class _3_LongestSubstringWithoutRepeatingCharacters {

    public static int lengthOfLongestSubstring(String s) {
        char[] array = s.toCharArray();
        if (array.length == 0) {
            return 0;
        }
        int res = 1, cnt = 1, lastInd = 0;
        Map<Character, Integer> map = new HashMap<>();
        map.put(array[0], 0);
        for (int i = 1; i < array.length; i++) {
            Integer cur = map.get(array[i]);
            if (cur == null) {
                cnt++;
            } else {
                res = Math.max(res, cnt);
                cnt = i - Math.max(lastInd - 1, cur);
                lastInd = Math.max(lastInd, cur + 1);
            }
            map.put(array[i], i);
        }
        return Math.max(res, cnt);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(5, lengthOfLongestSubstring("tmmzuxt"), "tmmzuxt");
        CheckAnswerUtil.check(8, lengthOfLongestSubstring("zwnigfunjwz"), "zwnigfunjwz");
        CheckAnswerUtil.check(3, lengthOfLongestSubstring("zwnnwz"), "zwnnwz");
        CheckAnswerUtil.check(3, lengthOfLongestSubstring("znwnwz"), "znwnwz");
        CheckAnswerUtil.check(2, lengthOfLongestSubstring("abba"), "abba");
        CheckAnswerUtil.check(3, lengthOfLongestSubstring("abcabcbb"), "abcabcbb");
        CheckAnswerUtil.check(6, lengthOfLongestSubstring("abcdef"), "abcdef");
        CheckAnswerUtil.check(0, lengthOfLongestSubstring(""), "");
        CheckAnswerUtil.check(1, lengthOfLongestSubstring("a"), "a");
        CheckAnswerUtil.check(2, lengthOfLongestSubstring("ababa"), "ababa");
        CheckAnswerUtil.check(1, lengthOfLongestSubstring("bbbbb"), "bbbbb");
        CheckAnswerUtil.check(3, lengthOfLongestSubstring("pwwkew"), "pwwkew");
    }
}
