package algorithms.leetcode.algorithms;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/left-and-right-sum-differences/
 * <p>
 * Given a 0-indexed integer array nums, find a 0-indexed integer array answer where:
 * <p>
 * answer.length == nums.length.
 * answer[i] = |leftSum[i] - rightSum[i]|.
 * Where:
 * <p>
 * leftSum[i] is the sum of elements to the left of the index i in the array nums. If there is no such element, leftSum[i] = 0.
 * rightSum[i] is the sum of elements to the right of the index i in the array nums. If there is no such element, rightSum[i] = 0.
 * Return the array answer.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [10,4,8,3]
 * Output: [15,1,11,22]
 * Explanation: The array leftSum is [0,10,14,22] and the array rightSum is [15,11,3,0].
 * The array answer is [|0 - 15|,|10 - 11|,|14 - 3|,|22 - 0|] = [15,1,11,22].
 * Example 2:
 * <p>
 * Input: nums = [1]
 * Output: [0]
 * Explanation: The array leftSum is [0] and the array rightSum is [0].
 * The array answer is [|0 - 0|] = [0].
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 1000
 * 1 <= nums[i] <= 105
 */
public class _2574_LeftAndRightSumDifferences {

    public static int[] leftRigthDifference(int[] nums) {
        int[] result = new int[nums.length];
        result[0] = result[result.length - 1] = 0;
        for (int i = 1; i < nums.length; i++) {
            result[i] = nums[i - 1] + result[i - 1];
        }
        int tmp = 0;
        result[result.length - 1] = Math.abs(result[result.length - 1]);
        for (int i = nums.length - 2; i >= 0; i--) {
            tmp += nums[i + 1];
            result[i] = Math.abs(result[i] - tmp);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(leftRigthDifference(new int[]{10, 4, 8, 3})));
        System.out.println("must be [15,1,11,22]");
        System.out.println();

        System.out.println(Arrays.toString(leftRigthDifference(new int[]{1})));
        System.out.println("must be [0]");
        System.out.println();

        System.out.println(Arrays.toString(leftRigthDifference(new int[]{1000, 1000})));
        System.out.println("must be [1000, 1000]");
        System.out.println();

        System.out.println(Arrays.toString(leftRigthDifference(new int[]{1000, 1000, 1000})));
        System.out.println("must be [2000, 0, 2000]");
        System.out.println();

        System.out.println(Arrays.toString(leftRigthDifference(new int[]{-1000, -1000, -1000})));
        System.out.println("must be [2000, 0, 2000]");
        System.out.println();
    }
}
