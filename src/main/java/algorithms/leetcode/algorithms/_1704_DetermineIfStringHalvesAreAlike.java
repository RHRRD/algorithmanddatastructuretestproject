package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.*;

/**
 * https://leetcode.com/problems/determine-if-string-halves-are-alike/
 * <p>
 * You are given a string s of even length. Split this string into two halves of equal lengths, and let a be the first half and b be the second half.
 * <p>
 * Two strings are alike if they have the same number of vowels ('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'). Notice that s contains uppercase and lowercase letters.
 * <p>
 * Return true if a and b are alike. Otherwise, return false.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: s = "book"
 * Output: true
 * Explanation: a = "bo" and b = "ok". a has 1 vowel and b has 1 vowel. Therefore, they are alike.
 * Example 2:
 * <p>
 * Input: s = "textbook"
 * Output: false
 * Explanation: a = "text" and b = "book". a has 1 vowel whereas b has 2. Therefore, they are not alike.
 * Notice that the vowel o is counted twice.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 2 <= s.length <= 1000
 * s.length is even.
 * s consists of uppercase and lowercase letters.
 */
public class _1704_DetermineIfStringHalvesAreAlike {

    public static boolean halvesAreAlike(String s) {
        String s1 = s.substring(0, s.length() / 2);
        String s2 = s.substring(s.length() / 2);
        Set<Character> vowelsSet = new HashSet<>(List.of('a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'));
        int res1 = 0, res2 = 0;
        for (char ch : s1.toCharArray()) {
            if (vowelsSet.contains(ch)) {
                res1++;
            }
        }
        for (char ch : s2.toCharArray()) {
            if (vowelsSet.contains(ch)) {
                res2++;
            }
        }
        return Objects.equals(res1, res2);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(true, halvesAreAlike("book"), "book");
        CheckAnswerUtil.check(true, halvesAreAlike("boOk"), "boOk");
        CheckAnswerUtil.check(true, halvesAreAlike("bOOk"), "bOOk");
        CheckAnswerUtil.check(true, halvesAreAlike("bOok"), "bOok");
        CheckAnswerUtil.check(false, halvesAreAlike("textbook"), "textbook");
        CheckAnswerUtil.check(true, halvesAreAlike("aaiaaa"), "aaiaaa");
        CheckAnswerUtil.check(true, halvesAreAlike("aaiaaa"), "aaiaaa");
        CheckAnswerUtil.check(true, halvesAreAlike("AbCdEfGh"), "AbCdEfGh");
    }
}
