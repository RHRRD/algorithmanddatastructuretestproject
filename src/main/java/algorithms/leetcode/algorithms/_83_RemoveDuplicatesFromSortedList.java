package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

public class _83_RemoveDuplicatesFromSortedList {

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1);
        ListNode listNode1 = new ListNode(1);
        l1.next = listNode1;
        ListNode l2 = new ListNode(1);
        listNode1.next = l2;
        l2.next = new ListNode(4);

        ListNode tt = deleteDuplicates(l1);
        System.out.println(tt.toString());
    }

    private static ListNode deleteDuplicates(ListNode head) {
        ListNode current = head;
        while (current != null) {
            ListNode next = current.next;
            if (next != null && current.val == next.val) {
                current.next = next.next;
            } else {
                current = next;
            }
        }
        return head;
    }

}
