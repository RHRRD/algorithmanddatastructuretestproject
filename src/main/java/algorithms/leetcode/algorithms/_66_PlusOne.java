package algorithms.leetcode.algorithms;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/plus-one/
 * <p>
 * You are given a large integer represented as an integer array digits, where each digits[i] is the ith digit of the
 * integer. The digits are ordered from most significant to least significant in left-to-right order. The large integer
 * does not contain any leading 0's.
 * <p>
 * Increment the large integer by one and return the resulting array of digits.
 */
public class _66_PlusOne {

    public static void main(String[] args) {
        assert Arrays.equals(new int[]{1, 0}, plusOne(new int[]{9}));
        assert Arrays.equals(new int[]{1, 2, 4}, plusOne(new int[]{1, 2, 3}));
        assert Arrays.equals(new int[]{4, 3, 2, 2}, plusOne(new int[]{4, 3, 2, 1}));
        assert Arrays.equals(new int[]{1, 0, 0, 0, 0}, plusOne(new int[]{9, 9, 9, 9}));
    }

    public static int[] plusOne(int[] digits) {
        int[] result = new int[digits.length + 1];
        result[0] = 0;
        System.arraycopy(digits, 0, result, 1, digits.length + 1 - 1);
        result[result.length - 1]++;
        for (int i = result.length - 1; i > 0; i--) {
            if (result[i] > 9) {
                result[i] = 0;
                result[i - 1]++;
            }
        }
        if (result[0] == 0) {
            return Arrays.copyOfRange(result, 1, result.length);
        } else {
            return result;
        }
    }
}
