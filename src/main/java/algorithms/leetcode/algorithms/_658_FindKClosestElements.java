package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * https://leetcode.com/problems/find-k-closest-elements/
 * <p>
 * Given a sorted integer array arr, two integers k and x, return the k closest integers to x in the array. The result should also be sorted in ascending order.
 * <p>
 * An integer a is closer to x than an integer b if:
 * <p>
 * |a - x| < |b - x|, or
 * |a - x| == |b - x| and a < b
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: arr = [1,2,3,4,5], k = 4, x = 3
 * Output: [1,2,3,4]
 * Example 2:
 * <p>
 * Input: arr = [1,2,3,4,5], k = 4, x = -1
 * Output: [1,2,3,4]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= k <= arr.length
 * 1 <= arr.length <= 104
 * arr is sorted in ascending order.
 * -10^4 <= arr[i], x <= 10^4
 */
public class _658_FindKClosestElements {

    public static List<Integer> findClosestElements(int[] arr, int k, int x) {
        List<Integer> result = new ArrayList<>();
        if (k >= arr.length) {
            for (int i = 0; i < arr.length; i++) {
                result.add(arr[i]);
            }
            return result;
        }
        if (x <= arr[0]) {
            for (int i = 0; i < k; i++) {
                result.add(arr[i]);
            }
            return result;
        }
        if (x >= arr[arr.length - 1]) {
            for (int i = arr.length - k; i < arr.length; i++) {
                result.add(arr[i]);
            }
            return result;
        }
        int index = rightSearch(arr, 0, arr.length, x);
        int left = index, right = index + 1;
        while (result.size() != k) {
            if (left < 0) {
                result.add(arr[right]);
                right++;
            } else if (right >= arr.length) {
                result.add(arr[left]);
                left--;
            } else if (Math.abs(arr[left] - x) <= Math.abs(arr[right] - x)) {
                result.add(arr[left]);
                left--;
            } else {
                result.add(arr[right]);
                right++;
            }
        }
        Collections.sort(result);
        return result;
    }

    public static int rightSearch(int[] nums, int left, int right, int target) {
        while (left < right) {
            int mid = (left + right + 1) / 2;
            if (nums[mid] <= target) {
                left = mid;
            } else {
                right = mid - 1;
            }
        }
        if (left < nums.length - 1) {
            return Math.abs(nums[left] - target) <= Math.abs(nums[left + 1] - target) ? left : left + 1;
        }
        return left;
    }

    public static void main(String[] args) {
        System.out.println(findClosestElements(new int[]{1, 2, 3, 4, 5}, 4, 3));
        System.out.println("[1, 2, 3, 4]");
        System.out.println();

        System.out.println(findClosestElements(new int[]{0, 1, 2, 4, 5, 6}, 4, 3));
        System.out.println("[1, 2, 4, 5]");
        System.out.println();

        System.out.println(findClosestElements(new int[]{-1, 0, 1, 6, 7, 8}, 4, 3));
        System.out.println("[-1, 0, 1, 6]");
        System.out.println();

        System.out.println(findClosestElements(new int[]{-1, 0, 1, 4, 7, 8}, 4, 3));
        System.out.println("[-1, 0, 1, 4]");
        System.out.println();

        System.out.println(findClosestElements(new int[]{1, 2, 3, 4, 5}, 4, 6));
        System.out.println("[2, 3, 4, 5]");
        System.out.println();

        System.out.println(findClosestElements(new int[]{1, 2, 3, 4, 5}, 5, 6));
        System.out.println("[1, 2, 3, 4, 5]");
        System.out.println();

        System.out.println(findClosestElements(new int[]{1, 2, 3, 4, 5}, 5, 0));
        System.out.println("[1, 2, 3, 4, 5]");
        System.out.println();

        System.out.println(findClosestElements(new int[]{1, 2, 3, 4, 5}, 4, -1));
        System.out.println("[1, 2, 3, 4]");
        System.out.println();
    }
}
