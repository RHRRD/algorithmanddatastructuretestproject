package algorithms.leetcode.algorithms;

public class _1108_DefangingAnIPAddress {

    public String defangIPaddr(String address) {
        return address.replace(".", "[.]");
    }
}
