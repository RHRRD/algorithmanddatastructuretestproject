package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

/**
 * https://leetcode.com/problems/partition-list/
 * <p>
 * Given the head of a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.
 * <p>
 * You should preserve the original relative order of the nodes in each of the two partitions.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,4,3,2,5,2], x = 3
 * Output: [1,2,2,4,3,5]
 * Example 2:
 * <p>
 * Input: head = [2,1], x = 2
 * Output: [1,2]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is in the range [0, 200].
 * -100 <= Node.val <= 100
 * -200 <= x <= 200
 */
public class _86_PartitionList {

    public static ListNode partition(ListNode head, int x) {
        ListNode list1 = new ListNode();
        ListNode list2 = new ListNode();
        ListNode first = list1;
        ListNode second = list2;
        while (head != null) {
            if (head.val < x) {
                first.next = head;
                first = first.next;
            } else {
                second.next = head;
                second = second.next;
            }
            head = head.next;
        }
        second.next = null;
        first.next = list2.next;
        return list1.next;
    }

    public static void main(String[] args) {
        ListNode list_1 = new ListNode(1, new ListNode(4, new ListNode(3, new ListNode(2, new ListNode(5, new ListNode(2))))));
        System.out.println(list_1);
        System.out.println(partition(list_1, 3));
        System.out.println();

        ListNode list_2 = new ListNode(2, new ListNode(1));
        System.out.println(list_2);
        System.out.println(partition(list_2, 2));
        System.out.println();

        ListNode list_3 = new ListNode(3);
        System.out.println(list_3);
        System.out.println(partition(list_3, 2));
        System.out.println();

    }
}
