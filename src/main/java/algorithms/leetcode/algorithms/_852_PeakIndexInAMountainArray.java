package algorithms.leetcode.algorithms;

public class _852_PeakIndexInAMountainArray {

    public static void main(String[] args) {
        assert 1 == peakIndexInMountainArray(new int[]{0, 1, 0});
        assert 1 == peakIndexInMountainArray(new int[]{0, 2, 1, 0});
    }

    private static int peakIndexInMountainArray(int[] A) {
        for (int j = 0; j < A.length; j++) {
            if (A[j] > A[j + 1]) {
                return j;
            }
        }
        return 0;
    }
}
