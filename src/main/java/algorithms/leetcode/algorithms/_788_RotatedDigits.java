package algorithms.leetcode.algorithms;

import java.util.HashMap;
import java.util.Map;

public class _788_RotatedDigits {

    public static void main(String[] args) {
        assert 4 == rotatedDigits(10);
    }

    private static int rotatedDigits(int N) {
        Map<String, String> map = new HashMap<>();
        map.put("0", "0");
        map.put("1", "1");
        map.put("8", "8");
        map.put("2", "5");
        map.put("5", "2");
        map.put("6", "9");
        map.put("9", "6");
        int count = 0;
        for (int i = 1; i <= N; i++) {
            StringBuilder sb = new StringBuilder();
            String str = String.valueOf(i);
            for (char c : str.toCharArray()) {
                if (map.get(String.valueOf(c)) == null) {
                    break;
                }
                sb.append(map.get(String.valueOf(c)));
            }
            String s = sb.toString();
            if (s.length() == str.length() && !s.equals(str)) {
                count++;
            }
        }
        return count;
    }
}
