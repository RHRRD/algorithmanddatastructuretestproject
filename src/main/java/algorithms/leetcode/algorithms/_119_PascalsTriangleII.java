package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.List;

/**
 * https://leetcode.com/problems/pascals-triangle-ii/
 * <p>
 * Given an integer rowIndex, return the rowIndexth (0-indexed) row of the Pascal's triangle.
 * <p>
 * In Pascal's triangle, each number is the sum of the two numbers directly above it as shown:
 * <p>
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: rowIndex = 3
 * Output: [1,3,3,1]
 * Example 2:
 * <p>
 * Input: rowIndex = 0
 * Output: [1]
 * Example 3:
 * <p>
 * Input: rowIndex = 1
 * Output: [1,1]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 0 <= rowIndex <= 33
 * <p>
 * <p>
 * Follow up: Could you optimize your algorithm to use only O(rowIndex) extra space?
 */
public class _119_PascalsTriangleII {

    public static List<Integer> getRow(int rowIndex) {
        List<Integer> result = new ArrayList<>();
        while (rowIndex >= 0) {
            result.add(0, 1);
            for (int i = 1; i < result.size() - 1; i++) {
                result.set(i, result.get(i) + result.get(i + 1));
            }
            rowIndex--;
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("[[1]] : " + getRow(0));
        System.out.println("[[1, 1]] : " + getRow(1));
        System.out.println("[[1, 2, 1]] : " + getRow(2));
        System.out.println("[[1, 3, 3, 1]] : " + getRow(3));
        System.out.println("[[1, 4, 6, 4, 1]] : " + getRow(4));
        System.out.println("[[1, 5, 10, 10, 5, 1]] : " + getRow(5));
    }
}
