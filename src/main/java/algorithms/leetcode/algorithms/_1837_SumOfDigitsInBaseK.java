package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/sum-of-digits-in-base-k/
 * <p>
 * Given an integer n (in base 10) and a base k, return the sum of the digits of n after converting n from base 10 to base k.
 * <p>
 * After converting, each digit should be interpreted as a base 10 number, and the sum should be returned in base 10.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: n = 34, k = 6
 * Output: 9
 * Explanation: 34 (base 10) expressed in base 6 is 54. 5 + 4 = 9.
 * Example 2:
 * <p>
 * Input: n = 10, k = 10
 * Output: 1
 * Explanation: n is already in base 10. 1 + 0 = 1.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= n <= 100
 * 2 <= k <= 10
 */
public class _1837_SumOfDigitsInBaseK {

    public static int sumBase(int n, int k) {
        String s = Integer.toString(n, k);
        int res = 0;
        for (char ch : s.toCharArray()) {
            res += Integer.parseInt(String.valueOf(ch));
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(9, sumBase(34, 6));
        CheckAnswerUtil.check(1, sumBase(10, 10));
    }
}
