package algorithms.leetcode.algorithms.utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Дана прямоугольная матрица, заполненная нулями и единицами.
 * Единицы - это "земля", нули - "вода".
 * Из каждой ячейки, заполненной единицей, можно попасть в другую ячейку,
 * заполненную единицей, если каждая из их координат отличается не более,
 * чем на единицу, т.е. каждая ячейка может иметь до 8 соседей.
 * <p>
 * Найти количество таких "островов" в матрице.
 * [
 * [1, 0, 1]
 * [0, 0, 0]
 * [1, 0, 1]
 * ] -> 4
 * <p>
 * [
 * [1, 0, 1, 0]
 * [0, 1, 0, 0]
 * [1, 0, 1, 0]
 * ] -> 1
 */
public class Islands {

    public static int getIslands(int[][] field) {
        int[][] map = new int[field.length + 2][field[0].length + 2];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                map[i + 1][j + 1] = field[i][j];
            }
        }
        Set<String> set = new HashSet<>();
        int result = 0;
        for (int i = 1; i < map.length - 1; i++) {
            for (int j = 1; j < map[i].length - 1; j++) {
                if (map[i][j] == 1 && !set.contains(i + "-" + j)) {
                    result++;
                    set.add(i + "-" + j);
                    traversalNeighbors(map, i, j, set);
                }
            }
        }
        return result;
    }

    private static void traversalNeighbors(int[][] map, int i, int j, Set<String> set) {
        for (int k = i - 1; k <= i + 1; k++) {
            for (int l = j - 1; l <= j + 1; l++) {
                if (map[k][l] == 1 && !set.contains(k + "-" + l)) {
                    set.add(k + "-" + l);
                    traversalNeighbors(map, k, l, set);
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("4 : " + getIslands(new int[][]{
                {1, 0, 1},
                {0, 0, 0},
                {1, 0, 1}}
        ));
        System.out.println("1 : " + getIslands(new int[][]{
                {1, 0, 1, 0},
                {0, 1, 0, 0},
                {1, 0, 1, 0}}
        ));
        System.out.println("0 : " + getIslands(new int[][]{
                {0, 0, 0, 0},
                {0, 0, 0, 0},
                {0, 0, 0, 0}}
        ));
        System.out.println("1 : " + getIslands(new int[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0}}
        ));
        System.out.println("2 : " + getIslands(new int[][]{
                {1, 0, 0, 1},
                {0, 1, 0, 0},
                {0, 0, 1, 0}}
        ));
        System.out.println("3 : " + getIslands(new int[][]{
                {1, 0, 1, 0},
                {0, 0, 0, 0},
                {0, 0, 1, 0}}
        ));
    }
}