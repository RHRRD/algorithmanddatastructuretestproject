package algorithms.leetcode.algorithms.utils;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

class FilteringIterator<T> implements Iterator<T> {
    private Predicate<T> predicate;
    private Iterator<T> source;
    private boolean isFound;
    private T nextValue;

    public FilteringIterator(Iterator<T> source, Predicate<T> predicate) {
        this.source = source;
        this.predicate = predicate;
        this.isFound = false;
    }

    @Override
    public boolean hasNext() {
        if (isFound) {
            return true;
        } else {
            while (source.hasNext()) {
                T next = source.next();
                if (predicate.test(next)) {
                    isFound = true;
                    nextValue = next;
                    return true;
                }
            }
            return false;
        }
    }

    //returns elements from source where #predicate is true
    @Override
    public T next() {
        if (hasNext()) {
            isFound = false;
            return nextValue;
        }
        throw new NoSuchElementException();
    }

    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3);
        FilteringIterator fi = new FilteringIterator(list.listIterator(), it -> it instanceof Integer && (Integer) it % 2 == 0);
        System.out.println(fi.hasNext());
        while (fi.hasNext()) {
            System.out.println(fi.next());
        }
        System.out.println(fi.hasNext());
        System.out.println();

        List<Integer> list2 = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        FilteringIterator fi2 = new FilteringIterator(list2.listIterator(), it -> it instanceof Integer && (Integer) it % 2 == 0);
        System.out.println(fi2.hasNext());
        while (fi2.hasNext()) {
            System.out.println(fi2.next());
        }
        System.out.println(fi.hasNext());
    }
}