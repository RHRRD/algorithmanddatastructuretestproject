package algorithms.leetcode.algorithms.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CurrencyEnum {

    public static final List<CurrencyEnum> VALUES = new ArrayList<>();

    public static final CurrencyEnum USD = new CurrencyEnum();
    public static final CurrencyEnum RUR = new CurrencyEnum();

    static {
        Field[] fields = CurrencyEnum.class.getFields();
        for (Field field : fields) {
            if (CurrencyEnum.class.equals(field.getType())) {
                CurrencyEnum currencyEnum = null;
                try {
                    currencyEnum = (CurrencyEnum) field.get(null);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
                currencyEnum.name = field.getName();
            }
        }
    }

    private final int ordinal;
    private String name;

    public CurrencyEnum() {
        this.ordinal = VALUES.size();
        VALUES.add(this);
    }

    public static List<CurrencyEnum> values() {
        return Collections.unmodifiableList(VALUES);
    }

    public int ordinal() {
        return ordinal;
    }

    public String name() {
        return name;
    }

    public static CurrencyEnum valueOf(int ordinal) {
        return VALUES.get(ordinal);
    }

    public static CurrencyEnum valueOf(String name) {
        for (CurrencyEnum value : VALUES) {
            if (value.name.equals(name)) {
                return value;
            }
        }
        throw new IllegalArgumentException();
    }

    @Override
    public String toString() {
        return name;
    }

    public static void main(String[] args) {
        System.out.println(CurrencyEnum.USD.name);
        System.out.println(CurrencyEnum.USD.ordinal);
        System.out.println(CurrencyEnum.values());
        System.out.println(CurrencyEnum.valueOf(1));
        System.out.println(CurrencyEnum.valueOf("RUR"));
    }

}
