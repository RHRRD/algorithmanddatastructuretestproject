package algorithms.leetcode.algorithms.utils;

public class MyPow {

    public static int myPow(int x, int n) {
        int res = 1;
        while (n > 0) {
            if (n % 2 == 1) {
                res *= x;
            }
            x *= x;
            n /= 2;
        }
        return res;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(32, myPow(2, 5));
        CheckAnswerUtil.check(1024, myPow(2, 10));
        CheckAnswerUtil.check(512, myPow(2, 9));
    }
}
