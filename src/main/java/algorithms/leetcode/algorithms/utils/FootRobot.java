package algorithms.leetcode.algorithms.utils;

import java.util.concurrent.atomic.AtomicReference;

class FootRobot implements Runnable {
    private final String name;
    private static final Object lock = new Object();
    private static final AtomicReference<String> ar = new AtomicReference<>("left");

    public FootRobot(String name) {
        this.name = name;
    }

    public void run() {
        while (!Thread.interrupted()) {
            try {
                synchronized (lock) {
                    while (ar.get().equals(name)) {
                        lock.wait();
                    }
                    step();
                    ar.set(name);
                    lock.notify();
                }
            } catch (Exception ex) {

            }
        }
    }

    private void step() {
        System.out.println("Step by " + name);
    }

    public static void main(String[] args) {
        new Thread(new FootRobot("left")).start();
        new Thread(new FootRobot("right")).start();
    }
}
