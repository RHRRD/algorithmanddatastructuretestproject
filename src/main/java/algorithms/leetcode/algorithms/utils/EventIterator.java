package algorithms.leetcode.algorithms.utils;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class EventIterator implements Iterator<Integer> {


    private Iterator<Integer> iterator;
    private boolean isFind;
    private Integer nextValue;

    public EventIterator(Iterator<Integer> iterator) {
        this.iterator = iterator;
        updateNextValue();
    }

    private void updateNextValue() {
        while (iterator.hasNext()) {
            Integer tmp = iterator.next();
            if (tmp % 2 == 0) {
                isFind = true;
                nextValue = tmp;
                return;
            }
        }
        isFind = false;
        nextValue = null;
    }

    @Override
    public Integer next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        Integer tmp = nextValue;
        updateNextValue();
        return tmp;
    }

    @Override
    public boolean hasNext() {
        if (!isFind) {
            updateNextValue();
        }
        return isFind;
    }

    public static void main(String[] args) {
        List<Integer> list = List.of(1, 2, 3);
        EventIterator iterator = new EventIterator(list.listIterator());
        System.out.println(iterator.hasNext());
        System.out.println(iterator.next());
        System.out.println(iterator.hasNext());
        System.out.println();

        List<Integer> list2 = List.of(1, 2, 3, 4, 5, 6, 7, 8, 9);
        EventIterator iterator2 = new EventIterator(list2.listIterator());
        System.out.println(iterator2.hasNext());
        System.out.println(iterator2.next());
        System.out.println(iterator2.hasNext());
        System.out.println(iterator2.next());
        System.out.println(iterator2.hasNext());
        System.out.println(iterator2.next());
        System.out.println(iterator2.hasNext());
        System.out.println(iterator2.next());
        System.out.println(iterator2.hasNext());
    }

}
