package algorithms.leetcode.algorithms.utils;

import algorithms.leetcode.algorithms.common.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

/**
 * https://en.wikipedia.org/wiki/Tree_traversal
 */
public class TraversalTree {

    /**
     * Pre-order, NLR
     * 1. Visit the current node (in the figure: position red).
     * 2. Recursively traverse the current node's left subtree.
     * 3. Recursively traverse the current node's right subtree.
     */
    public static List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while (cur != null || !stack.isEmpty()) {
            if (cur != null) {
                stack.push(cur);
                result.add(cur.val);
                cur = cur.left;
            } else {
                cur = stack.pop();
                cur = cur.right;
            }
        }
        return result;
    }

    /**
     * In-order, LNR
     * 1. Recursively traverse the current node's left subtree.
     * 2. Visit the current node (in the figure: position green).
     * 3. Recursively traverse the current node's right subtree.
     */
    public static List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while (cur != null || !stack.isEmpty()) {
            if (cur != null) {
                stack.push(cur);
                cur = cur.left;
            } else {
                cur = stack.pop();
                result.add(cur.val);
                cur = cur.right;
            }
        }
        return result;
    }

    /**
     * Post-order, LRN
     * 1. Recursively traverse the current node's left subtree.
     * 2. Recursively traverse the current node's right subtree.
     * 3. Visit the current node (in the figure: position blue).
     */
    public static List<Integer> postorderTraversal(TreeNode root) {
        LinkedList<Integer> result = new LinkedList<>();
        Stack<TreeNode> stack = new Stack<>();
        TreeNode cur = root;
        while (cur != null || !stack.isEmpty()) {
            if (cur != null) {
                stack.push(cur);
                result.addFirst(cur.val);
                cur = cur.right;
            } else {
                cur = stack.pop();
                cur = cur.left;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("Pre-order Traversal");
        System.out.println(preorderTraversal(null));
        System.out.println(preorderTraversal(new TreeNode(1)));
        System.out.println("[1, 2, 4, 5, 3, 6, 7] : " + preorderTraversal(new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, new TreeNode(6), new TreeNode(7)))));
        System.out.println(preorderTraversal(new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null))));
        System.out.println(preorderTraversal(new TreeNode(1, null, new TreeNode(2, null, new TreeNode(3)))));
        System.out.println();
        System.out.println("In-order Traversal");
        System.out.println(inorderTraversal(null));
        System.out.println(inorderTraversal(new TreeNode(1)));
        System.out.println("[4, 2, 5, 1, 6, 3, 7] : " + inorderTraversal(new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, new TreeNode(6), new TreeNode(7)))));
        System.out.println(inorderTraversal(new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null))));
        System.out.println(inorderTraversal(new TreeNode(1, null, new TreeNode(2, null, new TreeNode(3)))));
        System.out.println();
        System.out.println("Post-order Traversal");
        System.out.println(postorderTraversal(null));
        System.out.println(postorderTraversal(new TreeNode(1)));
        System.out.println("[4, 5, 2, 6, 7, 3, 1] : " + postorderTraversal(new TreeNode(1, new TreeNode(2, new TreeNode(4), new TreeNode(5)), new TreeNode(3, new TreeNode(6), new TreeNode(7)))));
        System.out.println(postorderTraversal(new TreeNode(1, null, new TreeNode(2, new TreeNode(3), null))));
        System.out.println(postorderTraversal(new TreeNode(1, null, new TreeNode(2, null, new TreeNode(3)))));
        System.out.println();
    }

}
