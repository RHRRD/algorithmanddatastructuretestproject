package algorithms.leetcode.algorithms.utils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class Renamer {

    public static void main(String[] args) throws IOException {
        final File[] files = new File("./src/main/java/algorithms/leetcode/algorithms").listFiles();
        for (File f : files) {
            final String oldFileName = f.getName();
            if (!f.isDirectory() && !oldFileName.contains("_")) {
                final String prefix = oldFileName.replaceAll("\\D+", "");
                final String newFileName = "_" + prefix + "_" + oldFileName.replaceAll(prefix, "");
                System.out.println(oldFileName + " " + newFileName);

                replaceTextInFile(f, oldFileName.split("[.]")[0], newFileName.split("[.]")[0]);
                f.renameTo(new File(f.getParent(), newFileName));
            }
        }
    }

    private static void replaceTextInFile(File f, String oldLine, String newLine) throws IOException {
        List<String> fileContent = new ArrayList<>(Files.readAllLines(f.toPath(), StandardCharsets.UTF_8));
        for (int i = 0; i < fileContent.size(); i++) {
            if (fileContent.get(i).contains(oldLine)) {
                fileContent.set(i, fileContent.get(i).replace(oldLine, newLine));
                break;
            }
        }
        Files.write(f.toPath(), fileContent, StandardCharsets.UTF_8);
    }

}
