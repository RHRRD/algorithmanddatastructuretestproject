package algorithms.leetcode.algorithms.utils;

public class CheckAnswerUtil {

    public static void check(Object expected, Object result, Object obj) {
        System.out.print("Equals: " + expected.equals(result));
        if (expected.equals(result)) {
            System.out.println();
        } else {
            System.out.println(" <------------!");
        }
        System.out.println("\tFor: " + obj);
        System.out.print("\tExpected: " + expected);
        System.out.print("; Was: " + result);
        System.out.println();
        System.out.println();
    }

    public static void check(Object expected, Object result) {
        System.out.print("Equals: " + expected.equals(result));
        if (expected.equals(result)) {
            System.out.println();
        } else {
            System.out.println(" <------------!");
        }
        System.out.print("\tExpected: " + expected);
        System.out.print("; Was: " + result);
        System.out.println();
        System.out.println();
    }

}
