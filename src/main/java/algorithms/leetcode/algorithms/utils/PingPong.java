package algorithms.leetcode.algorithms.utils;

public class PingPong {

    private static class Game extends Thread {
        private int round = 3;
        private static String turn = "Pong";
        private static final Object lock = new Object();
        private String name;

        public Game(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            while (!isDone()) {
                play();
            }
        }

        private void play() {
            try {
                synchronized (lock) {
                    while (!isMyTurn()) {
                        lock.wait();
                    }
                    hit();
                }
            } catch (Exception ex) {

            }
        }

        private void hit() {
            System.out.println(name + "!");
            turn = name;
            round--;
            lock.notify();
        }

        private boolean isMyTurn() {
            return !turn.equals(name);
        }

        private boolean isDone() {
            return round <= 0;
        }
    }

    public static void main(String[] args) throws InterruptedException {
        Thread ping = new Game("Ping");
        Thread pong = new Game("Pong");

        System.out.println("Go!");
        ping.start();
        pong.start();

        ping.join();
        pong.join();

        System.out.println("Stop!");

    }

}
