package algorithms.leetcode.algorithms;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/sort-colors/
 * <p>
 * Given an array nums with n objects colored red, white, or blue, sort them in-place so that objects of the same color are adjacent, with the colors in the order red, white, and blue.
 * <p>
 * We will use the integers 0, 1, and 2 to represent the color red, white, and blue, respectively.
 * <p>
 * You must solve this problem without using the library's sort function.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [2,0,2,1,1,0]
 * Output: [0,0,1,1,2,2]
 * Example 2:
 * <p>
 * Input: nums = [2,0,1]
 * Output: [0,1,2]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * n == nums.length
 * 1 <= n <= 300
 * nums[i] is either 0, 1, or 2.
 * <p>
 * <p>
 * Follow up: Could you come up with a one-pass algorithm using only constant extra space?
 */
public class _75_SortColors {

    public static void sortColors(int[] nums) {
        int count_0 = 0;
        int count_1 = 0;
        int count_2 = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                count_0++;
            } else if (nums[i] == 1) {
                count_1++;
            } else {
                count_2++;
            }
        }
        int i = 0;
        while (i < count_0) {
            nums[i++] = 0;
        }
        while (i < count_0 + count_1) {
            nums[i++] = 1;
        }
        while (i < count_0 + count_1 + count_2) {
            nums[i++] = 2;
        }
    }

    public static void sortColorsOld(int[] nums) {
        int n = nums.length;
        int firstNon = 0, first = 0;
        int lastZero = 0;
        while (firstNon < n && first < n) {
            while (firstNon < n && nums[firstNon] == 0) {
                firstNon++;
            }
            while (first < n && nums[first] != 0) {
                first++;
            }
            if (firstNon >= n || first >= n) {
                break;
            }
            boolean isSwap = false;
            if (firstNon < first) {
                int tmp = nums[firstNon];
                nums[firstNon] = nums[first];
                nums[first] = tmp;
                isSwap = true;
            }
            firstNon++;
            first++;
            lastZero = isSwap ? firstNon : first;
        }
        if (lastZero < n && nums[lastZero] == 0) {
            lastZero++;
        }
        firstNon = lastZero;
        first = lastZero;
        while (firstNon < n && first < n) {
            while (firstNon < n && nums[firstNon] == 1) {
                firstNon++;
            }
            while (first < n && nums[first] != 1) {
                first++;
            }
            if (firstNon >= n || first >= n) {
                break;
            }
            if (firstNon < first) {
                int tmp = nums[firstNon];
                nums[firstNon] = nums[first];
                nums[first] = tmp;
            }
            firstNon++;
            first++;
        }
    }

    public static void main(String[] args) {
        int[] nums1 = new int[]{2, 0, 2, 1, 1, 0};
        System.out.println(Arrays.toString(nums1));
        sortColors(nums1);
        System.out.println(Arrays.toString(nums1));
        System.out.println(Arrays.toString(new int[]{0, 0, 1, 1, 2, 2}));
        System.out.println();

        int[] nums2 = new int[]{2, 0, 1};
        System.out.println(Arrays.toString(nums2));
        sortColors(nums2);
        System.out.println(Arrays.toString(nums2));
        System.out.println(Arrays.toString(new int[]{0, 1, 2}));
        System.out.println();

        int[] nums3 = new int[]{2, 0, 0};
        System.out.println(Arrays.toString(nums3));
        sortColors(nums3);
        System.out.println(Arrays.toString(nums3));
        System.out.println(Arrays.toString(new int[]{0, 0, 2}));
        System.out.println();

        int[] nums4 = new int[]{2, 1, 1};
        System.out.println(Arrays.toString(nums4));
        sortColors(nums4);
        System.out.println(Arrays.toString(nums4));
        System.out.println(Arrays.toString(new int[]{1, 1, 2}));
        System.out.println();

        int[] nums5 = new int[]{0, 1, 1};
        System.out.println(Arrays.toString(nums5));
        sortColors(nums5);
        System.out.println(Arrays.toString(nums5));
        System.out.println(Arrays.toString(new int[]{0, 1, 1}));
        System.out.println();

        int[] nums6 = new int[]{1, 1, 1};
        System.out.println(Arrays.toString(nums6));
        sortColors(nums6);
        System.out.println(Arrays.toString(nums6));
        System.out.println(Arrays.toString(new int[]{1, 1, 1}));
        System.out.println();

        int[] nums7 = new int[]{1, 0};
        System.out.println(Arrays.toString(nums7));
        sortColors(nums7);
        System.out.println(Arrays.toString(nums7));
        System.out.println(Arrays.toString(new int[]{0, 1}));
        System.out.println();

        int[] nums8 = new int[]{0, 1};
        System.out.println(Arrays.toString(nums8));
        sortColors(nums8);
        System.out.println(Arrays.toString(nums8));
        System.out.println(Arrays.toString(new int[]{0, 1}));
        System.out.println();

        int[] nums9 = new int[]{0, 0, 1, 1, 1, 2, 2, 2, 2, 2};
        System.out.println(Arrays.toString(nums9));
        sortColors(nums9);
        System.out.println(Arrays.toString(nums9));
        System.out.println(Arrays.toString(new int[]{0, 0, 1, 1, 1, 2, 2, 2, 2, 2}));
        System.out.println();

        int[] nums10 = new int[]{0, 2, 1};
        System.out.println(Arrays.toString(nums10));
        sortColors(nums10);
        System.out.println(Arrays.toString(nums10));
        System.out.println(Arrays.toString(new int[]{0, 1, 2}));
        System.out.println();

        int[] nums11 = new int[]{0, 0, 1};
        System.out.println(Arrays.toString(nums11));
        sortColors(nums11);
        System.out.println(Arrays.toString(nums11));
        System.out.println(Arrays.toString(new int[]{0, 0, 1}));
        System.out.println();

    }
}
