package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/guess-number-higher-or-lower/
 * <p>
 * We are playing the Guess Game. The game is as follows:
 * <p>
 * I pick a number from 1 to n. You have to guess which number I picked.
 * <p>
 * Every time you guess wrong, I will tell you whether the number I picked is higher or lower than your guess.
 * <p>
 * You call a pre-defined API int guess(int num), which returns three possible results:
 * <p>
 * -1: Your guess is higher than the number I picked (i.e. num > pick).
 * 1: Your guess is lower than the number I picked (i.e. num < pick).
 * 0: your guess is equal to the number I picked (i.e. num == pick).
 * Return the number that I picked.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: n = 10, pick = 6
 * Output: 6
 * Example 2:
 * <p>
 * Input: n = 1, pick = 1
 * Output: 1
 * Example 3:
 * <p>
 * Input: n = 2, pick = 1
 * Output: 1
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= n <= 2^31 - 1
 * 1 <= pick <= n
 */
public class _374_GuessNumberHigherOrLower {

    /**
     * Forward declaration of guess API.
     *
     * @param num your guess
     * @return -1 if num is higher than the picked number
     * 1 if num is lower than the picked number
     * otherwise return 0
     * ;
     */
    private static int guess(int num) {
        return Integer.compare(990, num);
    }

    public static int guessNumber(int n) {
        int myGuess = n, lastGuess = 0, tmp, i;
        int res = guess(myGuess);
        while (res != 0) {
            tmp = lastGuess;
            lastGuess = myGuess;
            i = Math.abs(myGuess - tmp) / 2;
            if (res == 1) {
                myGuess += i == 0 ? 1 : i;
            } else if (res == -1) {
                myGuess -= i == 0 ? 1 : i;
            }
            res = guess(myGuess);
        }
        return myGuess;
    }

    public static void main(String[] args) {
//        CheckAnswerUtil.check(1, guessNumber(3), 3);
//        CheckAnswerUtil.check(6, guessNumber(10), 10);
//        CheckAnswerUtil.check(1, guessNumber(1), 1);
//        CheckAnswerUtil.check(1, guessNumber(2), 2);
//        CheckAnswerUtil.check(99, guessNumber(1000), 1000);
        CheckAnswerUtil.check(990, guessNumber(1000000), 1000000);
    }
}
