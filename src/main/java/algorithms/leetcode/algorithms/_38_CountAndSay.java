package algorithms.leetcode.algorithms;

/**
 * https://leetcode.com/problems/count-and-say/
 * <p>
 * The count-and-say sequence is a sequence of digit strings defined by the recursive formula:
 * <p>
 * countAndSay(1) = "1"
 * countAndSay(n) is the way you would "say" the digit string from countAndSay(n-1), which is then converted into a different digit string.
 * To determine how you "say" a digit string, split it into the minimal number of substrings such that each substring contains exactly one unique digit. Then for each substring, say the number of digits, then say the digit. Finally, concatenate every said digit.
 * <p>
 * For example, the saying and conversion for digit string "3322251":
 * <p>
 * <p>
 * Given a positive integer n, return the nth term of the count-and-say sequence.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: n = 1
 * Output: "1"
 * Explanation: This is the base case.
 * Example 2:
 * <p>
 * Input: n = 4
 * Output: "1211"
 * Explanation:
 * countAndSay(1) = "1"
 * countAndSay(2) = say "1" = one 1 = "11"
 * countAndSay(3) = say "11" = two 1's = "21"
 * countAndSay(4) = say "21" = one 2 + one 1 = "12" + "11" = "1211"
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= n <= 30
 */
public class _38_CountAndSay {

    public static String countAndSay(int n) {
        String res = "1";
        for (int i = 1; i < n; i++) {
            res = say(res);
        }
        return res;
    }

    private static String say(String str) {
        StringBuilder sb = new StringBuilder();
        char[] chars = str.toCharArray();
        char prev = chars[0];
        int cnt = 1;
        for (int i = 1; i < chars.length; i++) {
            if (prev == chars[i]) {
                cnt++;
            } else {
                sb.append(cnt);
                sb.append(prev);
                prev = chars[i];
                cnt = 1;
            }
        }
        sb.append(cnt);
        sb.append(prev);
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(countAndSay(1));
//        System.out.println(say("1"));
//        System.out.println("11");
        System.out.println("1");
        System.out.println();

        System.out.println(countAndSay(2));
//        System.out.println(say("11"));
//        System.out.println("21");
        System.out.println("11");
        System.out.println();

        System.out.println(countAndSay(3));
//        System.out.println(say("21"));
//        System.out.println("1211");
        System.out.println("21");
        System.out.println();

        System.out.println(countAndSay(4));
//        System.out.println(say("1211"));
//        System.out.println("111221");
        System.out.println("1211");
        System.out.println();

        System.out.println(countAndSay(5));
        System.out.println(111221);
        System.out.println();

        System.out.println(countAndSay(6));
        System.out.println(312211);
        System.out.println();
    }
}
