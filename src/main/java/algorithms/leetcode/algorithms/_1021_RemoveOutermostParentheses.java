package algorithms.leetcode.algorithms;

public class _1021_RemoveOutermostParentheses {

    public static void main(String[] args) {
        assert "".equals(removeOuterParentheses("()()"));
        assert "()()()()(())".equals(removeOuterParentheses("(()())(())(()(()))"));
    }

    private static String removeOuterParentheses(String S) {
        StringBuilder sb = new StringBuilder();
        int cnt = 0;
        StringBuilder buffer = new StringBuilder();
        for (char c : S.toCharArray()) {
            if (c == '(') {
                cnt++;
            } else if (c == ')') {
                cnt--;
            }
            buffer.append(c);
            if (cnt == 0) {
                sb.append(buffer.substring(1, buffer.length() - 1));
                buffer = new StringBuilder();
            }
        }
        return sb.toString();
    }

}
