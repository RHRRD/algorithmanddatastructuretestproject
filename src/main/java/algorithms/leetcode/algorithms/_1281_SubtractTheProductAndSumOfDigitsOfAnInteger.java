package algorithms.leetcode.algorithms;

public class _1281_SubtractTheProductAndSumOfDigitsOfAnInteger {

    public int subtractProductAndSum(int n) {
        int sum = 0;
        int pr = 1;
        for (char c : String.valueOf(n).toCharArray()) {
            int value = Character.getNumericValue(c);
            sum += value;
            pr *= value;
        }
        return pr - sum;
    }
}
