package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/minimum-sum-of-four-digit-number-after-splitting-digits/
 * <p>
 * You are given a positive integer num consisting of exactly four digits. Split num into two new integers new1 and new2 by using the digits found in num. Leading zeros are allowed in new1 and new2, and all the digits found in num must be used.
 * <p>
 * For example, given num = 2932, you have the following digits: two 2's, one 9 and one 3. Some of the possible pairs [new1, new2] are [22, 93], [23, 92], [223, 9] and [2, 329].
 * Return the minimum possible sum of new1 and new2.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: num = 2932
 * Output: 52
 * Explanation: Some possible pairs [new1, new2] are [29, 23], [223, 9], etc.
 * The minimum sum can be obtained by the pair [29, 23]: 29 + 23 = 52.
 * Example 2:
 * <p>
 * Input: num = 4009
 * Output: 13
 * Explanation: Some possible pairs [new1, new2] are [0, 49], [490, 0], etc.
 * The minimum sum can be obtained by the pair [4, 9]: 4 + 9 = 13.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1000 <= num <= 9999
 */
public class _2160_MinimumSumOfFourDigitNumberAfterSplittingDigits {

    public static int minimumSum(int num) {
        char[] chars = String.valueOf(num).toCharArray();
        Arrays.sort(chars);
        int num1 = Integer.parseInt(String.valueOf(chars[0]) + chars[2]);
        int num2 = Integer.parseInt(String.valueOf(chars[1]) + chars[3]);
        return num1 + num2;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(52, minimumSum(2932), 2932);
        CheckAnswerUtil.check(13, minimumSum(4009), 4009);
        CheckAnswerUtil.check(1, minimumSum(1000), 1000);
        CheckAnswerUtil.check(2, minimumSum(2000), 2000);
    }
}
