package algorithms.leetcode.algorithms;

import java.util.Arrays;

/**
 * https://leetcode.com/problems/squares-of-a-sorted-array/
 * <p>
 * Given an integer array nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: nums = [-4,-1,0,3,10]
 * Output: [0,1,9,16,100]
 * Explanation: After squaring, the array becomes [16,1,0,9,100].
 * After sorting, it becomes [0,1,9,16,100].
 * Example 2:
 * <p>
 * Input: nums = [-7,-3,2,3,11]
 * Output: [4,9,9,49,121]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= nums.length <= 104
 * -104 <= nums[i] <= 104
 * nums is sorted in non-decreasing order.
 * <p>
 * <p>
 * Follow up: Squaring each element and sorting the new array is very trivial, could you find an O(n) solution using a different approach?
 */
public class _977_SquaresOfASortedArray {

    public static int[] sortedSquares(int[] A) {
        int[] result = new int[A.length];
        int l = 0, r = A.length - 1;
        for (int i = result.length - 1; i >= 0; i--) {
            if (A[l] * A[l] > A[r] * A[r]) {
                result[i] = A[l] * A[l];
                l++;
            } else {
                result[i] = A[r] * A[r];
                r--;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println("[0,1,9,16,100] : " + Arrays.toString(sortedSquares(new int[]{-4, -1, 0, 3, 10})));

        System.out.println("[4,9,9,49,121] : " + Arrays.toString(sortedSquares(new int[]{-7, -3, 2, 3, 11})));
    }
}
