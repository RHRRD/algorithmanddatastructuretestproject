package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.ListNode;

import java.util.HashMap;

/**
 * https://leetcode.com/problems/reverse-linked-list-ii/
 * <p>
 * Given the head of a singly linked list and two integers left and right where left <= right, reverse the nodes of the list from position left to position right, and return the reversed list.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: head = [1,2,3,4,5], left = 2, right = 4
 * Output: [1,4,3,2,5]
 * Example 2:
 * <p>
 * Input: head = [5], left = 1, right = 1
 * Output: [5]
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the list is n.
 * 1 <= n <= 500
 * -500 <= Node.val <= 500
 * 1 <= left <= right <= n
 * <p>
 * <p>
 * Follow up: Could you do it in one pass?
 */
public class _92_ReverseLinkedListII {

    public static ListNode reverseBetween(ListNode head, int left, int right) {
        ListNode list1 = new ListNode();
        ListNode list2 = new ListNode();
        ListNode list3 = new ListNode();
        ListNode first = list1;
        ListNode second = list2;
        ListNode third = list3;
        int i = 1;
        while (head != null) {
            if (i < left) {
                first.next = head;
                first = first.next;
            } else if (i <= right) {
                second.next = head;
                second = second.next;
            } else {
                third.next = head;
                third = third.next;
            }
            i++;
            head = head.next;
        }
        third.next = null;
        second.next = null;
        list2 = reverseList(list2.next, list3.next);
        first.next = list2;
        return list1.next;
    }

    private static ListNode reverseList(ListNode head, ListNode tail) {
        if (head == null) {
            return null;
        }
        ListNode newHead = new ListNode(head.val);
        newHead.next = tail;
        ListNode currentNode = head.next;
        while (currentNode != null) {
            ListNode newNode = new ListNode(currentNode.val);
            newNode.next = newHead;
            newHead = newNode;
            currentNode = currentNode.next;
        }
        return newHead;
    }

    public static void main(String[] args) {
        ListNode list_1 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(list_1);
        System.out.println(reverseBetween(list_1, 2, 4));
        System.out.println();

        ListNode list_2 = new ListNode(5);
        System.out.println(list_2);
        System.out.println(reverseBetween(list_2, 1, 1));
        System.out.println();

        ListNode list_3 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(list_3);
        System.out.println(reverseBetween(list_3, 2, 3));
        System.out.println();

        ListNode list_4 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(list_4);
        System.out.println(reverseBetween(list_4, 3, 4));
        System.out.println();

        ListNode list_6 = new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(5)))));
        System.out.println(list_6);
        System.out.println(reverseBetween(list_6, 1, 5));
        System.out.println();
    }
}
