package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

/**
 * https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/
 * <p>
 * Given two strings needle and haystack, return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * Input: haystack = "sadbutsad", needle = "sad"
 * Output: 0
 * Explanation: "sad" occurs at index 0 and 6.
 * The first occurrence is at index 0, so we return 0.
 * Example 2:
 * <p>
 * Input: haystack = "leetcode", needle = "leeto"
 * Output: -1
 * Explanation: "leeto" did not occur in "leetcode", so we return -1.
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 1 <= haystack.length, needle.length <= 104
 * haystack and needle consist of only lowercase English characters.
 */
public class _28_FindTheIndexOfTheFirstOccurrenceInAString {

    private static int strStr(String haystack, String needle) {
        return haystack.indexOf(needle);
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, strStr("hello", "ll"));
        CheckAnswerUtil.check(-1, strStr("aaaaaaa", "bba"));
        CheckAnswerUtil.check(0, strStr("aaaaaaa", ""));
        CheckAnswerUtil.check(0, strStr("", ""));
        CheckAnswerUtil.check(-1, strStr("", "as"));
    }

}
