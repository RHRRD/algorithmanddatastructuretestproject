package algorithms.leetcode.algorithms;

import java.util.ArrayList;
import java.util.List;

public class _1304_FindNUniqueIntegersSumUpToZero {

    public int[] sumZero(int n) {
        List<Integer> tt = new ArrayList<>();
        for (int i = 0; i < n / 2; i++) {
            tt.add(i + 1);
            tt.add(-(i + 1));
        }
        if (n % 2 == 1) {
            tt.add(0);
        }

        return tt.stream().mapToInt(i -> i).toArray();
    }

}
