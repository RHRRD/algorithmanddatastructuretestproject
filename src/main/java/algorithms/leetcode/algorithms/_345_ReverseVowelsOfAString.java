package algorithms.leetcode.algorithms;

import java.util.Arrays;
import java.util.List;

public class _345_ReverseVowelsOfAString {

    public static void main(String[] args) {
        assert "holle".equals(reverseVowels("hello"));
        assert "leotcede".equals(reverseVowels("leetcode"));
        assert "a".equals(reverseVowels("a"));
        assert "ab".equals(reverseVowels("ab"));
        assert "aba".equals(reverseVowels("aba"));
        assert "eba".equals(reverseVowels("abe"));
        assert "b".equals(reverseVowels("b"));
        assert "".equals(reverseVowels(""));
    }

    private static String reverseVowels(String s) {
        if (s == null) return null;
        List<Character> vowels = Arrays.asList('A', 'E', 'U', 'I', 'O', 'a', 'e', 'u', 'i', 'o');
        char[] array = s.toCharArray();
        int i = 0, j = array.length - 1;
        while (i < j) {
            while (!vowels.contains(array[i]) && i < j) {
                i++;
            }
            while (!vowels.contains(array[j]) && i < j) {
                j--;
            }
            char tmp = array[i];
            array[i] = array[j];
            array[j] = tmp;
            i++;
            j--;
        }
        return new String(array);
    }

}
