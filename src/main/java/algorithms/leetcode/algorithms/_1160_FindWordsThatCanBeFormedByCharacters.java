package algorithms.leetcode.algorithms;

import java.util.HashMap;
import java.util.Map;

public class _1160_FindWordsThatCanBeFormedByCharacters {

    public static void main(String[] args) {
        assert 6 == countCharacters(new String[]{"cat", "bt", "hat", "tree"}, "atach");
        assert 0 == countCharacters(new String[]{"cat"}, "tr");
        assert 0 == countCharacters(new String[]{""}, "");
        assert 0 == countCharacters(new String[]{"1"}, "");
        assert 0 == countCharacters(new String[]{""}, "1");
    }

    private static int countCharacters(String[] words, String chars) {
        int length = 0;
        Map<Character, Integer> map = getMapCountCharacters(chars);
        boolean flag;
        for (String word : words) {
            Map<Character, Integer> tmpMap = getMapCountCharacters(word);
            flag = true;
            for (Character ch : tmpMap.keySet()) {
                if (tmpMap.get(ch) > map.getOrDefault(ch, 0)) {
                    flag = false;
                    break;
                }
            }
            if (flag) length += word.length();
        }
        return length;
    }

    private static Map<Character, Integer> getMapCountCharacters(String chars) {
        Map<Character, Integer> map;
        map = new HashMap<>();
        for (char ch : chars.toCharArray()) {
            map.put(ch, map.getOrDefault(ch, 0) + 1);
        }
        return map;
    }

}
