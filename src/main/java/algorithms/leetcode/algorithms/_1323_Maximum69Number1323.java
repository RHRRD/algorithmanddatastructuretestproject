package algorithms.leetcode.algorithms;

public class _1323_Maximum69Number1323 {

    public int maximum69Number(int num) {
        return Integer.parseInt(String.valueOf(num).replaceFirst("6", "9"));
    }
}
