package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.common.TreeNode;

/**
 * https://leetcode.com/problems/invert-binary-tree/
 * <p>
 * Given the root of a binary tree, invert the tree, and return its root.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: root = [4,2,7,1,3,6,9]
 * Output: [4,7,2,9,6,3,1]
 * Example 2:
 * <p>
 * <p>
 * Input: root = [2,1,3]
 * Output: [2,3,1]
 * Example 3:
 * <p>
 * Input: root = []
 * Output: []
 * <p>
 * <p>
 * Constraints:
 * <p>
 * The number of nodes in the tree is in the range [0, 100].
 * -100 <= Node.val <= 100
 */
public class _226_InvertBinaryTree {

    public static TreeNode invertTree(TreeNode root) {
        invertTreeInternal(root);
        return root;
    }

    private static void invertTreeInternal(TreeNode root) {
        if (root == null || (root.left == null && root.right == null)) {
            return;
        }
        TreeNode tmp = root.right;
        root.right = root.left;
        root.left = tmp;
        invertTreeInternal(root.left);
        invertTreeInternal(root.right);
    }

    public static void main(String[] args) {
        System.out.println(invertTree(null));
        System.out.println(invertTree(new TreeNode(2, new TreeNode(1), new TreeNode(3))));
        System.out.println(invertTree(new TreeNode(4, new TreeNode(2, new TreeNode(1), new TreeNode(3)), new TreeNode(7, new TreeNode(6), new TreeNode(9)))));
    }

}
