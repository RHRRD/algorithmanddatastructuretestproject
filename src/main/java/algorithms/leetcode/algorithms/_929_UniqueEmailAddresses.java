package algorithms.leetcode.algorithms;

import java.util.HashSet;
import java.util.Set;

public class _929_UniqueEmailAddresses {

    public static void main(String[] args) {
        assert 2 == numUniqueEmails(new String[]{"test.email+alex@leetcode.com", "test.e.mail+bob.cathy@leetcode.com", "testemail+david@lee.tcode.com"});
        assert 1 == numUniqueEmails(new String[]{"test.email+alex@leetcode.com"});
        assert 3 == numUniqueEmails(new String[]{"testemail@leetcode.com", "testemail1@leetcode.com", "testemail+david@lee.tcode.com"});
        assert 2 == numUniqueEmails(
                new String[]{
                        "fg.r.u.uzj+o.pw@kziczvh.com",
                        "r.cyo.g+d.h+b.ja@tgsg.z.com",
                        "fg.r.u.uzj+o.f.d@kziczvh.com",
                        "r.cyo.g+ng.r.iq@tgsg.z.com",
                        "fg.r.u.uzj+lp.k@kziczvh.com",
                        "r.cyo.g+n.h.e+n.g@tgsg.z.com",
                        "fg.r.u.uzj+k+p.j@kziczvh.com",
                        "fg.r.u.uzj+w.y+b@kziczvh.com",
                        "r.cyo.g+x+d.c+f.t@tgsg.z.com",
                        "r.cyo.g+x+t.y.l.i@tgsg.z.com",
                        "r.cyo.g+brxxi@tgsg.z.com",
                        "r.cyo.g+z+dr.k.u@tgsg.z.com",
                        "r.cyo.g+d+l.c.n+g@tgsg.z.com",
                        "fg.r.u.uzj+vq.o@kziczvh.com",
                        "fg.r.u.uzj+uzq@kziczvh.com",
                        "fg.r.u.uzj+mvz@kziczvh.com",
                        "fg.r.u.uzj+taj@kziczvh.com",
                        "fg.r.u.uzj+fek@kziczvh.com"
                });
    }

    private static int numUniqueEmails(String[] emails) {
        Set<String> list = new HashSet<>();
        for (String email : emails) {
            String[] split1 = email.split("@");
            list.add(split1[0].split("\\+")[0].replaceAll("\\.", "") + "@" + split1[1]);
        }
        return list.size();
    }
}
