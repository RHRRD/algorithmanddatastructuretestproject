package algorithms.leetcode.algorithms;

import algorithms.leetcode.algorithms.utils.CheckAnswerUtil;

import java.util.HashSet;
import java.util.Set;

/**
 * https://leetcode.com/problems/find-center-of-star-graph/
 * <p>
 * There is an undirected star graph consisting of n nodes labeled from 1 to n. A star graph is a graph where there is one center node and exactly n - 1 edges that connect the center node with every other node.
 * <p>
 * You are given a 2D integer array edges where each edges[i] = [ui, vi] indicates that there is an edge between the nodes ui and vi. Return the center of the given star graph.
 * <p>
 * <p>
 * <p>
 * Example 1:
 * <p>
 * <p>
 * Input: edges = [[1,2],[2,3],[4,2]]
 * Output: 2
 * Explanation: As shown in the figure above, node 2 is connected to every other node, so 2 is the center.
 * Example 2:
 * <p>
 * Input: edges = [[1,2],[5,1],[1,3],[1,4]]
 * Output: 1
 * <p>
 * <p>
 * Constraints:
 * <p>
 * 3 <= n <= 10^5
 * edges.length == n - 1
 * edges[i].length == 2
 * 1 <= ui, vi <= n
 * ui != vi
 * The given edges represent a valid star graph.
 */
public class _1791_FindCenterOfStarGraph {

    public static int findCenter(int[][] edges) {
        int node1 = edges[0][0];
        int node2 = edges[0][1];
        int node3 = edges[1][0];
        int node4 = edges[1][1];
        Set<Integer> set = new HashSet<>();
        set.add(node1);
        if (!set.add(node2)) {
            return node2;
        }
        if (!set.add(node3)) {
            return node3;
        }
        return node4;
    }

    public static void main(String[] args) {
        CheckAnswerUtil.check(2, findCenter(new int[][]{{1, 2}, {2, 3}, {4, 2}}));
        CheckAnswerUtil.check(1, findCenter(new int[][]{{1, 2}, {5, 1}, {1, 3}, {1, 4}}));
    }
}
