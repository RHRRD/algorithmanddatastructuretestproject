package algorithms.leetcode.algorithms;

public class _14_LongestCommonPrefix {

    public static void main(String[] args) {
        assert "fl".equals(longestCommonPrefix(new String[]{"flower", "flow", "flight"}));
        assert "".equals(longestCommonPrefix(new String[]{"dog", "racecar", "car"}));
        assert "".equals(longestCommonPrefix(new String[]{}));
        assert "c".equals(longestCommonPrefix(new String[]{"c", "c"}));
        assert "a".equals(longestCommonPrefix(new String[]{"aa", "a"}));
        assert "".equals(longestCommonPrefix(new String[]{"c", ""}));
    }

    private static String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) return "";
        StringBuilder str = new StringBuilder();
        boolean flag = true;
        int i = 0;
        while (flag && i < strs[0].length()) {
            char c = strs[0].charAt(i);
            for (int j = 1; j < strs.length; j++) {
                if (!flag || i >= strs[j].length() || c != strs[j].charAt(i)) {
                    flag = false;
                }
            }
            i++;
            if (flag) str.append(c);
        }

        return str.toString();
    }
}
