# Algorithm And Data Structure Test Project #

### What is this repository for? ###

* studying algorithms
* studying data structures 
* solving [leetcode problems](https://leetcode.com/problemset/all/)

### Structure of project ###

* Package _`algorithms.leetcode`_ contains solution of leetcode problems. Class name == name of problems + number of this problem

* Package _`algorithms.leetcode.algorithms`_ contains solutions of algorithms problem
    
* Package _`algorithms.leetcode.sql`_ contains solutions of sql problems

* Package _`algorithms.sort`_ contains sorting algorithms